package net.lalibre.collect.formstest

import net.lalibre.collect.forms.instances.Instance
import net.lalibre.collect.shared.TempFiles

object InstanceFixtures {

    fun instance(status: String = Instance.STATUS_INCOMPLETE, lastStatusChangeDate: Long = 0): Instance {
        val instancesDir = TempFiles.createTempDir()
        return InstanceUtils.buildInstance("formId", "version", instancesDir.absolutePath)
            .status(status)
            .lastStatusChangeDate(lastStatusChangeDate)
            .build()
    }
}
