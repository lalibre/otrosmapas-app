package net.lalibre.collect.errors

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.lalibre.collect.strings.localization.LocalizedActivity
import net.lalibre.collect.strings.localization.getLocalizedString

class ErrorActivity : LocalizedActivity() {
    companion object {
        const val EXTRA_ERRORS = "ERRORS"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error)
        title = getLocalizedString(net.lalibre.collect.strings.R.string.errors)
        val toolbar = findViewById<View>(net.lalibre.collect.otrosmapasshared.R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        findViewById<Toolbar>(net.lalibre.collect.otrosmapasshared.R.id.toolbar).setNavigationOnClickListener { finish() }

        val failures = intent.getSerializableExtra(EXTRA_ERRORS) as? List<ErrorItem>
        if (failures != null) {
            findViewById<RecyclerView>(R.id.errors).apply {
                adapter = ErrorAdapter(failures)
                layoutManager = LinearLayoutManager(context)
            }
        } else {
            finish()
        }
    }
}
