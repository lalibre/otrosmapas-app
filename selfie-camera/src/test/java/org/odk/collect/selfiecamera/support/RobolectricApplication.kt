package net.lalibre.collect.selfiecamera.support

import android.app.Application
import net.lalibre.collect.selfiecamera.SelfieCameraDependencyComponent
import net.lalibre.collect.selfiecamera.SelfieCameraDependencyComponentProvider

class RobolectricApplication : Application(), SelfieCameraDependencyComponentProvider {

    override lateinit var selfieCameraDependencyComponent: SelfieCameraDependencyComponent
}
