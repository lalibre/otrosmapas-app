package net.lalibre.collect.selfiecamera

import android.view.View
import androidx.activity.ComponentActivity
import net.lalibre.collect.otrosmapasshared.livedata.NonNullLiveData

internal interface Camera {
    fun initialize(activity: ComponentActivity, previewView: View)

    fun state(): NonNullLiveData<State>

    fun takePicture(imagePath: String, onImageSaved: () -> Unit, onImageSaveError: () -> Unit)

    enum class State {
        UNINITIALIZED,
        INITIALIZED,
        FAILED_TO_INITIALIZE
    }
}
