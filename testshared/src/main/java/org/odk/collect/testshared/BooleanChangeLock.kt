package net.lalibre.collect.testshared

import net.lalibre.collect.shared.locks.ChangeLock
import java.util.function.Function

class BooleanChangeLock : ChangeLock {
    private var locked = false

    override fun <T> withLock(function: Function<Boolean, T>): T {
        return function.apply(!locked)
    }

    fun lock() {
        locked = true
    }
}
