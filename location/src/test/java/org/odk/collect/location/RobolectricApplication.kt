package net.lalibre.collect.location

import android.app.Application
import net.lalibre.collect.otrosmapasshared.data.AppState
import net.lalibre.collect.otrosmapasshared.data.StateStore

class RobolectricApplication : Application(), StateStore {

    private val appState = AppState()

    override fun getState(): AppState {
        return appState
    }
}
