package net.lalibre.collect.location.satellites

import net.lalibre.collect.otrosmapasshared.livedata.NonNullLiveData

interface SatelliteInfoClient {

    val satellitesUsedInLastFix: NonNullLiveData<Int>
}
