package net.lalibre.collect.audiorecorder.recording

import android.app.Application
import net.lalibre.collect.otrosmapasshared.data.getState
import net.lalibre.collect.audiorecorder.recording.internal.ForegroundServiceAudioRecorder
import net.lalibre.collect.audiorecorder.recording.internal.RecordingRepository

open class AudioRecorderFactory(private val application: Application) {

    open fun create(): AudioRecorder {
        return ForegroundServiceAudioRecorder(application, RecordingRepository(application.getState()))
    }
}
