package net.lalibre.collect.audiorecorder.testsupport

import android.app.Application
import net.lalibre.collect.otrosmapasshared.data.AppState
import net.lalibre.collect.otrosmapasshared.data.StateStore
import net.lalibre.collect.audiorecorder.AudioRecorderDependencyComponent
import net.lalibre.collect.audiorecorder.AudioRecorderDependencyComponentProvider
import net.lalibre.collect.audiorecorder.AudioRecorderDependencyModule
import net.lalibre.collect.audiorecorder.DaggerAudioRecorderDependencyComponent

/**
 * Used as the Application in tests in in the `test/src` root. This is setup in `robolectric.properties`
 */
internal class RobolectricApplication : Application(), AudioRecorderDependencyComponentProvider, StateStore {

    override lateinit var audioRecorderDependencyComponent: AudioRecorderDependencyComponent

    private val appState = AppState()

    override fun onCreate() {
        super.onCreate()
        audioRecorderDependencyComponent = DaggerAudioRecorderDependencyComponent.builder()
            .application(this)
            .build()
    }

    fun setupDependencies(dependencyModule: AudioRecorderDependencyModule) {
        audioRecorderDependencyComponent = DaggerAudioRecorderDependencyComponent.builder()
            .dependencyModule(dependencyModule)
            .application(this)
            .build()
    }

    override fun getState(): AppState {
        return appState
    }
}
