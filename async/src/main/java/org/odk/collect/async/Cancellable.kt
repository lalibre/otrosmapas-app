package net.lalibre.collect.async

interface Cancellable {
    fun cancel(): Boolean
}
