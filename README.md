# Otros Mapas App
![Platform](https://img.shields.io/badge/platform-Android-blue.svg)
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Otros Mapas es una aplicación de recolección de datos de código abierto, basada en [ODK Collect] (https://github.com/getodk/collect), diseñada para facilitar la recopilación de datos geográficos y formularios en el campo. Nuestra versión personalizada está orientada a satisfacer las necesidades específicas de comunidades, investigadores y organizaciones que buscan mapear y recolectar información sobre diversos temas de interés local o especializado.

## Características Principales

- **Recopilación de Datos en Terreno**: Permite a los usuarios recolectar datos en formato de texto, multimedia y geoespaciales sin necesidad de conexión a internet.
- **Personalización de Formularios**: Ofrece la posibilidad de diseñar formularios personalizados para satisfacer las necesidades específicas de cada proyecto.
- **Sincronización con Servidores**: Una vez en línea, los datos recopilados pueden ser fácilmente sincronizados con servidores para su análisis y almacenamiento seguro.
- **Interfaz Amigable**: Diseñada para ser intuitiva y fácil de usar en campo, mejorando la experiencia de recolección de datos.
- **Soporte Multilingüe**: Soporte para múltiples idiomas, facilitando su uso en diversas comunidades alrededor del mundo.

## Comenzando

### Requisitos

- Android 5.0 (Lollipop) o superior.
- Conexión a internet para la sincronización de datos (opcional para la recolección de datos en terreno).

### Instalación

1. Descarga la aplicación desde [https://play.google.com/store/apps/details?id=net.lalibre.collect.otrosmapas](https://play.google.com/store/apps/details?id=net.lalibre.collect.otrosmapas)
2. Crea tu fomulario en Kobotoolbox, ODK o Google Drive
3. Abre la aplicación y configura tu servidor o usa los ajustes predeterminados para empezar.

## Uso

1. **Diseña tu Formulario**: Utiliza la plataforma XLSForm para diseñar tus formularios personalizados.
2. **Configura tu Proyecto**: En la aplicación, configura tu servidor y descarga los formularios diseñados.
3. **Recolecta Datos**: Sal al campo y comienza la recolección de datos utilizando los formularios descargados.
4. **Sincroniza los Datos**: Conéctate a internet y sincroniza los datos recolectados con tu servidor.

## Contribuir

¡Tu contribución es bienvenida! Si deseas ayudar a mejorar Otros Mapas.

## Licencia

Otros Mapas es un proyecto de código abierto licenciado bajo [Apache-2] (https://opensource.org/license/apache-2-0). Basado en ODK Collect, respeta las mismas licencias y derechos de autor asociados.

## Contacto

Para más información, preguntas o colaboraciones, por favor visita [contacto@otrosmapas.org](mailto:contacto@otrosmapas.org) o abre un issue en nuestro repositorio de GitLab.

