package net.lalibre.collect.entities

data class Entity(val dataset: String, val properties: List<Pair<String, String>>)
