package net.lalibre.collect.audioclips

data class Clip(val clipID: String, val uRI: String)
