package net.lalibre.collect.projects.support

import android.app.Application
import net.lalibre.collect.projects.DaggerProjectsDependencyComponent
import net.lalibre.collect.projects.ProjectsDependencyComponent
import net.lalibre.collect.projects.ProjectsDependencyComponentProvider

class RobolectricApplication : Application(), ProjectsDependencyComponentProvider {

    override lateinit var projectsDependencyComponent: ProjectsDependencyComponent

    override fun onCreate() {
        super.onCreate()
        projectsDependencyComponent = DaggerProjectsDependencyComponent.builder().build()
    }
}
