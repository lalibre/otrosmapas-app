package net.lalibre.collect.maps

interface MapFragmentFactory {
    fun createMapFragment(): MapFragment
}
