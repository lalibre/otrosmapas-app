package net.lalibre.collect.maps.markers

import net.lalibre.collect.maps.MapFragment
import net.lalibre.collect.maps.MapPoint

data class MarkerDescription(
    val point: MapPoint,
    val isDraggable: Boolean,
    @get:MapFragment.IconAnchor @param:MapFragment.IconAnchor
    val iconAnchor: String,
    val iconDescription: MarkerIconDescription
)
