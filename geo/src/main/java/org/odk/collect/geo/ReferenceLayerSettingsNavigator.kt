package net.lalibre.collect.geo

import androidx.fragment.app.FragmentActivity

interface ReferenceLayerSettingsNavigator {

    fun navigateToReferenceLayerSettings(activity: FragmentActivity)
}
