package net.lalibre.collect.geo

import android.Manifest
import android.app.Activity
import net.lalibre.collect.otrosmapasshared.ui.ToastUtils
import net.lalibre.collect.permissions.ContextCompatPermissionChecker

internal object GeoActivityUtils {

    @JvmStatic
    fun requireLocationPermissions(activity: Activity) {
        val permissionGranted = ContextCompatPermissionChecker(activity).isPermissionGranted(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        if (!permissionGranted) {
            ToastUtils.showLongToast(activity, net.lalibre.collect.strings.R.string.not_granted_permission)
            activity.finish()
        }
    }
}
