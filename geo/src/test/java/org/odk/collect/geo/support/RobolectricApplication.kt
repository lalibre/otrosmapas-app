package net.lalibre.collect.geo.support

import android.app.Application
import net.lalibre.collect.otrosmapasshared.ui.Animations
import net.lalibre.collect.geo.GeoDependencyComponent
import net.lalibre.collect.geo.GeoDependencyComponentProvider

class RobolectricApplication : Application(), GeoDependencyComponentProvider {

    override lateinit var geoDependencyComponent: GeoDependencyComponent

    override fun onCreate() {
        super.onCreate()
        Animations.DISABLE_ANIMATIONS = true
    }
}
