package net.lalibre.collect.settings.migration;

import static net.lalibre.collect.settings.migration.MigrationUtils.translateValue;
import static net.lalibre.collect.settings.support.SettingsUtils.assertSettings;
import static net.lalibre.collect.settings.support.SettingsUtils.initSettings;

import org.junit.Test;
import net.lalibre.collect.shared.settings.InMemSettings;
import net.lalibre.collect.shared.settings.Settings;

public class ValueTranslatorTest {

    private final Settings prefs = new InMemSettings();

    @Test
    public void translatesValueForKey() {
        initSettings(prefs,
                "key", "value"
        );

        translateValue("value").toValue("newValue").forKey("key").apply(prefs);

        assertSettings(prefs,
                "key", "newValue"
        );
    }

    @Test
    public void doesNotTranslateOtherValues() {
        initSettings(prefs,
                "key", "otherValue"
        );

        translateValue("value").toValue("newValue").forKey("key").apply(prefs);

        assertSettings(prefs,
                "key", "otherValue"
        );
    }

    @Test
    public void whenKeyNotInPrefs_doesNothing() {
        initSettings(prefs,
                "otherKey", "value"
        );

        translateValue("value").toValue("newValue").forKey("key").apply(prefs);

        assertSettings(prefs,
                "otherKey", "value"
        );
    }
}
