package net.lalibre.collect.settings.migration;

import static net.lalibre.collect.settings.migration.MigrationUtils.removeKey;
import static net.lalibre.collect.settings.support.SettingsUtils.assertSettingsEmpty;
import static net.lalibre.collect.settings.support.SettingsUtils.initSettings;

import org.junit.Test;
import net.lalibre.collect.shared.settings.InMemSettings;
import net.lalibre.collect.shared.settings.Settings;

public class KeyRemoverTest {

    private final Settings prefs = new InMemSettings();

    @Test
    public void whenKeyDoesNotExist_doesNothing() {
        initSettings(prefs);

        removeKey("blah").apply(prefs);

        assertSettingsEmpty(prefs);
    }
}
