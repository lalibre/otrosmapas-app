package net.lalibre.collect.settings.migration;

import net.lalibre.collect.shared.settings.Settings;

public interface Migration {
    void apply(Settings prefs);
}
