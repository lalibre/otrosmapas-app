package net.lalibre.collect.settings.importing

enum class SettingsImportingResult {
    SUCCESS,
    INVALID_SETTINGS,
    GD_PROJECT
}
