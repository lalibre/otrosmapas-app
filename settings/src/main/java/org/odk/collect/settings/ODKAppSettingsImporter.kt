package net.lalibre.collect.settings

import org.json.JSONObject
import net.lalibre.collect.projects.Project
import net.lalibre.collect.projects.ProjectsRepository
import net.lalibre.collect.settings.importing.ProjectDetailsCreatorImpl
import net.lalibre.collect.settings.importing.SettingsChangeHandler
import net.lalibre.collect.settings.importing.SettingsImporter
import net.lalibre.collect.settings.importing.SettingsImportingResult
import net.lalibre.collect.settings.validation.JsonSchemaSettingsValidator

class ODKAppSettingsImporter(
    projectsRepository: ProjectsRepository,
    settingsProvider: SettingsProvider,
    generalDefaults: Map<String, Any>,
    adminDefaults: Map<String, Any>,
    projectColors: List<String>,
    settingsChangedHandler: SettingsChangeHandler,
    private val deviceUnsupportedSettings: JSONObject
) {

    private val settingsImporter = SettingsImporter(
        settingsProvider,
        ODKAppSettingsMigrator(settingsProvider.getMetaSettings()),
        JsonSchemaSettingsValidator { javaClass.getResourceAsStream("/client-settings.schema.json")!! },
        generalDefaults,
        adminDefaults,
        settingsChangedHandler,
        projectsRepository,
        ProjectDetailsCreatorImpl(projectColors, generalDefaults)
    )

    fun fromJSON(json: String, project: Project.Saved): SettingsImportingResult {
        return try {
            settingsImporter.fromJSON(json, project, deviceUnsupportedSettings)
        } catch (e: Throwable) {
            SettingsImportingResult.INVALID_SETTINGS
        }
    }
}
