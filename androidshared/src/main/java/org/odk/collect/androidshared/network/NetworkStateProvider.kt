package net.lalibre.collect.otrosmapasshared.network

import android.net.NetworkInfo

interface NetworkStateProvider {
    val isDeviceOnline: Boolean
    val networkInfo: NetworkInfo?
}
