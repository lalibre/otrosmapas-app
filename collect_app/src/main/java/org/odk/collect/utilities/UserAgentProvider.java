package net.lalibre.collect.utilities;

public interface UserAgentProvider {


    String getUserAgent();
}
