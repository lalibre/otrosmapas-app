package net.lalibre.collect.otrosmapas.utilities

import android.content.Context
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.otrosmapas.formmanagement.FormDownloadException
import net.lalibre.collect.otrosmapas.formmanagement.FormDownloadExceptionMapper
import net.lalibre.collect.otrosmapas.formmanagement.ServerFormDetails
import net.lalibre.collect.errors.ErrorItem
import net.lalibre.collect.strings.localization.getLocalizedString

object FormsDownloadResultInterpreter {
    fun getFailures(result: Map<ServerFormDetails, FormDownloadException?>, context: Context) = result.filter {
        it.value != null
    }.map {
        ErrorItem(
            it.key.formName ?: "",
            context.getLocalizedString(net.lalibre.collect.strings.R.string.form_details, it.key.formId ?: "", it.key.formVersion ?: ""),
            FormDownloadExceptionMapper(context).getMessage(it.value)
        )
    }

    fun getNumberOfFailures(result: Map<ServerFormDetails, FormDownloadException?>) = result.count {
        it.value != null
    }

    fun allFormsDownloadedSuccessfully(result: Map<ServerFormDetails, FormDownloadException?>) = result.values.all {
        it == null
    }
}
