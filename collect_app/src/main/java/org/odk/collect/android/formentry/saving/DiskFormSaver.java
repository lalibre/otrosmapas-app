package net.lalibre.collect.otrosmapas.formentry.saving;

import android.net.Uri;

import net.lalibre.collect.otrosmapas.javarosawrapper.FormController;
import net.lalibre.collect.otrosmapas.tasks.SaveFormToDisk;
import net.lalibre.collect.otrosmapas.tasks.SaveToDiskResult;
import net.lalibre.collect.otrosmapas.utilities.MediaUtils;
import net.lalibre.collect.entities.EntitiesRepository;
import net.lalibre.collect.forms.instances.InstancesRepository;

import java.util.ArrayList;

public class DiskFormSaver implements FormSaver {

    @Override
    public SaveToDiskResult save(Uri instanceContentURI, FormController formController, MediaUtils mediaUtils, boolean shouldFinalize, boolean exitAfter,
                                 String updatedSaveName, ProgressListener progressListener, ArrayList<String> tempFiles, String currentProjectId, EntitiesRepository entitiesRepository, InstancesRepository instancesRepository) {
        SaveFormToDisk saveFormToDisk = new SaveFormToDisk(formController, mediaUtils, exitAfter, shouldFinalize,
                updatedSaveName, instanceContentURI, tempFiles, currentProjectId, entitiesRepository, instancesRepository);
        return saveFormToDisk.saveForm(progressListener);
    }
}
