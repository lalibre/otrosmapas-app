package net.lalibre.collect.otrosmapas.application.initialization

import net.lalibre.collect.otrosmapas.projects.ProjectDeleter
import net.lalibre.collect.projects.ProjectsRepository
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.keys.ProjectKeys
import net.lalibre.collect.upgrade.Upgrade

class GoogleDriveProjectsDeleter(
    private val projectsRepository: ProjectsRepository,
    private val settingsProvider: SettingsProvider,
    private val projectDeleter: ProjectDeleter
) : Upgrade {
    override fun key() = null

    override fun run() {
        projectsRepository.getAll().forEach {
            val unprotectedSettings = settingsProvider.getUnprotectedSettings(it.uuid)
            val protocol = unprotectedSettings.getString(ProjectKeys.KEY_PROTOCOL)

            if (protocol == ProjectKeys.PROTOCOL_GOOGLE_SHEETS) {
                projectDeleter.deleteProject(it.uuid)
            }
        }
    }
}
