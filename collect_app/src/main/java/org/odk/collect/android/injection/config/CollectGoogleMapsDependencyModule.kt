package net.lalibre.collect.otrosmapas.injection.config

import net.lalibre.collect.googlemaps.GoogleMapsDependencyModule
import net.lalibre.collect.location.LocationClient
import net.lalibre.collect.maps.layers.ReferenceLayerRepository
import net.lalibre.collect.settings.SettingsProvider

class CollectGoogleMapsDependencyModule(
    private val referenceLayerRepository: ReferenceLayerRepository,
    private val locationClient: LocationClient,
    private val settingsProvider: SettingsProvider
) : GoogleMapsDependencyModule() {
    override fun providesReferenceLayerRepository(): ReferenceLayerRepository {
        return referenceLayerRepository
    }

    override fun providesLocationClient(): LocationClient {
        return locationClient
    }

    override fun providesSettingsProvider(): SettingsProvider {
        return settingsProvider
    }
}
