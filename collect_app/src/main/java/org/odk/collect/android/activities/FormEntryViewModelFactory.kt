package net.lalibre.collect.otrosmapas.activities

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import org.javarosa.core.model.actions.recordaudio.RecordAudioActions
import org.javarosa.core.model.instance.TreeReference
import net.lalibre.collect.otrosmapas.entities.EntitiesRepositoryProvider
import net.lalibre.collect.otrosmapas.formentry.BackgroundAudioViewModel
import net.lalibre.collect.otrosmapas.formentry.BackgroundAudioViewModel.RecordAudioActionRegistry
import net.lalibre.collect.otrosmapas.formentry.FormEndViewModel
import net.lalibre.collect.otrosmapas.formentry.FormEntryViewModel
import net.lalibre.collect.otrosmapas.formentry.FormSessionRepository
import net.lalibre.collect.otrosmapas.formentry.audit.IdentityPromptViewModel
import net.lalibre.collect.otrosmapas.formentry.backgroundlocation.BackgroundLocationHelper
import net.lalibre.collect.otrosmapas.formentry.backgroundlocation.BackgroundLocationManager
import net.lalibre.collect.otrosmapas.formentry.backgroundlocation.BackgroundLocationViewModel
import net.lalibre.collect.otrosmapas.formentry.saving.DiskFormSaver
import net.lalibre.collect.otrosmapas.formentry.saving.FormSaveViewModel
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.AutoSendSettingsProvider
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService
import net.lalibre.collect.otrosmapas.utilities.ApplicationConstants
import net.lalibre.collect.otrosmapas.utilities.InstancesRepositoryProvider
import net.lalibre.collect.otrosmapas.utilities.MediaUtils
import net.lalibre.collect.async.Scheduler
import net.lalibre.collect.audiorecorder.recording.AudioRecorder
import net.lalibre.collect.location.LocationClient
import net.lalibre.collect.permissions.PermissionsChecker
import net.lalibre.collect.permissions.PermissionsProvider
import net.lalibre.collect.settings.SettingsProvider
import java.util.function.BiConsumer

class FormEntryViewModelFactory(
    owner: SavedStateRegistryOwner,
    private val mode: String?,
    private val sessionId: String,
    private val scheduler: Scheduler,
    private val formSessionRepository: FormSessionRepository,
    private val mediaUtils: MediaUtils,
    private val audioRecorder: AudioRecorder,
    private val projectsDataService: ProjectsDataService,
    private val entitiesRepositoryProvider: EntitiesRepositoryProvider,
    private val settingsProvider: SettingsProvider,
    private val permissionsChecker: PermissionsChecker,
    private val fusedLocationClient: LocationClient,
    private val permissionsProvider: PermissionsProvider,
    private val autoSendSettingsProvider: AutoSendSettingsProvider,
    private val instancesRepositoryProvider: InstancesRepositoryProvider
) : AbstractSavedStateViewModelFactory(owner, null) {

    override fun <T : ViewModel> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        val projectId = projectsDataService.getCurrentProject().uuid

        return when (modelClass) {
            FormEntryViewModel::class.java -> FormEntryViewModel(
                System::currentTimeMillis,
                scheduler,
                formSessionRepository,
                sessionId
            )

            FormSaveViewModel::class.java -> {
                FormSaveViewModel(
                    handle,
                    System::currentTimeMillis,
                    DiskFormSaver(),
                    mediaUtils,
                    scheduler,
                    audioRecorder,
                    projectsDataService,
                    formSessionRepository.get(sessionId),
                    entitiesRepositoryProvider.get(projectId),
                    instancesRepositoryProvider.get(projectId)
                )
            }

            BackgroundAudioViewModel::class.java -> {
                val recordAudioActionRegistry =
                    if (mode == ApplicationConstants.FormModes.VIEW_SENT) {
                        object : RecordAudioActionRegistry {
                            override fun register(listener: BiConsumer<TreeReference, String?>) {}
                            override fun unregister() {}
                        }
                    } else {
                        object : RecordAudioActionRegistry {
                            override fun register(listener: BiConsumer<TreeReference, String?>) {
                                RecordAudioActions.setRecordAudioListener { absoluteTargetRef: TreeReference, quality: String? ->
                                    listener.accept(absoluteTargetRef, quality)
                                }
                            }

                            override fun unregister() {
                                RecordAudioActions.setRecordAudioListener(null)
                            }
                        }
                    }

                BackgroundAudioViewModel(
                    audioRecorder,
                    settingsProvider.getUnprotectedSettings(),
                    recordAudioActionRegistry,
                    permissionsChecker,
                    System::currentTimeMillis,
                    formSessionRepository.get(sessionId)
                )
            }

            BackgroundLocationViewModel::class.java -> {
                val locationManager = BackgroundLocationManager(
                    fusedLocationClient,
                    BackgroundLocationHelper(
                        permissionsProvider,
                        settingsProvider.getUnprotectedSettings()
                    ) {
                        formSessionRepository.get(sessionId).value?.formController
                    }
                )

                BackgroundLocationViewModel(locationManager)
            }

            IdentityPromptViewModel::class.java -> IdentityPromptViewModel()

            FormEndViewModel::class.java -> FormEndViewModel(
                formSessionRepository,
                sessionId,
                settingsProvider,
                autoSendSettingsProvider
            )

            else -> throw IllegalArgumentException()
        } as T
    }
}
