package net.lalibre.collect.otrosmapas.notifications.builders

import android.app.Application
import android.app.Notification
import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.otrosmapas.instancemanagement.send.InstanceUploaderListActivity
import net.lalibre.collect.otrosmapas.mainmenu.MainMenuActivity
import net.lalibre.collect.otrosmapas.notifications.NotificationManagerNotifier
import net.lalibre.collect.otrosmapas.upload.FormUploadException
import net.lalibre.collect.otrosmapas.utilities.ApplicationConstants.RequestCodes
import net.lalibre.collect.otrosmapas.utilities.FormsUploadResultInterpreter
import net.lalibre.collect.errors.ErrorActivity
import net.lalibre.collect.forms.instances.Instance
import net.lalibre.collect.strings.localization.getLocalizedString
import java.io.Serializable

object FormsSubmissionNotificationBuilder {

    fun build(application: Application, result: Map<Instance, FormUploadException?>, projectName: String): Notification {
        val allFormsUploadedSuccessfully = FormsUploadResultInterpreter.allFormsUploadedSuccessfully(result)

        return NotificationCompat.Builder(
            application,
            NotificationManagerNotifier.COLLECT_NOTIFICATION_CHANNEL
        ).apply {
            setContentIntent(getNotificationPendingIntent(application, allFormsUploadedSuccessfully))
            setContentTitle(getTitle(application, allFormsUploadedSuccessfully))
            setContentText(getMessage(application, allFormsUploadedSuccessfully, result))
            setSubText(projectName)
            setSmallIcon(net.lalibre.collect.icons.R.drawable.ic_notification_small)
            setAutoCancel(true)

            if (!allFormsUploadedSuccessfully) {
                addAction(
                    R.drawable.ic_outline_info_small,
                    application.getLocalizedString(net.lalibre.collect.strings.R.string.show_details),
                    getShowDetailsPendingIntent(application, result)
                )
            }
        }.build()
    }

    private fun getTitle(application: Application, allFormsUploadedSuccessfully: Boolean): String {
        return if (allFormsUploadedSuccessfully) {
            application.getLocalizedString(net.lalibre.collect.strings.R.string.forms_upload_succeeded)
        } else {
            application.getLocalizedString(net.lalibre.collect.strings.R.string.forms_upload_failed)
        }
    }

    private fun getMessage(application: Application, allFormsUploadedSuccessfully: Boolean, result: Map<Instance, FormUploadException?>): String {
        return if (allFormsUploadedSuccessfully) {
            application.getLocalizedString(net.lalibre.collect.strings.R.string.all_uploads_succeeded)
        } else {
            application.getLocalizedString(
                net.lalibre.collect.strings.R.string.some_uploads_failed,
                FormsUploadResultInterpreter.getNumberOfFailures(result),
                result.size
            )
        }
    }

    private fun getNotificationPendingIntent(application: Application, allFormsUploadedSuccessfully: Boolean): PendingIntent {
        val notifyIntent = if (allFormsUploadedSuccessfully) {
            Intent(application, MainMenuActivity::class.java)
        } else {
            Intent(application, InstanceUploaderListActivity::class.java)
        }.apply {
            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        }

        return PendingIntent.getActivity(
            application,
            RequestCodes.FORMS_UPLOADED_NOTIFICATION,
            notifyIntent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
    }

    private fun getShowDetailsPendingIntent(application: Application, result: Map<Instance, FormUploadException?>): PendingIntent {
        val showDetailsIntent = Intent(application, ErrorActivity::class.java).apply {
            putExtra(ErrorActivity.EXTRA_ERRORS, FormsUploadResultInterpreter.getFailures(result, application) as Serializable)
        }

        return PendingIntent.getActivity(
            application,
            RequestCodes.FORMS_UPLOADED_NOTIFICATION,
            showDetailsIntent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
    }
}
