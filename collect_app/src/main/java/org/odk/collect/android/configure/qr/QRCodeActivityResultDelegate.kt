package net.lalibre.collect.otrosmapas.configure.qr

import android.app.Activity
import android.content.Intent
import android.widget.Toast
import net.lalibre.collect.analytics.Analytics.Companion.log
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.otrosmapas.activities.ActivityUtils
import net.lalibre.collect.otrosmapas.analytics.AnalyticsEvents
import net.lalibre.collect.otrosmapas.mainmenu.MainMenuActivity
import net.lalibre.collect.projects.Project.Saved
import net.lalibre.collect.qrcode.QRCodeDecoder
import net.lalibre.collect.settings.ODKAppSettingsImporter
import net.lalibre.collect.settings.importing.SettingsImportingResult
import java.io.FileNotFoundException
import java.io.InputStream

class QRCodeActivityResultDelegate(
    private val activity: Activity,
    private val settingsImporter: ODKAppSettingsImporter,
    private val qrCodeDecoder: QRCodeDecoder,
    private val project: Saved
) {
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == QRCodeMenuDelegate.SELECT_PHOTO && resultCode == Activity.RESULT_OK && data != null) {
            val imageUri = data.data
            if (imageUri != null) {
                val imageStream: InputStream? = try {
                    activity.contentResolver.openInputStream(imageUri)
                } catch (e: FileNotFoundException) {
                    // Not sure how this could happen? If you work it out: write a test!
                    return
                }
                try {
                    val response = qrCodeDecoder.decode(imageStream)

                    when (settingsImporter.fromJSON(response, project)) {
                        SettingsImportingResult.SUCCESS -> {
                            log(AnalyticsEvents.RECONFIGURE_PROJECT)
                            showToast(net.lalibre.collect.strings.R.string.successfully_imported_settings)
                            ActivityUtils.startActivityAndCloseAllOthers(
                                activity,
                                MainMenuActivity::class.java
                            )
                        }
                        SettingsImportingResult.INVALID_SETTINGS -> showToast(net.lalibre.collect.strings.R.string.invalid_qrcode)
                        SettingsImportingResult.GD_PROJECT -> showToast(net.lalibre.collect.strings.R.string.settings_with_gd_protocol)
                    }
                } catch (e: QRCodeDecoder.QRCodeInvalidException) {
                    showToast(net.lalibre.collect.strings.R.string.invalid_qrcode)
                } catch (e: QRCodeDecoder.QRCodeNotFoundException) {
                    showToast(net.lalibre.collect.strings.R.string.qr_code_not_found)
                }
            }
        }
    }

    private fun showToast(string: Int) {
        Toast.makeText(activity, activity.getString(string), Toast.LENGTH_LONG).show()
    }
}
