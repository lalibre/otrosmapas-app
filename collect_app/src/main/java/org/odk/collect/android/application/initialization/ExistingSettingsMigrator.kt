package net.lalibre.collect.otrosmapas.application.initialization

import net.lalibre.collect.projects.ProjectsRepository
import net.lalibre.collect.settings.ODKAppSettingsMigrator
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.upgrade.Upgrade

class ExistingSettingsMigrator(
    private val projectsRepository: ProjectsRepository,
    private val settingsProvider: SettingsProvider,
    private val settingsMigrator: ODKAppSettingsMigrator
) : Upgrade {

    override fun key(): String? {
        return null
    }

    override fun run() {
        projectsRepository.getAll().forEach {
            settingsMigrator.migrate(
                settingsProvider.getUnprotectedSettings(it.uuid),
                settingsProvider.getProtectedSettings(it.uuid)
            )
        }
    }
}
