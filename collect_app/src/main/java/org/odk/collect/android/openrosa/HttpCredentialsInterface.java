package net.lalibre.collect.otrosmapas.openrosa;

public interface HttpCredentialsInterface {
    String getUsername();

    String getPassword();
}
