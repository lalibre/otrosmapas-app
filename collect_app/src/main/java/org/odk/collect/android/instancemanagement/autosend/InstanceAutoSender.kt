package net.lalibre.collect.otrosmapas.instancemanagement.autosend

import android.content.Context
import net.lalibre.collect.otrosmapas.formmanagement.InstancesDataService
import net.lalibre.collect.otrosmapas.gdrive.GoogleAccountsManager
import net.lalibre.collect.otrosmapas.gdrive.GoogleApiProvider
import net.lalibre.collect.otrosmapas.instancemanagement.InstanceSubmitter
import net.lalibre.collect.otrosmapas.instancemanagement.SubmitException
import net.lalibre.collect.otrosmapas.notifications.Notifier
import net.lalibre.collect.otrosmapas.projects.ProjectDependencyProvider
import net.lalibre.collect.otrosmapas.upload.FormUploadException
import net.lalibre.collect.forms.instances.Instance
import net.lalibre.collect.metadata.PropertyManager
import net.lalibre.collect.permissions.PermissionsProvider

class InstanceAutoSender(
    private val instanceAutoSendFetcher: InstanceAutoSendFetcher,
    private val context: Context,
    private val notifier: Notifier,
    private val googleAccountsManager: GoogleAccountsManager,
    private val googleApiProvider: GoogleApiProvider,
    private val permissionsProvider: PermissionsProvider,
    private val instancesDataService: InstancesDataService,
    private val propertyManager: PropertyManager
) {
    fun autoSendInstances(projectDependencyProvider: ProjectDependencyProvider): Boolean {
        val instanceSubmitter = InstanceSubmitter(
            projectDependencyProvider.formsRepository,
            googleAccountsManager,
            googleApiProvider,
            permissionsProvider,
            projectDependencyProvider.generalSettings,
            propertyManager
        )
        return projectDependencyProvider.changeLockProvider.getInstanceLock(projectDependencyProvider.projectId).withLock { acquiredLock: Boolean ->
            if (acquiredLock) {
                val toUpload = instanceAutoSendFetcher.getInstancesToAutoSend(
                    projectDependencyProvider.projectId,
                    projectDependencyProvider.instancesRepository,
                    projectDependencyProvider.formsRepository
                )

                try {
                    val result: Map<Instance, FormUploadException?> = instanceSubmitter.submitInstances(toUpload)
                    notifier.onSubmission(result, projectDependencyProvider.projectId)
                } catch (e: SubmitException) {
                    when (e.type) {
                        SubmitException.Type.GOOGLE_ACCOUNT_NOT_SET -> {
                            val result: Map<Instance, FormUploadException?> = toUpload.associateWith {
                                FormUploadException(context.getString(net.lalibre.collect.strings.R.string.google_set_account))
                            }
                            notifier.onSubmission(result, projectDependencyProvider.projectId)
                        }
                        SubmitException.Type.GOOGLE_ACCOUNT_NOT_PERMITTED -> {
                            val result: Map<Instance, FormUploadException?> = toUpload.associateWith {
                                FormUploadException(context.getString(net.lalibre.collect.strings.R.string.odk_permissions_fail))
                            }
                            notifier.onSubmission(result, projectDependencyProvider.projectId)
                        }
                        SubmitException.Type.NOTHING_TO_SUBMIT -> {
                            // do nothing
                        }
                    }
                }
                instancesDataService.update()
                true
            } else {
                false
            }
        }
    }
}
