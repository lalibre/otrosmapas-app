package net.lalibre.collect.otrosmapas.injection.config

import net.lalibre.collect.otrosmapas.geo.MapConfiguratorProvider
import net.lalibre.collect.location.LocationClient
import net.lalibre.collect.maps.MapConfigurator
import net.lalibre.collect.maps.layers.ReferenceLayerRepository
import net.lalibre.collect.osmdroid.OsmDroidDependencyModule
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.keys.ProjectKeys

class CollectOsmDroidDependencyModule(
    private val referenceLayerRepository: ReferenceLayerRepository,
    private val locationClient: LocationClient,
    private val settingsProvider: SettingsProvider
) : OsmDroidDependencyModule() {
    override fun providesReferenceLayerRepository(): ReferenceLayerRepository {
        return referenceLayerRepository
    }

    override fun providesLocationClient(): LocationClient {
        return locationClient
    }

    override fun providesMapConfigurator(): MapConfigurator {
        return MapConfiguratorProvider.getConfigurator(
            settingsProvider.getUnprotectedSettings().getString(ProjectKeys.KEY_BASEMAP_SOURCE)
        )
    }

    override fun providesSettingsProvider(): SettingsProvider {
        return settingsProvider
    }
}
