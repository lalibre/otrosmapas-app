package net.lalibre.collect.otrosmapas.audio;

import java.io.File;
import java.io.IOException;

public interface AudioFileAppender {

    void append(File one, File two) throws IOException;
}
