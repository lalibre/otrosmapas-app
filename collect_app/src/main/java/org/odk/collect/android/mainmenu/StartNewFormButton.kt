package net.lalibre.collect.otrosmapas.mainmenu

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TextView
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.otrosmapas.utilities.ApplicationConstants
import net.lalibre.collect.otrosmapasshared.ui.multiclicksafe.MultiClickGuard

class StartNewFormButton(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    constructor(context: Context) : this(context, null)

    init {
        inflate(context, R.layout.start_new_from_button, this)
    }

    val text: String
        get() = findViewById<TextView>(R.id.name).text.toString()

    override fun performClick(): Boolean {
        return MultiClickGuard.allowClick(ApplicationConstants.ScreenName.MAIN_MENU.name) && super.performClick()
    }
}
