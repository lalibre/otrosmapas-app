package net.lalibre.collect.otrosmapas.notifications

import net.lalibre.collect.otrosmapas.formmanagement.FormDownloadException
import net.lalibre.collect.otrosmapas.formmanagement.ServerFormDetails
import net.lalibre.collect.otrosmapas.upload.FormUploadException
import net.lalibre.collect.forms.FormSourceException
import net.lalibre.collect.forms.instances.Instance

interface Notifier {
    fun onUpdatesAvailable(updates: List<ServerFormDetails>, projectId: String)
    fun onUpdatesDownloaded(result: Map<ServerFormDetails, FormDownloadException?>, projectId: String)
    fun onSync(exception: FormSourceException?, projectId: String)
    fun onSubmission(result: Map<Instance, FormUploadException?>, projectId: String)
}
