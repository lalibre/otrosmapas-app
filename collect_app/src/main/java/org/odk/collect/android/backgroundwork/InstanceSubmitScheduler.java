package net.lalibre.collect.otrosmapas.backgroundwork;

public interface InstanceSubmitScheduler {

    void scheduleSubmit(String projectId);

    void cancelSubmit(String projectId);
}
