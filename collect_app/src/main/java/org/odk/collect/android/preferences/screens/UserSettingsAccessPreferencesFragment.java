package net.lalibre.collect.otrosmapas.preferences.screens;

import android.os.Bundle;

import net.lalibre.collect.otrosmapas.R;

public class UserSettingsAccessPreferencesFragment extends BaseAdminPreferencesFragment {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        setPreferencesFromResource(R.xml.user_settings_access_preferences, rootKey);
    }
}
