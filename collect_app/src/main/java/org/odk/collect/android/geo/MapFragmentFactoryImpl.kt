package net.lalibre.collect.otrosmapas.geo

import net.lalibre.collect.otrosmapas.application.MapboxClassInstanceCreator
import net.lalibre.collect.googlemaps.GoogleMapFragment
import net.lalibre.collect.maps.MapFragment
import net.lalibre.collect.maps.MapFragmentFactory
import net.lalibre.collect.osmdroid.OsmDroidMapFragment
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.keys.ProjectKeys.BASEMAP_SOURCE_CARTO
import net.lalibre.collect.settings.keys.ProjectKeys.BASEMAP_SOURCE_MAPBOX
import net.lalibre.collect.settings.keys.ProjectKeys.BASEMAP_SOURCE_OSM
import net.lalibre.collect.settings.keys.ProjectKeys.BASEMAP_SOURCE_STAMEN
import net.lalibre.collect.settings.keys.ProjectKeys.BASEMAP_SOURCE_USGS
import net.lalibre.collect.settings.keys.ProjectKeys.KEY_BASEMAP_SOURCE

class MapFragmentFactoryImpl(private val settingsProvider: SettingsProvider) : MapFragmentFactory {

    override fun createMapFragment(): MapFragment {
        val settings = settingsProvider.getUnprotectedSettings()

        return when {
            isBasemapOSM(settings.getString(KEY_BASEMAP_SOURCE)) -> OsmDroidMapFragment()
            settings.getString(KEY_BASEMAP_SOURCE) == BASEMAP_SOURCE_MAPBOX -> MapboxClassInstanceCreator.createMapboxMapFragment()
            else -> GoogleMapFragment()
        }
    }

    private fun isBasemapOSM(basemap: String?): Boolean {
        return basemap == BASEMAP_SOURCE_OSM ||
            basemap == BASEMAP_SOURCE_USGS ||
            basemap == BASEMAP_SOURCE_CARTO ||
            basemap == BASEMAP_SOURCE_STAMEN
    }
}
