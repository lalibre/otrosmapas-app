package net.lalibre.collect.otrosmapas.formmanagement.drafts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import net.lalibre.collect.otrosmapas.formmanagement.FinalizeAllResult
import net.lalibre.collect.otrosmapas.formmanagement.InstancesDataService
import net.lalibre.collect.otrosmapasshared.data.Consumable
import net.lalibre.collect.otrosmapasshared.livedata.MutableNonNullLiveData
import net.lalibre.collect.otrosmapasshared.livedata.NonNullLiveData
import net.lalibre.collect.async.Scheduler
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.keys.ProtectedProjectKeys

class BulkFinalizationViewModel(
    private val scheduler: Scheduler,
    private val instancesDataService: InstancesDataService,
    private val settingsProvider: SettingsProvider
) {
    private val _finalizedForms = MutableLiveData<Consumable<FinalizeAllResult>>()
    val finalizedForms: LiveData<Consumable<FinalizeAllResult>> = _finalizedForms

    private val _isFinalizing = MutableNonNullLiveData(false)
    val isFinalizing: NonNullLiveData<Boolean> = _isFinalizing

    val draftsCount = instancesDataService.editableCount
    val isEnabled =
        settingsProvider.getProtectedSettings().getBoolean(ProtectedProjectKeys.KEY_BULK_FINALIZE)

    fun finalizeAllDrafts() {
        _isFinalizing.value = true

        scheduler.immediate(
            background = {
                instancesDataService.finalizeAllDrafts()
            },
            foreground = {
                _isFinalizing.value = false
                _finalizedForms.value = Consumable(it)
            }
        )
    }
}
