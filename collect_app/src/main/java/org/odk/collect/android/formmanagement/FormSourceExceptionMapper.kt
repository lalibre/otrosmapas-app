package net.lalibre.collect.otrosmapas.formmanagement

import android.content.Context
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.forms.FormSourceException
import net.lalibre.collect.strings.localization.getLocalizedString

class FormSourceExceptionMapper(private val context: Context) {
    fun getMessage(exception: FormSourceException?): String {
        return when (exception) {
            is FormSourceException.Unreachable -> {
                context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.unreachable_error,
                    exception.serverUrl
                ) + " " + context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.report_to_project_lead
                )
            }
            is FormSourceException.SecurityError -> {
                context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.security_error,
                    exception.serverUrl
                ) + " " + context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.report_to_project_lead
                )
            }
            is FormSourceException.ServerError -> {
                context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.server_error,
                    exception.serverUrl,
                    exception.statusCode
                ) + " " + context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.report_to_project_lead
                )
            }
            is FormSourceException.ParseError -> {
                context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.invalid_response,
                    exception.serverUrl
                ) + " " + context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.report_to_project_lead
                )
            }
            is FormSourceException.ServerNotOpenRosaError -> {
                "This server does not correctly implement the OpenRosa formList API." + " " + context.getLocalizedString(
                    net.lalibre.collect.strings.R.string.report_to_project_lead
                )
            }
            else -> {
                context.getLocalizedString(net.lalibre.collect.strings.R.string.report_to_project_lead)
            }
        }
    }
}
