package net.lalibre.collect.otrosmapas.formentry.audit

import org.javarosa.form.api.FormEntryController
import net.lalibre.collect.otrosmapas.javarosawrapper.FormController
import net.lalibre.collect.otrosmapas.javarosawrapper.RepeatsInFieldListException

object AuditUtils {
    @JvmStatic
    fun logCurrentScreen(
        formController: FormController,
        auditEventLogger: AuditEventLogger,
        currentTime: Long
    ) {
        if (formController.getEvent() == FormEntryController.EVENT_QUESTION ||
            formController.getEvent() == FormEntryController.EVENT_GROUP ||
            formController.getEvent() == FormEntryController.EVENT_REPEAT
        ) {
            try {
                for (question in formController.getQuestionPrompts()) {
                    val answer =
                        if (question.answerValue != null) {
                            question.answerValue!!.displayText
                        } else {
                            null
                        }

                    auditEventLogger.logEvent(
                        AuditEvent.AuditEventType.QUESTION,
                        question.index,
                        true,
                        answer,
                        currentTime,
                        null
                    )
                }
            } catch (e: RepeatsInFieldListException) {
                // ignore
            }
        }
    }
}
