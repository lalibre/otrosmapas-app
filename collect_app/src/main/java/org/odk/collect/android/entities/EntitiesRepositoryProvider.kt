package net.lalibre.collect.otrosmapas.entities

import android.app.Application
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService
import net.lalibre.collect.otrosmapasshared.data.getState
import net.lalibre.collect.entities.EntitiesRepository

class EntitiesRepositoryProvider(application: Application, private val projectsDataService: ProjectsDataService) {

    private val repositories =
        application.getState().get(MAP_KEY, mutableMapOf<String, EntitiesRepository>())

    fun get(projectId: String = projectsDataService.getCurrentProject().uuid): EntitiesRepository {
        return repositories.getOrPut(projectId) {
            InMemEntitiesRepository()
        }
    }

    companion object {
        private const val MAP_KEY = "entities_repository_map"
    }
}
