package net.lalibre.collect.otrosmapas.injection.config

import net.lalibre.collect.permissions.PermissionsChecker
import net.lalibre.collect.selfiecamera.SelfieCameraDependencyModule

class CollectSelfieCameraDependencyModule(private val permissionsChecker: () -> PermissionsChecker) : SelfieCameraDependencyModule() {
    override fun providesPermissionChecker(): PermissionsChecker {
        return permissionsChecker()
    }
}
