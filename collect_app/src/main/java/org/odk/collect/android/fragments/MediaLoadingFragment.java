package net.lalibre.collect.otrosmapas.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;
import net.lalibre.collect.otrosmapas.activities.FormFillingActivity;
import net.lalibre.collect.otrosmapas.javarosawrapper.FormController;
import net.lalibre.collect.otrosmapas.tasks.MediaLoadingTask;

public class MediaLoadingFragment extends Fragment {

    private MediaLoadingTask mediaLoadingTask;

    public void beginMediaLoadingTask(Uri uri, FormController formController) {
        mediaLoadingTask = new MediaLoadingTask((FormFillingActivity) getActivity(), formController.getInstanceFile());
        mediaLoadingTask.execute(uri);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (mediaLoadingTask != null) {
            mediaLoadingTask.onAttach((FormFillingActivity) getActivity());
        }
    }

    public boolean isMediaLoadingTaskRunning() {
        return mediaLoadingTask != null && mediaLoadingTask.getStatus() == AsyncTask.Status.RUNNING;
    }
}
