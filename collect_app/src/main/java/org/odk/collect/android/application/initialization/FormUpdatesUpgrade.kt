package net.lalibre.collect.otrosmapas.application.initialization

import net.lalibre.collect.otrosmapas.backgroundwork.FormUpdateScheduler
import net.lalibre.collect.async.Scheduler
import net.lalibre.collect.projects.ProjectsRepository
import net.lalibre.collect.upgrade.Upgrade

class FormUpdatesUpgrade(
    private val scheduler: Scheduler,
    private val projectsRepository: ProjectsRepository,
    private val formUpdateScheduler: FormUpdateScheduler
) : Upgrade {

    override fun key(): String? {
        return null
    }

    override fun run() {
        scheduler.cancelAllDeferred()

        projectsRepository.getAll().forEach {
            formUpdateScheduler.scheduleUpdates(it.uuid)
        }
    }
}
