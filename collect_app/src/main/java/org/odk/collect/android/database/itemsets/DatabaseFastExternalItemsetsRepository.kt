package net.lalibre.collect.otrosmapas.database.itemsets

import net.lalibre.collect.otrosmapas.fastexternalitemset.ItemsetDbAdapter
import net.lalibre.collect.otrosmapas.itemsets.FastExternalItemsetsRepository

class DatabaseFastExternalItemsetsRepository : FastExternalItemsetsRepository {

    override fun deleteAllByCsvPath(path: String) {
        ItemsetDbAdapter().open().use {
            it.delete(path)
        }
    }
}
