package net.lalibre.collect.otrosmapas.listeners;

import net.lalibre.collect.otrosmapas.widgets.QuestionWidget;

public interface WidgetValueChangedListener {
    void widgetValueChanged(QuestionWidget changedWidget);
}
