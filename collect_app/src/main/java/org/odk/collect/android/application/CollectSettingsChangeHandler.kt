package net.lalibre.collect.otrosmapas.application

import net.lalibre.collect.otrosmapas.analytics.AnalyticsUtils
import net.lalibre.collect.otrosmapas.backgroundwork.FormUpdateScheduler
import net.lalibre.collect.otrosmapas.formmanagement.FormsDataService
import net.lalibre.collect.metadata.PropertyManager
import net.lalibre.collect.settings.importing.SettingsChangeHandler
import net.lalibre.collect.settings.keys.ProjectKeys

class CollectSettingsChangeHandler(
    private val propertyManager: PropertyManager,
    private val formUpdateScheduler: FormUpdateScheduler,
    private val formsDataService: FormsDataService
) : SettingsChangeHandler {

    override fun onSettingChanged(projectId: String, newValue: Any?, changedKey: String) {
        propertyManager.reload()

        if (changedKey == ProjectKeys.KEY_SERVER_URL || changedKey == ProjectKeys.KEY_PROTOCOL) {
            formsDataService.clear(projectId)
        }

        if (changedKey == ProjectKeys.KEY_FORM_UPDATE_MODE ||
            changedKey == ProjectKeys.KEY_PERIODIC_FORM_UPDATES_CHECK ||
            changedKey == ProjectKeys.KEY_PROTOCOL
        ) {
            formUpdateScheduler.scheduleUpdates(projectId)
        }

        if (changedKey == ProjectKeys.KEY_SERVER_URL) {
            AnalyticsUtils.logServerConfiguration(newValue.toString())
        }
    }

    override fun onSettingsChanged(projectId: String) {
        propertyManager.reload()
        formUpdateScheduler.scheduleUpdates(projectId)
    }
}
