package net.lalibre.collect.otrosmapas.mainmenu

import androidx.lifecycle.ViewModel
import net.lalibre.collect.analytics.Analytics
import net.lalibre.collect.otrosmapas.analytics.AnalyticsEvents
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService
import net.lalibre.collect.otrosmapasshared.livedata.MutableNonNullLiveData
import net.lalibre.collect.otrosmapasshared.livedata.NonNullLiveData
import net.lalibre.collect.projects.Project

class CurrentProjectViewModel(
    private val projectsDataService: ProjectsDataService
) : ViewModel() {

    private val _currentProject by lazy { MutableNonNullLiveData(projectsDataService.getCurrentProject()) }
    val currentProject: NonNullLiveData<Project.Saved> by lazy { _currentProject }

    fun setCurrentProject(project: Project.Saved) {
        Analytics.log(AnalyticsEvents.SWITCH_PROJECT)
        projectsDataService.setCurrentProject(project.uuid)
        refresh()
    }

    fun refresh() {
        if (currentProject.value != projectsDataService.getCurrentProject()) {
            _currentProject.postValue(projectsDataService.getCurrentProject())
        }
    }

    fun hasCurrentProject(): Boolean {
        return try {
            projectsDataService.getCurrentProject()
            true
        } catch (e: IllegalStateException) {
            false
        }
    }
}
