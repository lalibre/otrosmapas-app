package net.lalibre.collect.otrosmapas.instancemanagement.autosend

import net.lalibre.collect.otrosmapas.analytics.AnalyticsEvents
import net.lalibre.collect.otrosmapas.analytics.AnalyticsUtils
import net.lalibre.collect.forms.Form

fun Form.shouldFormBeSentAutomatically(isAutoSendEnabledInSettings: Boolean): Boolean {
    if (!autoSend.isNullOrEmpty()) {
        AnalyticsUtils.logFormEvent(AnalyticsEvents.FORM_LEVEL_AUTO_SEND, formId, displayName)
    }

    return if (isAutoSendEnabledInSettings) {
        autoSend == null || autoSend.trim().lowercase() != "false"
    } else {
        autoSend != null && autoSend.trim().lowercase() == "true"
    }
}
