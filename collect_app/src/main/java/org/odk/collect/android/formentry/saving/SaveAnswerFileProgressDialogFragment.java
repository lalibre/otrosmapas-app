package net.lalibre.collect.otrosmapas.formentry.saving;

import android.content.Context;

import androidx.annotation.NonNull;

import net.lalibre.collect.material.MaterialProgressDialogFragment;

public class SaveAnswerFileProgressDialogFragment extends MaterialProgressDialogFragment {

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setMessage(getString(net.lalibre.collect.strings.R.string.saving_file));
    }
}
