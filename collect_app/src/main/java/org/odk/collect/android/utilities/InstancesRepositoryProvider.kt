package net.lalibre.collect.otrosmapas.utilities

import android.content.Context
import net.lalibre.collect.otrosmapas.database.instances.DatabaseInstancesRepository
import net.lalibre.collect.otrosmapas.storage.StoragePathProvider
import net.lalibre.collect.otrosmapas.storage.StorageSubdirectory
import net.lalibre.collect.forms.instances.InstancesRepository

class InstancesRepositoryProvider @JvmOverloads constructor(
    private val context: Context,
    private val storagePathProvider: StoragePathProvider = StoragePathProvider()
) {

    @JvmOverloads
    fun get(projectId: String? = null): InstancesRepository {
        return DatabaseInstancesRepository(
            context,
            storagePathProvider.getOdkDirPath(StorageSubdirectory.METADATA, projectId),
            storagePathProvider.getOdkDirPath(StorageSubdirectory.INSTANCES, projectId),
            System::currentTimeMillis
        )
    }
}
