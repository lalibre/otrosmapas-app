package net.lalibre.collect.otrosmapas.formmanagement

import net.lalibre.collect.otrosmapas.openrosa.OpenRosaFormSource
import net.lalibre.collect.otrosmapas.openrosa.OpenRosaHttpInterface
import net.lalibre.collect.otrosmapas.openrosa.OpenRosaResponseParserImpl
import net.lalibre.collect.otrosmapas.utilities.WebCredentialsUtils
import net.lalibre.collect.forms.FormSource
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.keys.ProjectKeys

class FormSourceProvider(
    private val settingsProvider: SettingsProvider,
    private val openRosaHttpInterface: OpenRosaHttpInterface
) {

    @JvmOverloads
    fun get(projectId: String? = null): FormSource {
        val generalSettings = settingsProvider.getUnprotectedSettings(projectId)

        val serverURL = generalSettings.getString(ProjectKeys.KEY_SERVER_URL)

        return OpenRosaFormSource(
            serverURL,
            openRosaHttpInterface,
            WebCredentialsUtils(generalSettings),
            OpenRosaResponseParserImpl()
        )
    }
}
