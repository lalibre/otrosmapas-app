/*
 * Copyright (C) 2017 University of Washington
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.lalibre.collect.otrosmapas.activities

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import net.lalibre.collect.otrosmapas.adapters.DeleteFormsTabsAdapter
import net.lalibre.collect.otrosmapas.databinding.TabsLayoutBinding
import net.lalibre.collect.otrosmapas.formlists.blankformlist.BlankFormListViewModel
import net.lalibre.collect.otrosmapas.formlists.blankformlist.DeleteBlankFormFragment
import net.lalibre.collect.otrosmapas.formmanagement.FormsDataService
import net.lalibre.collect.otrosmapas.injection.DaggerUtils
import net.lalibre.collect.otrosmapas.projects.ProjectDependencyProviderFactory
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService
import net.lalibre.collect.otrosmapasshared.ui.FragmentFactoryBuilder
import net.lalibre.collect.otrosmapasshared.ui.MultiSelectViewModel
import net.lalibre.collect.otrosmapasshared.utils.AppBarUtils.setupAppBarLayout
import net.lalibre.collect.async.Scheduler
import net.lalibre.collect.forms.instances.InstancesRepository
import net.lalibre.collect.shared.settings.Settings
import net.lalibre.collect.strings.localization.LocalizedActivity
import javax.inject.Inject

class DeleteSavedFormActivity : LocalizedActivity() {
    @Inject
    lateinit var projectDependencyProviderFactory: ProjectDependencyProviderFactory

    @Inject
    lateinit var projectsDataService: ProjectsDataService

    @Inject
    lateinit var formsDataService: FormsDataService

    @Inject
    lateinit var scheduler: Scheduler

    private lateinit var binding: TabsLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerUtils.getComponent(this).inject(this)

        val projectId = projectsDataService.getCurrentProject().uuid
        val projectDependencyProvider = projectDependencyProviderFactory.create(projectId)

        val viewModelFactory = ViewModelFactory(
            projectDependencyProvider.instancesRepository,
            this.application,
            formsDataService,
            scheduler,
            projectDependencyProvider.generalSettings,
            projectId
        )

        val viewModelProvider = ViewModelProvider(this, viewModelFactory)
        val blankFormsListViewModel = viewModelProvider[BlankFormListViewModel::class.java]

        supportFragmentManager.fragmentFactory = FragmentFactoryBuilder()
            .forClass(DeleteBlankFormFragment::class) {
                DeleteBlankFormFragment(viewModelFactory, this)
            }
            .build()

        super.onCreate(savedInstanceState)
        binding = TabsLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupAppBarLayout(this, getString(net.lalibre.collect.strings.R.string.manage_files))
        setUpViewPager(blankFormsListViewModel)
    }

    private fun setUpViewPager(viewModel: BlankFormListViewModel) {
        val viewPager = binding.viewPager.apply {
            adapter = DeleteFormsTabsAdapter(
                this@DeleteSavedFormActivity,
                viewModel.isMatchExactlyEnabled()
            )
        }

        TabLayoutMediator(binding.tabLayout, viewPager) { tab: TabLayout.Tab, position: Int ->
            tab.text = if (position == 0) {
                getString(net.lalibre.collect.strings.R.string.data)
            } else {
                getString(net.lalibre.collect.strings.R.string.forms)
            }
        }.attach()
    }

    private class ViewModelFactory(
        private val instancesRepository: InstancesRepository,
        private val application: Application,
        private val formsDataService: FormsDataService,
        private val scheduler: Scheduler,
        private val generalSettings: Settings,
        private val projectId: String
    ) :
        ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
            return when (modelClass) {
                BlankFormListViewModel::class.java -> BlankFormListViewModel(
                    instancesRepository,
                    application,
                    formsDataService,
                    scheduler,
                    generalSettings,
                    projectId,
                    showAllVersions = true
                )

                MultiSelectViewModel::class.java -> MultiSelectViewModel()
                else -> throw IllegalArgumentException()
            } as T
        }
    }
}
