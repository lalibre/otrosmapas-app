package net.lalibre.collect.otrosmapas.notifications.builders

import android.app.Application
import android.app.Notification
import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.otrosmapas.formlists.blankformlist.BlankFormListActivity
import net.lalibre.collect.otrosmapas.formmanagement.FormDownloadException
import net.lalibre.collect.otrosmapas.formmanagement.ServerFormDetails
import net.lalibre.collect.otrosmapas.notifications.NotificationManagerNotifier
import net.lalibre.collect.otrosmapas.utilities.ApplicationConstants
import net.lalibre.collect.otrosmapas.utilities.FormsDownloadResultInterpreter
import net.lalibre.collect.errors.ErrorActivity
import net.lalibre.collect.strings.localization.getLocalizedString
import java.io.Serializable

object FormUpdatesDownloadedNotificationBuilder {

    fun build(application: Application, result: Map<ServerFormDetails, FormDownloadException?>, projectName: String): Notification {
        val allFormsDownloadedSuccessfully = FormsDownloadResultInterpreter.allFormsDownloadedSuccessfully(result)

        val intent = if (allFormsDownloadedSuccessfully) {
            Intent(application, BlankFormListActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            }
        } else {
            Intent(application, ErrorActivity::class.java).apply {
                putExtra(ErrorActivity.EXTRA_ERRORS, FormsDownloadResultInterpreter.getFailures(result, application) as Serializable)
            }
        }

        val contentIntent = PendingIntent.getActivity(
            application,
            ApplicationConstants.RequestCodes.FORMS_DOWNLOADED_NOTIFICATION,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )

        val title =
            if (allFormsDownloadedSuccessfully) {
                application.getLocalizedString(net.lalibre.collect.strings.R.string.forms_download_succeeded)
            } else {
                application.getLocalizedString(net.lalibre.collect.strings.R.string.forms_download_failed)
            }

        val message =
            if (allFormsDownloadedSuccessfully) {
                application.getLocalizedString(net.lalibre.collect.strings.R.string.all_downloads_succeeded)
            } else {
                application.getLocalizedString(
                    net.lalibre.collect.strings.R.string.some_downloads_failed,
                    FormsDownloadResultInterpreter.getNumberOfFailures(result),
                    result.size
                )
            }

        return NotificationCompat.Builder(
            application,
            NotificationManagerNotifier.COLLECT_NOTIFICATION_CHANNEL
        ).apply {
            setContentIntent(contentIntent)
            setContentTitle(title)
            setContentText(message)
            setSubText(projectName)
            setSmallIcon(net.lalibre.collect.icons.R.drawable.ic_notification_small)
            setAutoCancel(true)
        }.build()
    }
}
