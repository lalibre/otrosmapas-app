package net.lalibre.collect.otrosmapas.mainmenu

import android.app.Application
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import net.lalibre.collect.otrosmapas.formmanagement.InstancesDataService
import net.lalibre.collect.otrosmapas.instancemanagement.InstanceDiskSynchronizer
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.AutoSendSettingsProvider
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.shouldFormBeSentAutomatically
import net.lalibre.collect.otrosmapas.instancemanagement.canBeEdited
import net.lalibre.collect.otrosmapas.instancemanagement.isDraft
import net.lalibre.collect.otrosmapas.preferences.utilities.FormUpdateMode
import net.lalibre.collect.otrosmapas.preferences.utilities.SettingsUtils
import net.lalibre.collect.otrosmapas.utilities.ContentUriHelper
import net.lalibre.collect.otrosmapas.utilities.FormsRepositoryProvider
import net.lalibre.collect.otrosmapas.utilities.InstancesRepositoryProvider
import net.lalibre.collect.otrosmapas.version.VersionInformation
import net.lalibre.collect.async.Scheduler
import net.lalibre.collect.forms.instances.Instance
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.keys.ProtectedProjectKeys

class MainMenuViewModel(
    private val application: Application,
    private val versionInformation: VersionInformation,
    private val settingsProvider: SettingsProvider,
    private val instancesDataService: InstancesDataService,
    private val scheduler: Scheduler,
    private val formsRepositoryProvider: FormsRepositoryProvider,
    private val instancesRepositoryProvider: InstancesRepositoryProvider,
    private val autoSendSettingsProvider: AutoSendSettingsProvider
) : ViewModel() {

    val version: String
        get() = versionInformation.versionToDisplay

    val versionCommitDescription: String?
        get() {
            var commitDescription = ""
            if (versionInformation.commitCount != null) {
                commitDescription =
                    appendToCommitDescription(commitDescription, versionInformation.commitCount.toString())
            }
            if (versionInformation.commitSHA != null) {
                commitDescription =
                    appendToCommitDescription(commitDescription, versionInformation.commitSHA!!)
            }
            if (versionInformation.isDirty) {
                commitDescription = appendToCommitDescription(commitDescription, "dirty")
            }
            return if (commitDescription.isNotEmpty()) {
                commitDescription
            } else {
                null
            }
        }

    fun shouldEditSavedFormButtonBeVisible(): Boolean {
        return settingsProvider.getProtectedSettings().getBoolean(ProtectedProjectKeys.KEY_EDIT_SAVED)
    }

    fun shouldSendFinalizedFormButtonBeVisible(): Boolean {
        return settingsProvider.getProtectedSettings().getBoolean(ProtectedProjectKeys.KEY_SEND_FINALIZED)
    }

    fun shouldViewSentFormButtonBeVisible(): Boolean {
        return settingsProvider.getProtectedSettings().getBoolean(ProtectedProjectKeys.KEY_VIEW_SENT)
    }

    fun shouldGetBlankFormButtonBeVisible(): Boolean {
        val buttonEnabled = settingsProvider.getProtectedSettings().getBoolean(ProtectedProjectKeys.KEY_GET_BLANK)
        return !isMatchExactlyEnabled() && buttonEnabled
    }

    fun shouldDeleteSavedFormButtonBeVisible(): Boolean {
        return settingsProvider.getProtectedSettings().getBoolean(ProtectedProjectKeys.KEY_DELETE_SAVED)
    }

    private fun isMatchExactlyEnabled(): Boolean {
        return SettingsUtils.getFormUpdateMode(application, settingsProvider.getUnprotectedSettings()) == FormUpdateMode.MATCH_EXACTLY
    }

    private fun appendToCommitDescription(commitDescription: String, part: String): String {
        return if (commitDescription.isEmpty()) {
            part
        } else {
            "$commitDescription-$part"
        }
    }

    fun refreshInstances() {
        scheduler.immediate<Any?>({
            InstanceDiskSynchronizer(settingsProvider).doInBackground()
            instancesDataService.update()
            null
        }) { }
    }

    val editableInstancesCount: LiveData<Int>
        get() = instancesDataService.editableCount

    val sendableInstancesCount: LiveData<Int>
        get() = instancesDataService.sendableCount

    val sentInstancesCount: LiveData<Int>
        get() = instancesDataService.sentCount

    fun getFormSavedSnackbarDetails(uri: Uri): Pair<Int, Int?>? {
        val instance = instancesRepositoryProvider.get().get(ContentUriHelper.getIdFromUri(uri))
        return if (instance != null) {
            val message = if (instance.isDraft()) {
                net.lalibre.collect.strings.R.string.form_saved_as_draft
            } else if (instance.status == Instance.STATUS_COMPLETE || instance.status == Instance.STATUS_SUBMISSION_FAILED) {
                val form = formsRepositoryProvider.get().getAllByFormIdAndVersion(instance.formId, instance.formVersion).first()
                if (form.shouldFormBeSentAutomatically(autoSendSettingsProvider.isAutoSendEnabledInSettings())) {
                    net.lalibre.collect.strings.R.string.form_sending
                } else {
                    net.lalibre.collect.strings.R.string.form_saved
                }
            } else {
                return null
            }

            val action = if (instance.canBeEdited(settingsProvider)) {
                net.lalibre.collect.strings.R.string.edit_form
            } else {
                if (instance.isDraft() || instance.canEditWhenComplete()) {
                    net.lalibre.collect.strings.R.string.view_form
                } else {
                    null
                }
            }

            return Pair(message, action)
        } else {
            null
        }
    }
}
