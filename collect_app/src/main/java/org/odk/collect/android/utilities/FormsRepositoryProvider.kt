package net.lalibre.collect.otrosmapas.utilities

import android.content.Context
import net.lalibre.collect.otrosmapas.database.forms.DatabaseFormsRepository
import net.lalibre.collect.otrosmapas.storage.StoragePathProvider
import net.lalibre.collect.otrosmapas.storage.StorageSubdirectory
import net.lalibre.collect.forms.FormsRepository

class FormsRepositoryProvider @JvmOverloads constructor(
    private val context: Context,
    private val storagePathProvider: StoragePathProvider = StoragePathProvider()
) {

    private val clock = { System.currentTimeMillis() }

    @JvmOverloads
    fun get(projectId: String? = null): FormsRepository {
        val dbPath = storagePathProvider.getOdkDirPath(StorageSubdirectory.METADATA, projectId)
        val formsPath = storagePathProvider.getOdkDirPath(StorageSubdirectory.FORMS, projectId)
        val cachePath = storagePathProvider.getOdkDirPath(StorageSubdirectory.CACHE, projectId)
        return DatabaseFormsRepository(context, dbPath, formsPath, cachePath, clock)
    }
}
