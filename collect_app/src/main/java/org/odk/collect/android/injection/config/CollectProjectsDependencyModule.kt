package net.lalibre.collect.otrosmapas.injection.config

import net.lalibre.collect.projects.ProjectsDependencyModule
import net.lalibre.collect.projects.ProjectsRepository

class CollectProjectsDependencyModule(private val projectsRepository: ProjectsRepository) :
    ProjectsDependencyModule() {
    override fun providesProjectsRepository(): ProjectsRepository {
        return projectsRepository
    }
}
