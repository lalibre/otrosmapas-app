package net.lalibre.collect.otrosmapas.formentry.saving;

import android.net.Uri;

import net.lalibre.collect.otrosmapas.javarosawrapper.FormController;
import net.lalibre.collect.otrosmapas.tasks.SaveToDiskResult;
import net.lalibre.collect.otrosmapas.utilities.MediaUtils;
import net.lalibre.collect.entities.EntitiesRepository;
import net.lalibre.collect.forms.instances.InstancesRepository;

import java.util.ArrayList;

public interface FormSaver {
    SaveToDiskResult save(Uri instanceContentURI, FormController formController, MediaUtils mediaUtils, boolean shouldFinalize, boolean exitAfter,
                          String updatedSaveName, ProgressListener progressListener, ArrayList<String> tempFiles, String currentProjectId, EntitiesRepository entitiesRepository, InstancesRepository instancesRepository);

    interface ProgressListener {
        void onProgressUpdate(String message);
    }
}
