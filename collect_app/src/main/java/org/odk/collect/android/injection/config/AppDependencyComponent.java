package net.lalibre.collect.otrosmapas.injection.config;

import android.app.Application;

import org.javarosa.core.reference.ReferenceManager;
import net.lalibre.collect.otrosmapas.activities.AboutActivity;
import net.lalibre.collect.otrosmapas.activities.AppListActivity;
import net.lalibre.collect.otrosmapas.activities.DeleteSavedFormActivity;
import net.lalibre.collect.otrosmapas.activities.FirstLaunchActivity;
import net.lalibre.collect.otrosmapas.activities.FormDownloadListActivity;
import net.lalibre.collect.otrosmapas.activities.FormFillingActivity;
import net.lalibre.collect.otrosmapas.activities.FormHierarchyActivity;
import net.lalibre.collect.otrosmapas.activities.FormMapActivity;
import net.lalibre.collect.otrosmapas.activities.InstanceChooserList;
import net.lalibre.collect.otrosmapas.instancemanagement.send.InstanceUploaderActivity;
import net.lalibre.collect.otrosmapas.instancemanagement.send.InstanceUploaderListActivity;
import net.lalibre.collect.otrosmapas.adapters.InstanceUploaderAdapter;
import net.lalibre.collect.otrosmapas.application.Collect;
import net.lalibre.collect.otrosmapas.application.initialization.ApplicationInitializer;
import net.lalibre.collect.otrosmapas.application.initialization.ExistingProjectMigrator;
import net.lalibre.collect.otrosmapas.audio.AudioRecordingControllerFragment;
import net.lalibre.collect.otrosmapas.audio.AudioRecordingErrorDialogFragment;
import net.lalibre.collect.otrosmapas.backgroundwork.AutoSendTaskSpec;
import net.lalibre.collect.otrosmapas.backgroundwork.AutoUpdateTaskSpec;
import net.lalibre.collect.otrosmapas.backgroundwork.SyncFormsTaskSpec;
import net.lalibre.collect.otrosmapas.configure.qr.QRCodeScannerFragment;
import net.lalibre.collect.otrosmapas.configure.qr.QRCodeTabsActivity;
import net.lalibre.collect.otrosmapas.configure.qr.ShowQRCodeFragment;
import net.lalibre.collect.otrosmapas.draw.DrawActivity;
import net.lalibre.collect.otrosmapas.draw.PenColorPickerDialog;
import net.lalibre.collect.otrosmapas.entities.EntitiesRepositoryProvider;
import net.lalibre.collect.otrosmapas.external.AndroidShortcutsActivity;
import net.lalibre.collect.otrosmapas.external.FormUriActivity;
import net.lalibre.collect.otrosmapas.external.FormsProvider;
import net.lalibre.collect.otrosmapas.external.InstanceProvider;
import net.lalibre.collect.otrosmapas.formentry.BackgroundAudioPermissionDialogFragment;
import net.lalibre.collect.otrosmapas.formentry.ODKView;
import net.lalibre.collect.otrosmapas.formentry.repeats.DeleteRepeatDialogFragment;
import net.lalibre.collect.otrosmapas.formentry.saving.SaveAnswerFileErrorDialogFragment;
import net.lalibre.collect.otrosmapas.formentry.saving.SaveFormProgressDialogFragment;
import net.lalibre.collect.otrosmapas.formlists.blankformlist.BlankFormListActivity;
import net.lalibre.collect.otrosmapas.formmanagement.FormSourceProvider;
import net.lalibre.collect.otrosmapas.formmanagement.FormsDataService;
import net.lalibre.collect.otrosmapas.fragments.AppListFragment;
import net.lalibre.collect.otrosmapas.fragments.BarCodeScannerFragment;
import net.lalibre.collect.otrosmapas.fragments.SavedFormListFragment;
import net.lalibre.collect.otrosmapas.fragments.dialogs.FormsDownloadResultDialog;
import net.lalibre.collect.otrosmapas.fragments.dialogs.SelectMinimalDialog;
import net.lalibre.collect.otrosmapas.gdrive.GoogleSheetsUploaderActivity;
import net.lalibre.collect.googlemaps.GoogleMapFragment;
import net.lalibre.collect.otrosmapas.mainmenu.MainMenuActivity;
import net.lalibre.collect.otrosmapas.openrosa.OpenRosaHttpInterface;
import net.lalibre.collect.otrosmapas.preferences.CaptionedListPreference;
import net.lalibre.collect.otrosmapas.preferences.dialogs.AdminPasswordDialogFragment;
import net.lalibre.collect.otrosmapas.preferences.dialogs.ChangeAdminPasswordDialog;
import net.lalibre.collect.otrosmapas.preferences.dialogs.ResetDialogPreferenceFragmentCompat;
import net.lalibre.collect.otrosmapas.preferences.dialogs.ServerAuthDialogFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.BaseAdminPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.BasePreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.BaseProjectPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.ExperimentalPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.FormManagementPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.FormMetadataPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.IdentityPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.MapsPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.ProjectDisplayPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.ProjectManagementPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.ProjectPreferencesActivity;
import net.lalibre.collect.otrosmapas.preferences.screens.ProjectPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.ServerPreferencesFragment;
import net.lalibre.collect.otrosmapas.preferences.screens.UserInterfacePreferencesFragment;
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService;
import net.lalibre.collect.otrosmapas.projects.ManualProjectCreatorDialog;
import net.lalibre.collect.otrosmapas.projects.ProjectSettingsDialog;
import net.lalibre.collect.otrosmapas.projects.QrCodeProjectCreatorDialog;
import net.lalibre.collect.otrosmapas.storage.StoragePathProvider;
import net.lalibre.collect.otrosmapas.tasks.DownloadFormListTask;
import net.lalibre.collect.otrosmapas.tasks.InstanceServerUploaderTask;
import net.lalibre.collect.otrosmapas.tasks.MediaLoadingTask;
import net.lalibre.collect.otrosmapas.upload.InstanceUploader;
import net.lalibre.collect.otrosmapas.utilities.AuthDialogUtility;
import net.lalibre.collect.otrosmapas.utilities.FormsRepositoryProvider;
import net.lalibre.collect.otrosmapas.utilities.InstancesRepositoryProvider;
import net.lalibre.collect.otrosmapas.utilities.ProjectResetter;
import net.lalibre.collect.otrosmapas.utilities.ThemeUtils;
import net.lalibre.collect.otrosmapas.widgets.ExStringWidget;
import net.lalibre.collect.otrosmapas.widgets.QuestionWidget;
import net.lalibre.collect.otrosmapas.widgets.items.SelectOneFromMapDialogFragment;
import net.lalibre.collect.otrosmapasshared.network.NetworkStateProvider;
import net.lalibre.collect.async.Scheduler;
import net.lalibre.collect.location.LocationClient;
import net.lalibre.collect.maps.MapFragmentFactory;
import net.lalibre.collect.maps.layers.ReferenceLayerRepository;
import net.lalibre.collect.permissions.PermissionsChecker;
import net.lalibre.collect.permissions.PermissionsProvider;
import net.lalibre.collect.projects.ProjectsRepository;
import net.lalibre.collect.settings.ODKAppSettingsImporter;
import net.lalibre.collect.settings.SettingsProvider;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Dagger component for the application. Should include
 * application level Dagger Modules and be built with Application
 * object.
 * <p>
 * Add an `inject(MyClass myClass)` method here for objects you want
 * to inject into so Dagger knows to wire it up.
 * <p>
 * Annotated with @Singleton so modules can include @Singletons that will
 * be retained at an application level (as this an instance of this components
 * is owned by the Application object).
 * <p>
 * If you need to call a provider directly from the component (in a test
 * for example) you can add a method with the type you are looking to fetch
 * (`MyType myType()`) to this interface.
 * <p>
 * To read more about Dagger visit: https://google.github.io/dagger/users-guide
 **/

@Singleton
@Component(modules = {
        AppDependencyModule.class
})
public interface AppDependencyComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        Builder appDependencyModule(AppDependencyModule testDependencyModule);

        AppDependencyComponent build();
    }

    void inject(Collect collect);

    void inject(AboutActivity aboutActivity);

    void inject(InstanceUploaderAdapter instanceUploaderAdapter);

    void inject(SavedFormListFragment savedFormListFragment);

    void inject(FormFillingActivity formFillingActivity);

    void inject(InstanceServerUploaderTask uploader);

    void inject(ServerPreferencesFragment serverPreferencesFragment);

    void inject(ProjectDisplayPreferencesFragment projectDisplayPreferencesFragment);

    void inject(ProjectManagementPreferencesFragment projectManagementPreferencesFragment);

    void inject(AuthDialogUtility authDialogUtility);

    void inject(FormDownloadListActivity formDownloadListActivity);

    void inject(InstanceUploaderListActivity activity);

    void inject(GoogleSheetsUploaderActivity googleSheetsUploaderActivity);

    void inject(QuestionWidget questionWidget);

    void inject(ExStringWidget exStringWidget);

    void inject(ODKView odkView);

    void inject(FormMetadataPreferencesFragment formMetadataPreferencesFragment);

    void inject(FormMapActivity formMapActivity);

    void inject(GoogleMapFragment mapFragment);

    void inject(MainMenuActivity mainMenuActivity);

    void inject(QRCodeTabsActivity qrCodeTabsActivity);

    void inject(ShowQRCodeFragment showQRCodeFragment);

    void inject(AutoSendTaskSpec autoSendTaskSpec);

    void inject(AdminPasswordDialogFragment adminPasswordDialogFragment);

    void inject(FormHierarchyActivity formHierarchyActivity);

    void inject(FormManagementPreferencesFragment formManagementPreferencesFragment);

    void inject(IdentityPreferencesFragment identityPreferencesFragment);

    void inject(UserInterfacePreferencesFragment userInterfacePreferencesFragment);

    void inject(SaveFormProgressDialogFragment saveFormProgressDialogFragment);

    void inject(BarCodeScannerFragment barCodeScannerFragment);

    void inject(QRCodeScannerFragment qrCodeScannerFragment);

    void inject(ProjectPreferencesActivity projectPreferencesActivity);

    void inject(ResetDialogPreferenceFragmentCompat resetDialogPreferenceFragmentCompat);

    void inject(SyncFormsTaskSpec syncWork);

    void inject(ExperimentalPreferencesFragment experimentalPreferencesFragment);

    void inject(AutoUpdateTaskSpec autoUpdateTaskSpec);

    void inject(ServerAuthDialogFragment serverAuthDialogFragment);

    void inject(BasePreferencesFragment basePreferencesFragment);

    void inject(InstanceUploaderActivity instanceUploaderActivity);

    void inject(ProjectPreferencesFragment projectPreferencesFragment);

    void inject(DeleteSavedFormActivity deleteSavedFormActivity);

    void inject(SelectMinimalDialog selectMinimalDialog);

    void inject(AudioRecordingControllerFragment audioRecordingControllerFragment);

    void inject(SaveAnswerFileErrorDialogFragment saveAnswerFileErrorDialogFragment);

    void inject(AudioRecordingErrorDialogFragment audioRecordingErrorDialogFragment);

    void inject(InstanceChooserList instanceChooserList);

    void inject(FormsProvider formsProvider);

    void inject(InstanceProvider instanceProvider);

    void inject(BackgroundAudioPermissionDialogFragment backgroundAudioPermissionDialogFragment);

    void inject(AppListFragment appListFragment);

    void inject(ChangeAdminPasswordDialog changeAdminPasswordDialog);

    void inject(MediaLoadingTask mediaLoadingTask);

    void inject(ThemeUtils themeUtils);

    void inject(BaseProjectPreferencesFragment baseProjectPreferencesFragment);

    void inject(BaseAdminPreferencesFragment baseAdminPreferencesFragment);

    void inject(CaptionedListPreference captionedListPreference);

    void inject(AndroidShortcutsActivity androidShortcutsActivity);

    void inject(ProjectSettingsDialog projectSettingsDialog);

    void inject(ManualProjectCreatorDialog manualProjectCreatorDialog);

    void inject(QrCodeProjectCreatorDialog qrCodeProjectCreatorDialog);

    void inject(FirstLaunchActivity firstLaunchActivity);

    void inject(InstanceUploader instanceUploader);

    void inject(FormUriActivity formUriActivity);

    void inject(MapsPreferencesFragment mapsPreferencesFragment);

    void inject(FormsDownloadResultDialog formsDownloadResultDialog);

    void inject(SelectOneFromMapDialogFragment selectOneFromMapDialogFragment);

    void inject(DrawActivity drawActivity);

    void inject(PenColorPickerDialog colorPickerDialog);

    void inject(BlankFormListActivity blankFormListActivity);

    void inject(DeleteRepeatDialogFragment deleteRepeatDialogFragment);

    void inject(AppListActivity appListActivity);

    void inject(DownloadFormListTask downloadFormListTask);

    OpenRosaHttpInterface openRosaHttpInterface();

    ReferenceManager referenceManager();

    SettingsProvider settingsProvider();

    ApplicationInitializer applicationInitializer();

    ODKAppSettingsImporter settingsImporter();

    ProjectsRepository projectsRepository();

    ProjectsDataService currentProjectProvider();

    StoragePathProvider storagePathProvider();

    FormsRepositoryProvider formsRepositoryProvider();

    InstancesRepositoryProvider instancesRepositoryProvider();

    FormSourceProvider formSourceProvider();

    ExistingProjectMigrator existingProjectMigrator();

    ProjectResetter projectResetter();

    MapFragmentFactory mapFragmentFactory();

    Scheduler scheduler();

    LocationClient locationClient();

    PermissionsProvider permissionsProvider();

    PermissionsChecker permissionsChecker();

    ReferenceLayerRepository referenceLayerRepository();

    NetworkStateProvider networkStateProvider();

    EntitiesRepositoryProvider entitiesRepositoryProvider();

    FormsDataService formsDataService();
}
