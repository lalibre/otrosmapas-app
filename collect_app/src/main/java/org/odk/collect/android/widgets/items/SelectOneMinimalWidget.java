package net.lalibre.collect.otrosmapas.widgets.items;

import static net.lalibre.collect.otrosmapas.formentry.media.FormMediaUtils.getPlayColor;

import android.annotation.SuppressLint;
import android.content.Context;

import org.javarosa.core.model.data.IAnswerData;
import org.javarosa.core.model.data.SelectOneData;
import org.javarosa.core.model.data.helper.Selection;
import net.lalibre.collect.otrosmapas.activities.FormFillingActivity;
import net.lalibre.collect.otrosmapas.formentry.questions.QuestionDetails;
import net.lalibre.collect.otrosmapas.fragments.dialogs.SelectMinimalDialog;
import net.lalibre.collect.otrosmapas.fragments.dialogs.SelectOneMinimalDialog;
import net.lalibre.collect.otrosmapas.listeners.AdvanceToNextListener;
import net.lalibre.collect.otrosmapas.utilities.Appearances;
import net.lalibre.collect.otrosmapas.utilities.HtmlUtils;
import net.lalibre.collect.otrosmapas.utilities.SelectOneWidgetUtils;
import net.lalibre.collect.otrosmapas.widgets.interfaces.SelectChoiceLoader;
import net.lalibre.collect.otrosmapas.widgets.utilities.WaitingForDataRegistry;
import net.lalibre.collect.otrosmapasshared.ui.DialogFragmentUtils;

import java.util.List;

@SuppressLint("ViewConstructor")
public class SelectOneMinimalWidget extends SelectMinimalWidget {
    private Selection selectedItem;
    private final boolean autoAdvance;
    private AdvanceToNextListener autoAdvanceListener;

    public SelectOneMinimalWidget(Context context, QuestionDetails prompt, boolean autoAdvance, WaitingForDataRegistry waitingForDataRegistry, SelectChoiceLoader selectChoiceLoader) {
        super(context, prompt, waitingForDataRegistry, selectChoiceLoader);
        render();

        selectedItem = SelectOneWidgetUtils.getSelectedItem(prompt.getPrompt(), items);
        this.autoAdvance = autoAdvance;
        if (context instanceof AdvanceToNextListener) {
            autoAdvanceListener = (AdvanceToNextListener) context;
        }
        updateAnswer();
    }

    @Override
    protected void showDialog() {
        int numColumns = Appearances.getNumberOfColumns(getFormEntryPrompt(), screenUtils);
        boolean noButtonsMode = Appearances.isCompactAppearance(getFormEntryPrompt()) || Appearances.isNoButtonsAppearance(getFormEntryPrompt());

        SelectOneMinimalDialog dialog = new SelectOneMinimalDialog(getSavedSelectedValue(),
                Appearances.isFlexAppearance(getFormEntryPrompt()),
                Appearances.isAutocomplete(getFormEntryPrompt()), getContext(), items,
                getFormEntryPrompt(), getReferenceManager(),
                getPlayColor(getFormEntryPrompt(), themeUtils), numColumns, noButtonsMode, mediaUtils);

        DialogFragmentUtils.showIfNotShowing(dialog, SelectMinimalDialog.class, ((FormFillingActivity) getContext()).getSupportFragmentManager());
    }

    @Override
    public IAnswerData getAnswer() {
        return selectedItem == null
                ? null
                : new SelectOneData(selectedItem);
    }

    @Override
    public void clearAnswer() {
        selectedItem = null;
        super.clearAnswer();
    }

    @Override
    public void setData(Object answer) {
        List<Selection> answers = (List<Selection>) answer;
        selectedItem = answers.isEmpty() ? null : answers.get(0);
        updateAnswer();
        widgetValueChanged();

        if (autoAdvance && autoAdvanceListener != null) {
            autoAdvanceListener.advance();
        }
    }

    @Override
    public void setChoiceSelected(int choiceIndex, boolean isSelected) {
        selectedItem = isSelected
                ? items.get(choiceIndex).selection()
                : null;
    }

    private void updateAnswer() {
        if (selectedItem == null) {
            binding.answer.setText(net.lalibre.collect.strings.R.string.select_answer);
        } else {
            binding.answer.setText(HtmlUtils.textToHtml(getFormEntryPrompt().getSelectItemText(selectedItem)));
        }
    }

    private String getSavedSelectedValue() {
        return selectedItem == null
                ? null
                : selectedItem.getValue();
    }

    @Override
    public void setOnLongClickListener(OnLongClickListener l) {
    }
}
