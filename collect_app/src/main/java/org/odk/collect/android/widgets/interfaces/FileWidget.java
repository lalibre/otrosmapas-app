package net.lalibre.collect.otrosmapas.widgets.interfaces;

/**
 * @author James Knight
 */
public interface FileWidget extends Widget {
    void deleteFile();
}
