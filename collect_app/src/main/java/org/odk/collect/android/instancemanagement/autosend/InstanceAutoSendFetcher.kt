package net.lalibre.collect.otrosmapas.instancemanagement.autosend

import net.lalibre.collect.forms.FormsRepository
import net.lalibre.collect.forms.instances.Instance
import net.lalibre.collect.forms.instances.InstancesRepository

class InstanceAutoSendFetcher(private val autoSendSettingsProvider: AutoSendSettingsProvider) {

    fun getInstancesToAutoSend(projectId: String, instancesRepository: InstancesRepository, formsRepository: FormsRepository): List<Instance> {
        val allFinalizedForms = instancesRepository.getAllByStatus(Instance.STATUS_COMPLETE, Instance.STATUS_SUBMISSION_FAILED)

        val isAutoSendEnabledInSettings = autoSendSettingsProvider.isAutoSendEnabledInSettings(projectId)
        return allFinalizedForms.filter {
            formsRepository.getLatestByFormIdAndVersion(it.formId, it.formVersion)?.let { form ->
                form.shouldFormBeSentAutomatically(isAutoSendEnabledInSettings)
            } ?: false
        }
    }
}
