package net.lalibre.collect.otrosmapas.injection.config;

import static androidx.core.content.FileProvider.getUriForFile;
import static net.lalibre.collect.otrosmapasshared.data.AppStateKt.getState;
import static net.lalibre.collect.settings.keys.MetaKeys.KEY_INSTALL_ID;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

import android.app.Application;
import android.content.Context;
import android.media.MediaPlayer;
import android.webkit.MimeTypeMap;

import androidx.work.WorkManager;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.DriveScopes;
import com.google.gson.Gson;

import org.javarosa.core.reference.ReferenceManager;
import org.json.JSONArray;
import org.json.JSONObject;
import net.lalibre.collect.analytics.Analytics;
import net.lalibre.collect.analytics.BlockableFirebaseAnalytics;
import net.lalibre.collect.analytics.NoopAnalytics;
import net.lalibre.collect.otrosmapas.BuildConfig;
import net.lalibre.collect.otrosmapas.R;
import net.lalibre.collect.otrosmapas.application.CollectSettingsChangeHandler;
import net.lalibre.collect.otrosmapas.application.MapboxClassInstanceCreator;
import net.lalibre.collect.otrosmapas.application.initialization.AnalyticsInitializer;
import net.lalibre.collect.otrosmapas.application.initialization.ApplicationInitializer;
import net.lalibre.collect.otrosmapas.application.initialization.ExistingProjectMigrator;
import net.lalibre.collect.otrosmapas.application.initialization.ExistingSettingsMigrator;
import net.lalibre.collect.otrosmapas.application.initialization.FormUpdatesUpgrade;
import net.lalibre.collect.otrosmapas.application.initialization.GoogleDriveProjectsDeleter;
import net.lalibre.collect.otrosmapas.application.initialization.MapsInitializer;
import net.lalibre.collect.otrosmapas.application.initialization.upgrade.UpgradeInitializer;
import net.lalibre.collect.otrosmapas.backgroundwork.FormUpdateAndInstanceSubmitScheduler;
import net.lalibre.collect.otrosmapas.backgroundwork.FormUpdateScheduler;
import net.lalibre.collect.otrosmapas.backgroundwork.InstanceSubmitScheduler;
import net.lalibre.collect.otrosmapas.configure.qr.AppConfigurationGenerator;
import net.lalibre.collect.otrosmapas.configure.qr.CachingQRCodeGenerator;
import net.lalibre.collect.otrosmapas.configure.qr.QRCodeGenerator;
import net.lalibre.collect.otrosmapas.database.itemsets.DatabaseFastExternalItemsetsRepository;
import net.lalibre.collect.otrosmapas.draw.PenColorPickerViewModel;
import net.lalibre.collect.otrosmapas.entities.EntitiesRepositoryProvider;
import net.lalibre.collect.otrosmapas.external.InstancesContract;
import net.lalibre.collect.otrosmapas.formentry.AppStateFormSessionRepository;
import net.lalibre.collect.otrosmapas.formentry.FormSessionRepository;
import net.lalibre.collect.otrosmapas.formentry.media.AudioHelperFactory;
import net.lalibre.collect.otrosmapas.formentry.media.ScreenContextAudioHelperFactory;
import net.lalibre.collect.otrosmapas.formlists.blankformlist.BlankFormListViewModel;
import net.lalibre.collect.otrosmapas.formmanagement.CollectFormEntryControllerFactory;
import net.lalibre.collect.otrosmapas.formmanagement.FormDownloader;
import net.lalibre.collect.otrosmapas.formmanagement.FormMetadataParser;
import net.lalibre.collect.otrosmapas.formmanagement.FormSourceProvider;
import net.lalibre.collect.otrosmapas.formmanagement.FormsDataService;
import net.lalibre.collect.otrosmapas.formmanagement.InstancesDataService;
import net.lalibre.collect.otrosmapas.formmanagement.ServerFormDownloader;
import net.lalibre.collect.otrosmapas.formmanagement.ServerFormsDetailsFetcher;
import net.lalibre.collect.otrosmapas.gdrive.GoogleAccountCredentialGoogleAccountPicker;
import net.lalibre.collect.otrosmapas.gdrive.GoogleAccountPicker;
import net.lalibre.collect.otrosmapas.gdrive.GoogleAccountsManager;
import net.lalibre.collect.otrosmapas.gdrive.GoogleApiProvider;
import net.lalibre.collect.otrosmapas.geo.MapFragmentFactoryImpl;
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.AutoSendSettingsProvider;
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.InstanceAutoSendFetcher;
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.InstanceAutoSender;
import net.lalibre.collect.otrosmapas.instancemanagement.send.ReadyToSendViewModel;
import net.lalibre.collect.otrosmapas.itemsets.FastExternalItemsetsRepository;
import net.lalibre.collect.otrosmapas.mainmenu.MainMenuViewModelFactory;
import net.lalibre.collect.otrosmapas.notifications.NotificationManagerNotifier;
import net.lalibre.collect.otrosmapas.notifications.Notifier;
import net.lalibre.collect.otrosmapas.openrosa.CollectThenSystemContentTypeMapper;
import net.lalibre.collect.otrosmapas.openrosa.OpenRosaHttpInterface;
import net.lalibre.collect.otrosmapas.openrosa.okhttp.OkHttpConnection;
import net.lalibre.collect.otrosmapas.openrosa.okhttp.OkHttpOpenRosaServerClientProvider;
import net.lalibre.collect.otrosmapas.preferences.Defaults;
import net.lalibre.collect.otrosmapas.preferences.PreferenceVisibilityHandler;
import net.lalibre.collect.otrosmapas.preferences.ProjectPreferencesViewModel;
import net.lalibre.collect.otrosmapas.preferences.source.SettingsStore;
import net.lalibre.collect.otrosmapas.preferences.source.SharedPreferencesSettingsProvider;
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService;
import net.lalibre.collect.otrosmapas.projects.ProjectCreator;
import net.lalibre.collect.otrosmapas.projects.ProjectDeleter;
import net.lalibre.collect.otrosmapas.projects.ProjectDependencyProviderFactory;
import net.lalibre.collect.otrosmapas.storage.StoragePathProvider;
import net.lalibre.collect.otrosmapas.storage.StorageSubdirectory;
import net.lalibre.collect.otrosmapas.tasks.FormLoaderTask;
import net.lalibre.collect.otrosmapas.utilities.AdminPasswordProvider;
import net.lalibre.collect.otrosmapas.utilities.AndroidUserAgent;
import net.lalibre.collect.otrosmapas.utilities.ChangeLockProvider;
import net.lalibre.collect.otrosmapas.utilities.CodeCaptureManagerFactory;
import net.lalibre.collect.otrosmapas.utilities.ContentUriProvider;
import net.lalibre.collect.otrosmapas.utilities.ExternalAppIntentProvider;
import net.lalibre.collect.otrosmapas.utilities.ExternalWebPageHelper;
import net.lalibre.collect.otrosmapas.utilities.FileProvider;
import net.lalibre.collect.otrosmapas.utilities.FormsRepositoryProvider;
import net.lalibre.collect.otrosmapas.utilities.ImageCompressionController;
import net.lalibre.collect.otrosmapas.utilities.InstancesRepositoryProvider;
import net.lalibre.collect.otrosmapas.utilities.MediaUtils;
import net.lalibre.collect.otrosmapas.utilities.ProjectResetter;
import net.lalibre.collect.otrosmapas.utilities.SoftKeyboardController;
import net.lalibre.collect.otrosmapas.utilities.WebCredentialsUtils;
import net.lalibre.collect.otrosmapas.version.VersionInformation;
import net.lalibre.collect.otrosmapas.views.BarcodeViewDecoder;
import net.lalibre.collect.otrosmapasshared.bitmap.ImageCompressor;
import net.lalibre.collect.otrosmapasshared.network.ConnectivityProvider;
import net.lalibre.collect.otrosmapasshared.network.NetworkStateProvider;
import net.lalibre.collect.otrosmapasshared.system.IntentLauncher;
import net.lalibre.collect.otrosmapasshared.system.IntentLauncherImpl;
import net.lalibre.collect.otrosmapasshared.utils.ScreenUtils;
import net.lalibre.collect.async.CoroutineAndWorkManagerScheduler;
import net.lalibre.collect.async.Scheduler;
import net.lalibre.collect.audiorecorder.recording.AudioRecorder;
import net.lalibre.collect.audiorecorder.recording.AudioRecorderFactory;
import net.lalibre.collect.forms.FormsRepository;
import net.lalibre.collect.imageloader.GlideImageLoader;
import net.lalibre.collect.imageloader.ImageLoader;
import net.lalibre.collect.location.GoogleFusedLocationClient;
import net.lalibre.collect.location.LocationClient;
import net.lalibre.collect.location.LocationClientProvider;
import net.lalibre.collect.maps.MapFragmentFactory;
import net.lalibre.collect.maps.layers.DirectoryReferenceLayerRepository;
import net.lalibre.collect.maps.layers.ReferenceLayerRepository;
import net.lalibre.collect.metadata.InstallIDProvider;
import net.lalibre.collect.metadata.PropertyManager;
import net.lalibre.collect.metadata.SettingsInstallIDProvider;
import net.lalibre.collect.permissions.ContextCompatPermissionChecker;
import net.lalibre.collect.permissions.PermissionsChecker;
import net.lalibre.collect.permissions.PermissionsProvider;
import net.lalibre.collect.projects.ProjectsRepository;
import net.lalibre.collect.projects.SharedPreferencesProjectsRepository;
import net.lalibre.collect.qrcode.QRCodeDecoder;
import net.lalibre.collect.qrcode.QRCodeDecoderImpl;
import net.lalibre.collect.qrcode.QRCodeEncoderImpl;
import net.lalibre.collect.settings.ODKAppSettingsImporter;
import net.lalibre.collect.settings.ODKAppSettingsMigrator;
import net.lalibre.collect.settings.SettingsProvider;
import net.lalibre.collect.settings.importing.ProjectDetailsCreatorImpl;
import net.lalibre.collect.settings.importing.SettingsChangeHandler;
import net.lalibre.collect.settings.keys.AppConfigurationKeys;
import net.lalibre.collect.settings.keys.MetaKeys;
import net.lalibre.collect.settings.keys.ProjectKeys;
import net.lalibre.collect.shared.strings.UUIDGenerator;
import net.lalibre.collect.utilities.UserAgentProvider;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import okhttp3.OkHttpClient;

/**
 * Add dependency providers here (annotated with @Provides)
 * for objects you need to inject
 */
@Module
@SuppressWarnings("PMD.CouplingBetweenObjects")
public class AppDependencyModule {

    @Provides
    Context context(Application application) {
        return application;
    }

    @Provides
    MimeTypeMap provideMimeTypeMap() {
        return MimeTypeMap.getSingleton();
    }

    @Provides
    @Singleton
    UserAgentProvider providesUserAgent() {
        return new AndroidUserAgent();
    }

    @Provides
    @Singleton
    public OpenRosaHttpInterface provideHttpInterface(MimeTypeMap mimeTypeMap, UserAgentProvider userAgentProvider, Application application, VersionInformation versionInformation) {
        String cacheDir = application.getCacheDir().getAbsolutePath();
        return new OkHttpConnection(
                new OkHttpOpenRosaServerClientProvider(new OkHttpClient(), cacheDir),
                new CollectThenSystemContentTypeMapper(mimeTypeMap),
                userAgentProvider.getUserAgent()
        );
    }

    @Provides
    WebCredentialsUtils provideWebCredentials(SettingsProvider settingsProvider) {
        return new WebCredentialsUtils(settingsProvider.getUnprotectedSettings());
    }

    @Provides
    public FormDownloader providesFormDownloader(FormSourceProvider formSourceProvider, FormsRepositoryProvider formsRepositoryProvider, StoragePathProvider storagePathProvider) {
        return new ServerFormDownloader(formSourceProvider.get(), formsRepositoryProvider.get(), new File(storagePathProvider.getOdkDirPath(StorageSubdirectory.CACHE)), storagePathProvider.getOdkDirPath(StorageSubdirectory.FORMS), new FormMetadataParser(), System::currentTimeMillis);
    }

    @Provides
    @Singleton
    public Analytics providesAnalytics(Application application) {
        try {
            return new BlockableFirebaseAnalytics(application);
        } catch (IllegalStateException e) {
            // Couldn't setup Firebase so use no-op instance
            return new NoopAnalytics();
        }
    }

    @Provides
    public PermissionsProvider providesPermissionsProvider(PermissionsChecker permissionsChecker) {
        return new PermissionsProvider(permissionsChecker);
    }

    @Provides
    public ReferenceManager providesReferenceManager() {
        return ReferenceManager.instance();
    }

    @Provides
    public AudioHelperFactory providesAudioHelperFactory(Scheduler scheduler) {
        return new ScreenContextAudioHelperFactory(scheduler, MediaPlayer::new);
    }

    @Provides
    @Singleton
    public SettingsProvider providesSettingsProvider(Context context) {
        return new SharedPreferencesSettingsProvider(context);
    }


    @Provides
    public InstallIDProvider providesInstallIDProvider(SettingsProvider settingsProvider) {
        return new SettingsInstallIDProvider(settingsProvider.getMetaSettings(), KEY_INSTALL_ID);
    }

    @Provides
    public StoragePathProvider providesStoragePathProvider(Context context, ProjectsDataService projectsDataService, ProjectsRepository projectsRepository) {
        File externalFilesDir = context.getExternalFilesDir(null);

        if (externalFilesDir != null) {
            return new StoragePathProvider(projectsDataService, projectsRepository, externalFilesDir.getAbsolutePath());
        } else {
            throw new IllegalStateException("Storage is not available!");
        }
    }

    @Provides
    public AdminPasswordProvider providesAdminPasswordProvider(SettingsProvider settingsProvider) {
        return new AdminPasswordProvider(settingsProvider.getProtectedSettings());
    }

    @Provides
    public FormUpdateScheduler providesFormUpdateManger(Scheduler scheduler, SettingsProvider settingsProvider, Application application) {
        return new FormUpdateAndInstanceSubmitScheduler(scheduler, settingsProvider, application);
    }

    @Provides
    public InstanceSubmitScheduler providesFormSubmitManager(Scheduler scheduler, SettingsProvider settingsProvider, Application application) {
        return new FormUpdateAndInstanceSubmitScheduler(scheduler, settingsProvider, application);
    }

    @Provides
    public NetworkStateProvider providesNetworkStateProvider(Context context) {
        return new ConnectivityProvider(context);
    }

    @Provides
    public QRCodeGenerator providesQRCodeGenerator(Context context) {
        return new CachingQRCodeGenerator(new QRCodeEncoderImpl());
    }

    @Provides
    public VersionInformation providesVersionInformation() {
        return new VersionInformation(() -> BuildConfig.VERSION_NAME);
    }

    @Provides
    public FileProvider providesFileProvider(Context context) {
        return filePath -> getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", new File(filePath));
    }

    @Provides
    public WorkManager providesWorkManager(Context context) {
        return WorkManager.getInstance(context);
    }

    @Provides
    public Scheduler providesScheduler(WorkManager workManager) {
        return new CoroutineAndWorkManagerScheduler(workManager);
    }

    @Provides
    public ODKAppSettingsMigrator providesPreferenceMigrator(SettingsProvider settingsProvider) {
        return new ODKAppSettingsMigrator(settingsProvider.getMetaSettings());
    }

    @Provides
    @Singleton
    public PropertyManager providesPropertyManager(InstallIDProvider installIDProvider, SettingsProvider settingsProvider) {
        return new PropertyManager(installIDProvider, settingsProvider);
    }

    @Provides
    public SettingsChangeHandler providesSettingsChangeHandler(PropertyManager propertyManager, FormUpdateScheduler formUpdateScheduler, FormsDataService formsDataService) {
        return new CollectSettingsChangeHandler(propertyManager, formUpdateScheduler, formsDataService);
    }

    @Provides
    public ODKAppSettingsImporter providesODKAppSettingsImporter(Context context, ProjectsRepository projectsRepository, SettingsProvider settingsProvider, SettingsChangeHandler settingsChangeHandler) {
        JSONObject deviceUnsupportedSettings = new JSONObject();
        if (!MapboxClassInstanceCreator.isMapboxAvailable()) {
            try {
                deviceUnsupportedSettings.put(
                        AppConfigurationKeys.GENERAL,
                        new JSONObject().put(ProjectKeys.KEY_BASEMAP_SOURCE, new JSONArray(singletonList(ProjectKeys.BASEMAP_SOURCE_MAPBOX)))
                );
            } catch (Throwable ignored) {
                // ignore
            }
        }

        return new ODKAppSettingsImporter(
                projectsRepository,
                settingsProvider,
                Defaults.getUnprotected(),
                Defaults.getProtected(),
                asList(context.getResources().getStringArray(R.array.project_colors)),
                settingsChangeHandler,
                deviceUnsupportedSettings
        );
    }

    @Provides
    public BarcodeViewDecoder providesBarcodeViewDecoder() {
        return new BarcodeViewDecoder();
    }

    @Provides
    public QRCodeDecoder providesQRCodeDecoder() {
        return new QRCodeDecoderImpl();
    }

    @Provides
    public ServerFormsDetailsFetcher providesServerFormDetailsFetcher(FormsRepositoryProvider formsRepositoryProvider, FormSourceProvider formSourceProvider) {
        FormsRepository formsRepository = formsRepositoryProvider.get();
        return new ServerFormsDetailsFetcher(formsRepository, formSourceProvider.get());
    }

    @Provides
    public Notifier providesNotifier(Application application, SettingsProvider settingsProvider, ProjectsRepository projectsRepository) {
        return new NotificationManagerNotifier(application, settingsProvider, projectsRepository);
    }

    @Provides
    @Singleton
    public ChangeLockProvider providesChangeLockProvider() {
        return new ChangeLockProvider();
    }

    @Provides
    public GoogleApiProvider providesGoogleApiProvider(Context context) {
        return new GoogleApiProvider(context);
    }

    @Provides
    public GoogleAccountPicker providesGoogleAccountPicker(Context context) {
        return new GoogleAccountCredentialGoogleAccountPicker(GoogleAccountCredential
                .usingOAuth2(context, singletonList(DriveScopes.DRIVE))
                .setBackOff(new ExponentialBackOff()));
    }

    @Provides
    ScreenUtils providesScreenUtils(Context context) {
        return new ScreenUtils(context);
    }

    @Provides
    public AudioRecorder providesAudioRecorder(Application application) {
        return new AudioRecorderFactory(application).create();
    }

    @Provides
    @Singleton
    public EntitiesRepositoryProvider provideEntitiesRepositoryProvider(Application application, ProjectsDataService projectsDataService) {
        return new EntitiesRepositoryProvider(application, projectsDataService);
    }

    @Provides
    public SoftKeyboardController provideSoftKeyboardController() {
        return new SoftKeyboardController();
    }

    @Provides
    public AppConfigurationGenerator providesJsonPreferencesGenerator(SettingsProvider settingsProvider, ProjectsDataService projectsDataService) {
        return new AppConfigurationGenerator(settingsProvider, projectsDataService);
    }

    @Provides
    @Singleton
    public PermissionsChecker providesPermissionsChecker(Context context) {
        return new ContextCompatPermissionChecker(context);
    }

    @Provides
    @Singleton
    public ExternalAppIntentProvider providesExternalAppIntentProvider() {
        return new ExternalAppIntentProvider();
    }

    @Provides
    public FormSessionRepository providesFormSessionStore(Application application) {
        return new AppStateFormSessionRepository(application);
    }

    @Provides
    @Named("GENERAL_SETTINGS_STORE")
    public SettingsStore providesGeneralSettingsStore(SettingsProvider settingsProvider) {
        return new SettingsStore(settingsProvider.getUnprotectedSettings());
    }

    @Provides
    @Named("ADMIN_SETTINGS_STORE")
    public SettingsStore providesAdminSettingsStore(SettingsProvider settingsProvider) {
        return new SettingsStore(settingsProvider.getProtectedSettings());
    }

    @Provides
    public ExternalWebPageHelper providesExternalWebPageHelper() {
        return new ExternalWebPageHelper();
    }

    @Provides
    @Singleton
    public ProjectsRepository providesProjectsRepository(UUIDGenerator uuidGenerator, Gson gson, SettingsProvider settingsProvider) {
        return new SharedPreferencesProjectsRepository(uuidGenerator, gson, settingsProvider.getMetaSettings(), MetaKeys.KEY_PROJECTS);
    }

    @Provides
    public ProjectCreator providesProjectCreator(ProjectsRepository projectsRepository, ProjectsDataService projectsDataService,
                                                 ODKAppSettingsImporter settingsImporter, SettingsProvider settingsProvider) {
        return new ProjectCreator(projectsRepository, projectsDataService, settingsImporter, settingsProvider);
    }

    @Provides
    public Gson providesGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    public UUIDGenerator providesUUIDGenerator() {
        return new UUIDGenerator();
    }

    @Provides
    public InstancesDataService providesInstancesDataService(Application application, InstancesRepositoryProvider instancesRepositoryProvider, ProjectsDataService projectsDataService, FormsRepositoryProvider formsRepositoryProvider, EntitiesRepositoryProvider entitiesRepositoryProvider, StoragePathProvider storagePathProvider, InstanceSubmitScheduler instanceSubmitScheduler) {
        Function0<Unit> onUpdate = () -> {
            application.getContentResolver().notifyChange(
                    InstancesContract.getUri(projectsDataService.getCurrentProject().getUuid()),
                    null
            );

            return null;
        };

        return new InstancesDataService(getState(application), formsRepositoryProvider, instancesRepositoryProvider, entitiesRepositoryProvider, storagePathProvider, instanceSubmitScheduler, projectsDataService, onUpdate);
    }

    @Provides
    public FastExternalItemsetsRepository providesItemsetsRepository() {
        return new DatabaseFastExternalItemsetsRepository();
    }

    @Provides
    public ProjectsDataService providesCurrentProjectProvider(SettingsProvider settingsProvider, ProjectsRepository projectsRepository, AnalyticsInitializer analyticsInitializer, Context context, MapsInitializer mapsInitializer) {
        return new ProjectsDataService(settingsProvider, projectsRepository, analyticsInitializer, mapsInitializer);
    }

    @Provides
    public FormsRepositoryProvider providesFormsRepositoryProvider(Application application) {
        return new FormsRepositoryProvider(application);
    }

    @Provides
    public InstancesRepositoryProvider providesInstancesRepositoryProvider(Context context, StoragePathProvider storagePathProvider) {
        return new InstancesRepositoryProvider(context, storagePathProvider);
    }

    @Provides
    public ProjectPreferencesViewModel.Factory providesProjectPreferencesViewModel(AdminPasswordProvider adminPasswordProvider) {
        return new ProjectPreferencesViewModel.Factory(adminPasswordProvider);
    }

    @Provides
    public ReadyToSendViewModel.Factory providesReadyToSendViewModel(InstancesRepositoryProvider instancesRepositoryProvider, Scheduler scheduler) {
        return new ReadyToSendViewModel.Factory(instancesRepositoryProvider.get(), scheduler, System::currentTimeMillis);
    }

    @Provides
    public MainMenuViewModelFactory providesMainMenuViewModelFactory(VersionInformation versionInformation, Application application,
                                                                     SettingsProvider settingsProvider, InstancesDataService instancesDataService,
                                                                     Scheduler scheduler, ProjectsDataService projectsDataService,
                                                                     AnalyticsInitializer analyticsInitializer, PermissionsChecker permissionChecker,
                                                                     FormsRepositoryProvider formsRepositoryProvider, InstancesRepositoryProvider instancesRepositoryProvider,
                                                                     AutoSendSettingsProvider autoSendSettingsProvider) {
        return new MainMenuViewModelFactory(versionInformation, application, settingsProvider, instancesDataService, scheduler, projectsDataService, analyticsInitializer, permissionChecker, formsRepositoryProvider, instancesRepositoryProvider, autoSendSettingsProvider);
    }

    @Provides
    public AnalyticsInitializer providesAnalyticsInitializer(Analytics analytics, VersionInformation versionInformation, SettingsProvider settingsProvider) {
        return new AnalyticsInitializer(analytics, versionInformation, settingsProvider);
    }

    @Provides
    public FormSourceProvider providesFormSourceProvider(SettingsProvider settingsProvider, OpenRosaHttpInterface openRosaHttpInterface) {
        return new FormSourceProvider(settingsProvider, openRosaHttpInterface);
    }

    @Provides
    public FormsDataService providesFormsUpdater(Application application, Notifier notifier, ProjectDependencyProviderFactory projectDependencyProviderFactory) {
        return new FormsDataService(getState(application), notifier, projectDependencyProviderFactory, System::currentTimeMillis);
    }

    @Provides
    public InstanceAutoSender providesInstanceAutoSender(AutoSendSettingsProvider autoSendSettingsProvider, Context context, Notifier notifier, GoogleAccountsManager googleAccountsManager, GoogleApiProvider googleApiProvider, PermissionsProvider permissionsProvider, InstancesDataService instancesDataService, PropertyManager propertyManager) {
        InstanceAutoSendFetcher instanceAutoSendFetcher = new InstanceAutoSendFetcher(autoSendSettingsProvider);
        return new InstanceAutoSender(instanceAutoSendFetcher, context, notifier, googleAccountsManager, googleApiProvider, permissionsProvider, instancesDataService, propertyManager);
    }

    @Provides
    public AutoSendSettingsProvider providesAutoSendSettingsProvider(NetworkStateProvider networkStateProvider, SettingsProvider settingsProvider) {
        return new AutoSendSettingsProvider(networkStateProvider, settingsProvider);
    }

    @Provides
    public CodeCaptureManagerFactory providesCodeCaptureManagerFactory() {
        return CodeCaptureManagerFactory.INSTANCE;
    }

    @Provides
    public ExistingProjectMigrator providesExistingProjectMigrator(Context context, StoragePathProvider storagePathProvider, ProjectsRepository projectsRepository, SettingsProvider settingsProvider, ProjectsDataService projectsDataService) {
        return new ExistingProjectMigrator(context, storagePathProvider, projectsRepository, settingsProvider, projectsDataService, new ProjectDetailsCreatorImpl(asList(context.getResources().getStringArray(R.array.project_colors)), Defaults.getUnprotected()));
    }

    @Provides
    public FormUpdatesUpgrade providesFormUpdatesUpgrader(Scheduler scheduler, ProjectsRepository projectsRepository, FormUpdateScheduler formUpdateScheduler) {
        return new FormUpdatesUpgrade(scheduler, projectsRepository, formUpdateScheduler);
    }

    @Provides
    public ExistingSettingsMigrator providesExistingSettingsMigrator(ProjectsRepository projectsRepository, SettingsProvider settingsProvider, ODKAppSettingsMigrator settingsMigrator) {
        return new ExistingSettingsMigrator(projectsRepository, settingsProvider, settingsMigrator);
    }

    @Provides
    public GoogleDriveProjectsDeleter providesGoogleDriveProjectsDeleter(ProjectsRepository projectsRepository, SettingsProvider settingsProvider, ProjectDeleter projectDeleter) {
        return new GoogleDriveProjectsDeleter(projectsRepository, settingsProvider, projectDeleter);
    }

    @Provides
    public UpgradeInitializer providesUpgradeInitializer(Context context, SettingsProvider settingsProvider, ExistingProjectMigrator existingProjectMigrator, ExistingSettingsMigrator existingSettingsMigrator, FormUpdatesUpgrade formUpdatesUpgrade, GoogleDriveProjectsDeleter googleDriveProjectsDeleter) {
        return new UpgradeInitializer(
                context,
                settingsProvider,
                existingProjectMigrator,
                existingSettingsMigrator,
                formUpdatesUpgrade,
                googleDriveProjectsDeleter
        );
    }

    @Provides
    public ApplicationInitializer providesApplicationInitializer(Application context, UserAgentProvider userAgentProvider, PropertyManager propertyManager, Analytics analytics, UpgradeInitializer upgradeInitializer, AnalyticsInitializer analyticsInitializer, ProjectsRepository projectsRepository, SettingsProvider settingsProvider, MapsInitializer mapsInitializer) {
        return new ApplicationInitializer(context, propertyManager, analytics, upgradeInitializer, analyticsInitializer, mapsInitializer, projectsRepository, settingsProvider);
    }

    @Provides
    public ProjectDeleter providesProjectDeleter(ProjectsRepository projectsRepository, ProjectsDataService projectsDataService, FormUpdateScheduler formUpdateScheduler, InstanceSubmitScheduler instanceSubmitScheduler, InstancesRepositoryProvider instancesRepositoryProvider, StoragePathProvider storagePathProvider, ChangeLockProvider changeLockProvider, SettingsProvider settingsProvider) {
        return new ProjectDeleter(projectsRepository, projectsDataService, formUpdateScheduler, instanceSubmitScheduler, instancesRepositoryProvider, storagePathProvider, changeLockProvider, settingsProvider);
    }

    @Provides
    public ProjectResetter providesProjectResetter(StoragePathProvider storagePathProvider, PropertyManager propertyManager, SettingsProvider settingsProvider, InstancesRepositoryProvider instancesRepositoryProvider, FormsRepositoryProvider formsRepositoryProvider) {
        return new ProjectResetter(storagePathProvider, propertyManager, settingsProvider, instancesRepositoryProvider, formsRepositoryProvider);
    }

    @Provides
    public PreferenceVisibilityHandler providesDisabledPreferencesRemover(SettingsProvider settingsProvider, VersionInformation versionInformation) {
        return new PreferenceVisibilityHandler(settingsProvider, versionInformation);
    }

    @Provides
    public ReferenceLayerRepository providesReferenceLayerRepository(StoragePathProvider storagePathProvider) {
        return new DirectoryReferenceLayerRepository(
                storagePathProvider.getOdkDirPath(StorageSubdirectory.LAYERS),
                storagePathProvider.getOdkDirPath(StorageSubdirectory.SHARED_LAYERS)
        );
    }

    @Provides
    public IntentLauncher providesIntentLauncher() {
        return IntentLauncherImpl.INSTANCE;
    }

    @Provides
    public LocationClient providesLocationClient(Application application) {
        return LocationClientProvider.getClient(application);
    }

    @Provides
    @Named("fused")
    public LocationClient providesFusedLocationClient(Application application) {
        return new GoogleFusedLocationClient(application);
    }

    @Provides
    public MediaUtils providesMediaUtils(IntentLauncher intentLauncher) {
        return new MediaUtils(intentLauncher, new ContentUriProvider());
    }

    @Provides
    public MapFragmentFactory providesMapFragmentFactory(SettingsProvider settingsProvider) {
        return new MapFragmentFactoryImpl(settingsProvider);
    }

    @Provides
    public ImageLoader providesImageLoader() {
        return new GlideImageLoader();
    }

    @Provides
    public PenColorPickerViewModel.Factory providesPenColorPickerViewModel(SettingsProvider settingsProvider) {
        return new PenColorPickerViewModel.Factory(settingsProvider.getMetaSettings());
    }

    @Provides
    public ProjectDependencyProviderFactory providesProjectDependencyProviderFactory(SettingsProvider settingsProvider, FormsRepositoryProvider formsRepositoryProvider, InstancesRepositoryProvider instancesRepositoryProvider, StoragePathProvider storagePathProvider, ChangeLockProvider changeLockProvider, FormSourceProvider formSourceProvider) {
        return new ProjectDependencyProviderFactory(settingsProvider, formsRepositoryProvider, instancesRepositoryProvider, storagePathProvider, changeLockProvider, formSourceProvider);
    }

    @Provides
    public BlankFormListViewModel.Factory providesBlankFormListViewModel(FormsRepositoryProvider formsRepositoryProvider, InstancesRepositoryProvider instancesRepositoryProvider, Application application, FormsDataService formsDataService, Scheduler scheduler, SettingsProvider settingsProvider, ChangeLockProvider changeLockProvider, ProjectsDataService projectsDataService) {
        return new BlankFormListViewModel.Factory(instancesRepositoryProvider.get(), application, formsDataService, scheduler, settingsProvider.getUnprotectedSettings(), projectsDataService.getCurrentProject().getUuid());
    }

    @Provides
    @Singleton
    public ImageCompressionController providesImageCompressorManager() {
        return new ImageCompressionController(ImageCompressor.INSTANCE);
    }

    @Provides
    public FormLoaderTask.FormEntryControllerFactory formEntryControllerFactory(SettingsProvider settingsProvider) {
        return new CollectFormEntryControllerFactory();
    }
}
