package net.lalibre.collect.otrosmapas.preferences.dialogs

import android.content.Context
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.material.MaterialProgressDialogFragment
import net.lalibre.collect.strings.localization.getLocalizedString

class ResetProgressDialog : MaterialProgressDialogFragment() {
    override fun onAttach(context: Context) {
        super.onAttach(context)

        setTitle(context.getLocalizedString(net.lalibre.collect.strings.R.string.please_wait))
        setMessage(context.getLocalizedString(net.lalibre.collect.strings.R.string.reset_in_progress))
        isCancelable = false
    }
}
