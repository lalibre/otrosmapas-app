package net.lalibre.collect.otrosmapas.injection.config

import android.app.Application
import android.content.Context
import android.location.LocationManager
import androidx.fragment.app.FragmentActivity
import net.lalibre.collect.otrosmapas.preferences.screens.MapsPreferencesFragment
import net.lalibre.collect.async.Scheduler
import net.lalibre.collect.geo.GeoDependencyModule
import net.lalibre.collect.geo.ReferenceLayerSettingsNavigator
import net.lalibre.collect.location.LocationClient
import net.lalibre.collect.location.satellites.GpsStatusSatelliteInfoClient
import net.lalibre.collect.location.satellites.SatelliteInfoClient
import net.lalibre.collect.location.tracker.ForegroundServiceLocationTracker
import net.lalibre.collect.location.tracker.LocationTracker
import net.lalibre.collect.maps.MapFragmentFactory
import net.lalibre.collect.permissions.PermissionsChecker

class CollectGeoDependencyModule(
    private val mapFragmentFactory: MapFragmentFactory,
    private val locationClient: LocationClient,
    private val scheduler: Scheduler,
    private val permissionChecker: PermissionsChecker
) : GeoDependencyModule() {

    override fun providesReferenceLayerSettingsNavigator(): ReferenceLayerSettingsNavigator {
        return object : ReferenceLayerSettingsNavigator {
            override fun navigateToReferenceLayerSettings(activity: FragmentActivity) {
                MapsPreferencesFragment.showReferenceLayerDialog(activity)
            }
        }
    }

    override fun providesMapFragmentFactory(): MapFragmentFactory {
        return mapFragmentFactory
    }

    override fun providesLocationTracker(application: Application): LocationTracker {
        return ForegroundServiceLocationTracker(application)
    }

    override fun providesLocationClient(): LocationClient {
        return locationClient
    }

    override fun providesScheduler(): Scheduler {
        return scheduler
    }

    override fun providesSatelliteInfoClient(context: Context): SatelliteInfoClient {
        val locationManager =
            context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return GpsStatusSatelliteInfoClient(locationManager)
    }

    override fun providesPermissionChecker(context: Context): PermissionsChecker {
        return permissionChecker
    }
}
