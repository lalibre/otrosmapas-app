/*
 * Copyright (C) 2017 University of Washington
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package net.lalibre.collect.otrosmapas.application;

import static net.lalibre.collect.settings.keys.MetaKeys.KEY_GOOGLE_BUG_154855417_FIXED;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.StrictMode;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import org.jetbrains.annotations.NotNull;
import net.lalibre.collect.otrosmapas.BuildConfig;
import net.lalibre.collect.otrosmapas.externaldata.ExternalDataManager;
import net.lalibre.collect.otrosmapas.injection.DaggerUtils;
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyComponent;
import net.lalibre.collect.otrosmapas.injection.config.CollectGeoDependencyModule;
import net.lalibre.collect.otrosmapas.injection.config.CollectGoogleMapsDependencyModule;
import net.lalibre.collect.otrosmapas.injection.config.CollectOsmDroidDependencyModule;
import net.lalibre.collect.otrosmapas.injection.config.CollectProjectsDependencyModule;
import net.lalibre.collect.otrosmapas.injection.config.CollectSelfieCameraDependencyModule;
import net.lalibre.collect.otrosmapas.injection.config.DaggerAppDependencyComponent;
import net.lalibre.collect.otrosmapas.utilities.FormsRepositoryProvider;
import net.lalibre.collect.otrosmapas.utilities.LocaleHelper;
import net.lalibre.collect.otrosmapasshared.data.AppState;
import net.lalibre.collect.otrosmapasshared.data.StateStore;
import net.lalibre.collect.otrosmapasshared.network.NetworkStateProvider;
import net.lalibre.collect.otrosmapasshared.system.ExternalFilesUtils;
import net.lalibre.collect.audiorecorder.AudioRecorderDependencyComponent;
import net.lalibre.collect.audiorecorder.AudioRecorderDependencyComponentProvider;
import net.lalibre.collect.audiorecorder.DaggerAudioRecorderDependencyComponent;
import net.lalibre.collect.crashhandler.CrashHandler;
import net.lalibre.collect.entities.DaggerEntitiesDependencyComponent;
import net.lalibre.collect.entities.EntitiesDependencyComponent;
import net.lalibre.collect.entities.EntitiesDependencyComponentProvider;
import net.lalibre.collect.entities.EntitiesDependencyModule;
import net.lalibre.collect.entities.EntitiesRepository;
import net.lalibre.collect.forms.Form;
import net.lalibre.collect.geo.DaggerGeoDependencyComponent;
import net.lalibre.collect.geo.GeoDependencyComponent;
import net.lalibre.collect.geo.GeoDependencyComponentProvider;
import net.lalibre.collect.googlemaps.DaggerGoogleMapsDependencyComponent;
import net.lalibre.collect.googlemaps.GoogleMapsDependencyComponent;
import net.lalibre.collect.googlemaps.GoogleMapsDependencyComponentProvider;
import net.lalibre.collect.location.LocationClient;
import net.lalibre.collect.maps.layers.ReferenceLayerRepository;
import net.lalibre.collect.osmdroid.DaggerOsmDroidDependencyComponent;
import net.lalibre.collect.osmdroid.OsmDroidDependencyComponent;
import net.lalibre.collect.osmdroid.OsmDroidDependencyComponentProvider;
import net.lalibre.collect.projects.DaggerProjectsDependencyComponent;
import net.lalibre.collect.projects.ProjectsDependencyComponent;
import net.lalibre.collect.projects.ProjectsDependencyComponentProvider;
import net.lalibre.collect.selfiecamera.DaggerSelfieCameraDependencyComponent;
import net.lalibre.collect.selfiecamera.SelfieCameraDependencyComponent;
import net.lalibre.collect.selfiecamera.SelfieCameraDependencyComponentProvider;
import net.lalibre.collect.settings.SettingsProvider;
import net.lalibre.collect.settings.keys.ProjectKeys;
import net.lalibre.collect.shared.injection.ObjectProvider;
import net.lalibre.collect.shared.injection.ObjectProviderHost;
import net.lalibre.collect.shared.injection.SupplierObjectProvider;
import net.lalibre.collect.shared.settings.Settings;
import net.lalibre.collect.shared.strings.Md5;
import net.lalibre.collect.strings.localization.LocalizedApplication;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Locale;

@SuppressWarnings("PMD.CouplingBetweenObjects")
public class Collect extends Application implements
        LocalizedApplication,
        AudioRecorderDependencyComponentProvider,
        ProjectsDependencyComponentProvider,
        GeoDependencyComponentProvider,
        OsmDroidDependencyComponentProvider,
        StateStore,
        ObjectProviderHost,
        EntitiesDependencyComponentProvider,
        SelfieCameraDependencyComponentProvider,
        GoogleMapsDependencyComponentProvider {

    public static String defaultSysLanguage;
    private static Collect singleton;

    private final AppState appState = new AppState();
    private final SupplierObjectProvider objectProvider = new SupplierObjectProvider();

    private ExternalDataManager externalDataManager;
    private AppDependencyComponent applicationComponent;

    private AudioRecorderDependencyComponent audioRecorderDependencyComponent;
    private ProjectsDependencyComponent projectsDependencyComponent;
    private GeoDependencyComponent geoDependencyComponent;
    private OsmDroidDependencyComponent osmDroidDependencyComponent;
    private EntitiesDependencyComponent entitiesDependencyComponent;
    private SelfieCameraDependencyComponent selfieCameraDependencyComponent;
    private GoogleMapsDependencyComponent googleMapsDependencyComponent;

    /**
     * @deprecated we shouldn't have to reference a static singleton of the application. Code doing this
     * should either have a {@link Context} instance passed to it (or have any references removed if
     * possible).
     */
    @Deprecated
    public static Collect getInstance() {
        return singleton;
    }

    public ExternalDataManager getExternalDataManager() {
        return externalDataManager;
    }

    public void setExternalDataManager(ExternalDataManager externalDataManager) {
        this.externalDataManager = externalDataManager;
    }

    /*
        Adds support for multidex support library. For more info check out the link below,
        https://developer.android.com/studio/build/multidex.html
    */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        CrashHandler.install(this).launchApp(
                () -> ExternalFilesUtils.testExternalFilesAccess(this),
                () -> {
                    setupDagger();
                    DaggerUtils.getComponent(this).inject(this);

                    applicationComponent.applicationInitializer().initialize();
                    fixGoogleBug154855417();
                    setupStrictMode();
                }
        );
    }

    /**
     * Enable StrictMode and log violations to the system log.
     * This catches disk and network access on the main thread, as well as leaked SQLite
     * cursors and unclosed resources.
     */
    private void setupStrictMode() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .permitDiskReads()  // shared preferences are being read on main thread
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
        }
    }

    private void setupDagger() {
        applicationComponent = DaggerAppDependencyComponent.builder()
                .application(this)
                .build();

        audioRecorderDependencyComponent = DaggerAudioRecorderDependencyComponent.builder()
                .application(this)
                .build();

        projectsDependencyComponent = DaggerProjectsDependencyComponent.builder()
                .projectsDependencyModule(new CollectProjectsDependencyModule(applicationComponent.projectsRepository()))
                .build();

        selfieCameraDependencyComponent = DaggerSelfieCameraDependencyComponent.builder()
                .selfieCameraDependencyModule(new CollectSelfieCameraDependencyModule(applicationComponent::permissionsChecker))
                .build();

        // Mapbox dependencies
        objectProvider.addSupplier(SettingsProvider.class, applicationComponent::settingsProvider);
        objectProvider.addSupplier(NetworkStateProvider.class, applicationComponent::networkStateProvider);
        objectProvider.addSupplier(ReferenceLayerRepository.class, applicationComponent::referenceLayerRepository);
        objectProvider.addSupplier(LocationClient.class, applicationComponent::locationClient);
    }

    @NotNull
    @Override
    public AudioRecorderDependencyComponent getAudioRecorderDependencyComponent() {
        return audioRecorderDependencyComponent;
    }

    @NotNull
    @Override
    public ProjectsDependencyComponent getProjectsDependencyComponent() {
        return projectsDependencyComponent;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        //noinspection deprecation
        defaultSysLanguage = newConfig.locale.getLanguage();
    }

    public AppDependencyComponent getComponent() {
        return applicationComponent;
    }

    public void setComponent(AppDependencyComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
        applicationComponent.inject(this);
    }

    /**
     * Gets a unique, privacy-preserving identifier for a form based on its id and version.
     *
     * @param formId      id of a form
     * @param formVersion version of a form
     * @return md5 hash of the form title, a space, the form ID
     */
    public static String getFormIdentifierHash(String formId, String formVersion) {
        Form form = new FormsRepositoryProvider(Collect.getInstance()).get().getLatestByFormIdAndVersion(formId, formVersion);

        String formTitle = form != null ? form.getDisplayName() : "";

        String formIdentifier = formTitle + " " + formId;
        return Md5.getMd5Hash(new ByteArrayInputStream(formIdentifier.getBytes()));
    }

    // https://issuetracker.google.com/issues/154855417
    private void fixGoogleBug154855417() {
        try {
            Settings metaSharedPreferences = applicationComponent.settingsProvider().getMetaSettings();

            boolean hasFixedGoogleBug154855417 = metaSharedPreferences.getBoolean(KEY_GOOGLE_BUG_154855417_FIXED);

            if (!hasFixedGoogleBug154855417) {
                File corruptedZoomTables = new File(getFilesDir(), "ZoomTables.data");
                corruptedZoomTables.delete();

                metaSharedPreferences.save(KEY_GOOGLE_BUG_154855417_FIXED, true);
            }
        } catch (Exception ignored) {
            // ignored
        }
    }

    @NotNull
    @Override
    public Locale getLocale() {
        if (this.applicationComponent != null) {
            return LocaleHelper.getLocale(applicationComponent.settingsProvider().getUnprotectedSettings().getString(ProjectKeys.KEY_APP_LANGUAGE));
        } else {
            return getResources().getConfiguration().locale;
        }
    }

    @NotNull
    @Override
    public AppState getState() {
        return appState;
    }

    @NonNull
    @Override
    public GeoDependencyComponent getGeoDependencyComponent() {
        if (geoDependencyComponent == null) {
            geoDependencyComponent = DaggerGeoDependencyComponent.builder()
                    .application(this)
                    .geoDependencyModule(new CollectGeoDependencyModule(
                            applicationComponent.mapFragmentFactory(),
                            applicationComponent.locationClient(),
                            applicationComponent.scheduler(),
                            applicationComponent.permissionsChecker()
                    ))
                    .build();
        }

        return geoDependencyComponent;
    }

    @NonNull
    @Override
    public OsmDroidDependencyComponent getOsmDroidDependencyComponent() {
        if (osmDroidDependencyComponent == null) {
            osmDroidDependencyComponent = DaggerOsmDroidDependencyComponent.builder()
                    .osmDroidDependencyModule(new CollectOsmDroidDependencyModule(
                            applicationComponent.referenceLayerRepository(),
                            applicationComponent.locationClient(),
                            applicationComponent.settingsProvider()
                    ))
                    .build();
        }

        return osmDroidDependencyComponent;
    }

    @NonNull
    @Override
    public ObjectProvider getObjectProvider() {
        return objectProvider;
    }

    @NonNull
    @Override
    public EntitiesDependencyComponent getEntitiesDependencyComponent() {
        if (entitiesDependencyComponent == null) {
            entitiesDependencyComponent = DaggerEntitiesDependencyComponent.builder()
                    .entitiesDependencyModule(new EntitiesDependencyModule() {
                        @NonNull
                        @Override
                        public EntitiesRepository providesEntitiesRepository() {
                            String projectId = applicationComponent.currentProjectProvider().getCurrentProject().getUuid();
                            return applicationComponent.entitiesRepositoryProvider().get(projectId);
                        }
                    })
                    .build();
        }

        return entitiesDependencyComponent;
    }

    @NonNull
    @Override
    public SelfieCameraDependencyComponent getSelfieCameraDependencyComponent() {
        return selfieCameraDependencyComponent;
    }

    @NonNull
    @Override
    public GoogleMapsDependencyComponent getGoogleMapsDependencyComponent() {
        if (googleMapsDependencyComponent == null) {
            googleMapsDependencyComponent = DaggerGoogleMapsDependencyComponent.builder()
                    .googleMapsDependencyModule(new CollectGoogleMapsDependencyModule(
                            applicationComponent.referenceLayerRepository(),
                            applicationComponent.locationClient(),
                            applicationComponent.settingsProvider()
                    ))
                    .build();
        }

        return googleMapsDependencyComponent;
    }
}
