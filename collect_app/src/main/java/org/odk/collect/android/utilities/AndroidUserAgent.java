package net.lalibre.collect.otrosmapas.utilities;

import net.lalibre.collect.otrosmapas.BuildConfig;
import net.lalibre.collect.utilities.UserAgentProvider;

public final class AndroidUserAgent implements UserAgentProvider {

    @Override
    public String getUserAgent() {
        return String.format("%s/%s %s",
                BuildConfig.APPLICATION_ID,
                BuildConfig.VERSION_NAME,
                System.getProperty("http.agent"));
    }

}
