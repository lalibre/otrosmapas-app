package net.lalibre.collect.otrosmapas.formentry.media;

import android.content.Context;

import net.lalibre.collect.otrosmapas.audio.AudioHelper;

public interface AudioHelperFactory {

    AudioHelper create(Context context);
}
