package net.lalibre.collect.otrosmapas.formlists.sorting

data class FormListSortingOption(
    val icon: Int,
    val text: Int
)
