package net.lalibre.collect.otrosmapas.widgets.utilities;

import org.javarosa.form.api.FormEntryPrompt;

public interface RecordingRequester {
    void requestRecording(FormEntryPrompt prompt);
}
