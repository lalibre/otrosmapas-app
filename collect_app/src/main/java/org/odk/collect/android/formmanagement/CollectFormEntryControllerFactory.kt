package net.lalibre.collect.otrosmapas.formmanagement

import org.javarosa.core.model.FormDef
import org.javarosa.entities.EntityFormFinalizationProcessor
import org.javarosa.form.api.FormEntryController
import org.javarosa.form.api.FormEntryModel
import net.lalibre.collect.otrosmapas.application.Collect
import net.lalibre.collect.otrosmapas.externaldata.ExternalDataManagerImpl
import net.lalibre.collect.otrosmapas.externaldata.handler.ExternalDataHandlerPull
import net.lalibre.collect.otrosmapas.tasks.FormLoaderTask.FormEntryControllerFactory
import java.io.File

class CollectFormEntryControllerFactory :
    FormEntryControllerFactory {
    override fun create(formDef: FormDef, formMediaDir: File): FormEntryController {
        val externalDataManager = ExternalDataManagerImpl(formMediaDir).also {
            Collect.getInstance().externalDataManager = it
        }

        val externalDataHandlerPull = ExternalDataHandlerPull(externalDataManager)
        formDef.evaluationContext.addFunctionHandler(externalDataHandlerPull)

        return FormEntryController(FormEntryModel(formDef)).also {
            it.addPostProcessor(EntityFormFinalizationProcessor())
        }
    }
}
