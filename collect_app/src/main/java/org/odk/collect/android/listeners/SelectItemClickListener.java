package net.lalibre.collect.otrosmapas.listeners;

public interface SelectItemClickListener {
    void onItemClicked();
}
