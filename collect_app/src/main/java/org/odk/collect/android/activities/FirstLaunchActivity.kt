package net.lalibre.collect.otrosmapas.activities

import android.os.Bundle
import net.lalibre.collect.analytics.Analytics
import net.lalibre.collect.otrosmapas.analytics.AnalyticsEvents
import net.lalibre.collect.otrosmapas.databinding.FirstLaunchLayoutBinding
import net.lalibre.collect.otrosmapas.injection.DaggerUtils
import net.lalibre.collect.otrosmapas.mainmenu.MainMenuActivity
import net.lalibre.collect.otrosmapas.projects.ManualProjectCreatorDialog
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService
import net.lalibre.collect.otrosmapas.projects.QrCodeProjectCreatorDialog
import net.lalibre.collect.otrosmapas.version.VersionInformation
import net.lalibre.collect.otrosmapasshared.ui.DialogFragmentUtils
import net.lalibre.collect.otrosmapasshared.ui.GroupClickListener.addOnClickListener
import net.lalibre.collect.projects.Project
import net.lalibre.collect.projects.ProjectsRepository
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.strings.localization.LocalizedActivity
import javax.inject.Inject

class FirstLaunchActivity : LocalizedActivity() {

    @Inject
    lateinit var projectsRepository: ProjectsRepository

    @Inject
    lateinit var versionInformation: VersionInformation

    @Inject
    lateinit var projectsDataService: ProjectsDataService

    @Inject
    lateinit var settingsProvider: SettingsProvider

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerUtils.getComponent(this).inject(this)

        FirstLaunchLayoutBinding.inflate(layoutInflater).apply {
            setContentView(this.root)

            configureViaQrButton.setOnClickListener {
                DialogFragmentUtils.showIfNotShowing(
                    QrCodeProjectCreatorDialog::class.java,
                    supportFragmentManager
                )
            }

            configureManuallyButton.setOnClickListener {
                DialogFragmentUtils.showIfNotShowing(
                    ManualProjectCreatorDialog::class.java,
                    supportFragmentManager
                )
            }

            appName.text = String.format(
                "%s %s",
                getString(net.lalibre.collect.strings.R.string.collect_app_name),
                versionInformation.versionToDisplay
            )

            configureLater.addOnClickListener {
                Analytics.log(AnalyticsEvents.TRY_DEMO)

                projectsRepository.save(Project.DEMO_PROJECT)
                projectsDataService.setCurrentProject(Project.DEMO_PROJECT_ID)

                ActivityUtils.startActivityAndCloseAllOthers(
                    this@FirstLaunchActivity,
                    MainMenuActivity::class.java
                )
            }
        }
    }
}
