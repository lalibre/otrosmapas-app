package net.lalibre.collect.otrosmapas.version;

public interface VersionDescriptionProvider {
    String getVersionDescription();
}