package net.lalibre.collect.otrosmapas.projects

import net.lalibre.collect.projects.Project
import net.lalibre.collect.projects.ProjectsRepository
import net.lalibre.collect.settings.ODKAppSettingsImporter
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.importing.SettingsImportingResult

class ProjectCreator(
    private val projectsRepository: ProjectsRepository,
    private val projectsDataService: ProjectsDataService,
    private val settingsImporter: ODKAppSettingsImporter,
    private val settingsProvider: SettingsProvider
) {

    fun createNewProject(settingsJson: String): SettingsImportingResult {
        val savedProject = projectsRepository.save(Project.New("", "", ""))
        val settingsImportingResult = settingsImporter.fromJSON(settingsJson, savedProject)

        return if (settingsImportingResult == SettingsImportingResult.SUCCESS) {
            projectsDataService.setCurrentProject(savedProject.uuid)
            settingsImportingResult
        } else {
            settingsProvider.getUnprotectedSettings(savedProject.uuid).clear()
            settingsProvider.getProtectedSettings(savedProject.uuid).clear()
            projectsRepository.delete(savedProject.uuid)
            settingsImportingResult
        }
    }
}
