package net.lalibre.collect.otrosmapas.mainmenu

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.lalibre.collect.otrosmapas.application.initialization.AnalyticsInitializer
import net.lalibre.collect.otrosmapas.formmanagement.InstancesDataService
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.AutoSendSettingsProvider
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService
import net.lalibre.collect.otrosmapas.utilities.FormsRepositoryProvider
import net.lalibre.collect.otrosmapas.utilities.InstancesRepositoryProvider
import net.lalibre.collect.otrosmapas.version.VersionInformation
import net.lalibre.collect.async.Scheduler
import net.lalibre.collect.permissions.PermissionsChecker
import net.lalibre.collect.settings.SettingsProvider

open class MainMenuViewModelFactory(
    private val versionInformation: VersionInformation,
    private val application: Application,
    private val settingsProvider: SettingsProvider,
    private val instancesDataService: InstancesDataService,
    private val scheduler: Scheduler,
    private val projectsDataService: ProjectsDataService,
    private val analyticsInitializer: AnalyticsInitializer,
    private val permissionChecker: PermissionsChecker,
    private val formsRepositoryProvider: FormsRepositoryProvider,
    private val instancesRepositoryProvider: InstancesRepositoryProvider,
    private val autoSendSettingsProvider: AutoSendSettingsProvider
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass) {
            MainMenuViewModel::class.java -> MainMenuViewModel(
                application,
                versionInformation,
                settingsProvider,
                instancesDataService,
                scheduler,
                formsRepositoryProvider,
                instancesRepositoryProvider,
                autoSendSettingsProvider
            )

            CurrentProjectViewModel::class.java -> CurrentProjectViewModel(
                projectsDataService
            )

            RequestPermissionsViewModel::class.java -> RequestPermissionsViewModel(
                settingsProvider,
                permissionChecker
            )

            else -> throw IllegalArgumentException()
        } as T
    }
}
