package net.lalibre.collect.otrosmapas.application.initialization.upgrade

import android.content.Context
import net.lalibre.collect.otrosmapas.BuildConfig
import net.lalibre.collect.otrosmapas.application.initialization.ExistingProjectMigrator
import net.lalibre.collect.otrosmapas.application.initialization.ExistingSettingsMigrator
import net.lalibre.collect.otrosmapas.application.initialization.FormUpdatesUpgrade
import net.lalibre.collect.otrosmapas.application.initialization.GoogleDriveProjectsDeleter
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.keys.MetaKeys
import net.lalibre.collect.upgrade.AppUpgrader

class UpgradeInitializer(
    private val context: Context,
    private val settingsProvider: SettingsProvider,
    private val existingProjectMigrator: ExistingProjectMigrator,
    private val existingSettingsMigrator: ExistingSettingsMigrator,
    private val formUpdatesUpgrade: FormUpdatesUpgrade,
    private val googleDriveProjectsDeleter: GoogleDriveProjectsDeleter
) {

    fun initialize() {
        AppUpgrader(
            MetaKeys.LAST_LAUNCHED,
            settingsProvider.getMetaSettings(),
            BuildConfig.VERSION_CODE,
            BeforeProjectsInstallDetector(context),
            listOf(
                existingProjectMigrator,
                existingSettingsMigrator,
                formUpdatesUpgrade,
                googleDriveProjectsDeleter
            )
        ).upgradeIfNeeded()
    }
}
