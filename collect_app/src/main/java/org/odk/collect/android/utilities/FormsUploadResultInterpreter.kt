package net.lalibre.collect.otrosmapas.utilities

import android.content.Context
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.otrosmapas.upload.FormUploadException
import net.lalibre.collect.errors.ErrorItem
import net.lalibre.collect.forms.instances.Instance
import net.lalibre.collect.strings.localization.getLocalizedString

object FormsUploadResultInterpreter {
    fun getFailures(result: Map<Instance, FormUploadException?>, context: Context) = result.filter {
        it.value != null
    }.map {
        ErrorItem(
            it.key.displayName,
            context.getLocalizedString(net.lalibre.collect.strings.R.string.form_details, it.key.formId ?: "", it.key.formVersion ?: ""),
            it.value?.message ?: ""
        )
    }

    fun getNumberOfFailures(result: Map<Instance, FormUploadException?>) = result.count {
        it.value != null
    }

    fun allFormsUploadedSuccessfully(result: Map<Instance, FormUploadException?>) = result.values.all {
        it == null
    }
}
