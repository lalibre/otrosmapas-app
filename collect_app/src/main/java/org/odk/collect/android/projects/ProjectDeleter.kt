package net.lalibre.collect.otrosmapas.projects

import net.lalibre.collect.otrosmapas.backgroundwork.FormUpdateScheduler
import net.lalibre.collect.otrosmapas.backgroundwork.InstanceSubmitScheduler
import net.lalibre.collect.otrosmapas.database.DatabaseConnection
import net.lalibre.collect.otrosmapas.storage.StoragePathProvider
import net.lalibre.collect.otrosmapas.utilities.ChangeLockProvider
import net.lalibre.collect.otrosmapas.utilities.InstancesRepositoryProvider
import net.lalibre.collect.forms.instances.Instance
import net.lalibre.collect.projects.Project
import net.lalibre.collect.projects.ProjectsRepository
import net.lalibre.collect.settings.SettingsProvider
import java.io.File

class ProjectDeleter(
    private val projectsRepository: ProjectsRepository,
    private val projectsDataService: ProjectsDataService,
    private val formUpdateScheduler: FormUpdateScheduler,
    private val instanceSubmitScheduler: InstanceSubmitScheduler,
    private val instancesRepositoryProvider: InstancesRepositoryProvider,
    private val storagePathProvider: StoragePathProvider,
    private val changeLockProvider: ChangeLockProvider,
    private val settingsProvider: SettingsProvider
) {
    fun deleteProject(projectId: String = projectsDataService.getCurrentProject().uuid): DeleteProjectResult {
        return when {
            unsentInstancesDetected(projectId) -> DeleteProjectResult.UnsentInstances
            runningBackgroundJobsDetected(projectId) -> DeleteProjectResult.RunningBackgroundJobs
            else -> performProjectDeletion(projectId)
        }
    }

    private fun unsentInstancesDetected(projectId: String): Boolean {
        return instancesRepositoryProvider.get(projectId).getAllByStatus(
            Instance.STATUS_INCOMPLETE,
            Instance.STATUS_INVALID,
            Instance.STATUS_VALID,
            Instance.STATUS_COMPLETE,
            Instance.STATUS_SUBMISSION_FAILED
        ).isNotEmpty()
    }

    private fun runningBackgroundJobsDetected(projectId: String): Boolean {
        val acquiredFormLock = changeLockProvider.getFormLock(projectId).withLock { acquiredLock ->
            acquiredLock
        }
        val acquiredInstanceLock = changeLockProvider.getInstanceLock(projectId).withLock { acquiredLock ->
            acquiredLock
        }

        return !acquiredFormLock || !acquiredInstanceLock
    }

    private fun performProjectDeletion(projectId: String): DeleteProjectResult {
        formUpdateScheduler.cancelUpdates(projectId)
        instanceSubmitScheduler.cancelSubmit(projectId)

        settingsProvider.getUnprotectedSettings(projectId).clear()
        settingsProvider.getProtectedSettings(projectId).clear()

        projectsRepository.delete(projectId)

        File(storagePathProvider.getProjectRootDirPath(projectId)).deleteRecursively()

        DatabaseConnection.cleanUp()

        return try {
            projectsDataService.getCurrentProject()
            DeleteProjectResult.DeletedSuccessfullyInactiveProject
        } catch (e: IllegalStateException) {
            if (projectsRepository.getAll().isEmpty()) {
                DeleteProjectResult.DeletedSuccessfullyLastProject
            } else {
                val newProject = projectsRepository.getAll()[0]
                projectsDataService.setCurrentProject(newProject.uuid)
                DeleteProjectResult.DeletedSuccessfullyCurrentProject(newProject)
            }
        }
    }
}

sealed class DeleteProjectResult {
    object UnsentInstances : DeleteProjectResult()

    object RunningBackgroundJobs : DeleteProjectResult()

    object DeletedSuccessfullyLastProject : DeleteProjectResult()

    object DeletedSuccessfullyInactiveProject : DeleteProjectResult()

    data class DeletedSuccessfullyCurrentProject(val newCurrentProject: Project.Saved) : DeleteProjectResult()
}
