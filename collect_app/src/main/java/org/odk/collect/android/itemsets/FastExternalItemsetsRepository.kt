package net.lalibre.collect.otrosmapas.itemsets

interface FastExternalItemsetsRepository {

    fun deleteAllByCsvPath(path: String)
}
