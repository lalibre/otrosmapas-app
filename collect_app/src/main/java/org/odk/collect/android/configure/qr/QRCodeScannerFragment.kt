package net.lalibre.collect.otrosmapas.configure.qr

import android.content.Context
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeResult
import net.lalibre.collect.analytics.Analytics
import net.lalibre.collect.otrosmapas.activities.ActivityUtils
import net.lalibre.collect.otrosmapas.analytics.AnalyticsEvents
import net.lalibre.collect.otrosmapas.fragments.BarCodeScannerFragment
import net.lalibre.collect.otrosmapas.injection.DaggerUtils
import net.lalibre.collect.otrosmapas.mainmenu.MainMenuActivity
import net.lalibre.collect.otrosmapas.projects.ProjectsDataService
import net.lalibre.collect.otrosmapas.storage.StoragePathProvider
import net.lalibre.collect.otrosmapasshared.ui.ToastUtils.showLongToast
import net.lalibre.collect.otrosmapasshared.utils.CompressionUtils
import net.lalibre.collect.settings.ODKAppSettingsImporter
import net.lalibre.collect.settings.importing.SettingsImportingResult
import java.io.File
import java.io.IOException
import java.util.zip.DataFormatException
import javax.inject.Inject

class QRCodeScannerFragment : BarCodeScannerFragment() {

    @Inject
    lateinit var settingsImporter: ODKAppSettingsImporter

    @Inject
    lateinit var projectsDataService: ProjectsDataService

    @Inject
    lateinit var storagePathProvider: StoragePathProvider

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerUtils.getComponent(context).inject(this)
    }

    @Throws(IOException::class, DataFormatException::class)
    override fun handleScanningResult(result: BarcodeResult) {
        val oldProjectName = projectsDataService.getCurrentProject().name

        val settingsImportingResult = settingsImporter.fromJSON(
            CompressionUtils.decompress(result.text),
            projectsDataService.getCurrentProject()
        )

        when (settingsImportingResult) {
            SettingsImportingResult.SUCCESS -> {
                Analytics.log(AnalyticsEvents.RECONFIGURE_PROJECT)

                val newProjectName = projectsDataService.getCurrentProject().name
                if (newProjectName != oldProjectName) {
                    File(storagePathProvider.getProjectRootDirPath() + File.separator + oldProjectName).delete()
                    File(storagePathProvider.getProjectRootDirPath() + File.separator + newProjectName).createNewFile()
                }

                showLongToast(
                    requireContext(),
                    getString(net.lalibre.collect.strings.R.string.successfully_imported_settings)
                )
                ActivityUtils.startActivityAndCloseAllOthers(
                    requireActivity(),
                    MainMenuActivity::class.java
                )
            }

            SettingsImportingResult.INVALID_SETTINGS -> showLongToast(
                requireContext(),
                getString(
                    net.lalibre.collect.strings.R.string.invalid_qrcode
                )
            )

            SettingsImportingResult.GD_PROJECT -> showLongToast(
                requireContext(),
                getString(net.lalibre.collect.strings.R.string.settings_with_gd_protocol)
            )
        }
    }

    override fun getSupportedCodeFormats(): Collection<String> {
        return listOf(IntentIntegrator.QR_CODE)
    }
}
