package net.lalibre.collect.otrosmapas.openrosa;

import org.jetbrains.annotations.Nullable;
import org.kxml2.kdom.Document;
import net.lalibre.collect.forms.FormListItem;
import net.lalibre.collect.forms.MediaFile;

import java.util.List;

public interface OpenRosaResponseParser {

    @Nullable List<FormListItem> parseFormList(Document document);
    @Nullable List<MediaFile> parseManifest(Document document);
}
