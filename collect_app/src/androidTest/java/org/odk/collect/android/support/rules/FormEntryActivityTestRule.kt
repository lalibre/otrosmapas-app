package net.lalibre.collect.otrosmapas.support.rules

import android.app.Activity
import android.app.Application
import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import org.junit.rules.ExternalResource
import net.lalibre.collect.otrosmapas.activities.FormFillingActivity
import net.lalibre.collect.otrosmapas.external.FormsContract
import net.lalibre.collect.otrosmapas.formmanagement.FormFillingIntentFactory
import net.lalibre.collect.otrosmapas.injection.DaggerUtils
import net.lalibre.collect.otrosmapas.storage.StorageSubdirectory
import net.lalibre.collect.otrosmapas.support.CollectHelpers
import net.lalibre.collect.otrosmapas.support.StorageUtils
import net.lalibre.collect.otrosmapas.support.pages.FormEntryPage
import net.lalibre.collect.otrosmapas.support.pages.FormHierarchyPage
import net.lalibre.collect.otrosmapas.support.pages.Page
import net.lalibre.collect.otrosmapastest.ActivityScenarioExtensions.saveInstanceState
import timber.log.Timber
import java.io.IOException

open class FormEntryActivityTestRule :
    ExternalResource() {

    private lateinit var intent: Intent
    private lateinit var scenario: ActivityScenario<Activity>

    override fun after() {
        try {
            scenario.close()
        } catch (e: Throwable) {
            Timber.e(Error("Error closing ActivityScenario: $e"))
        }
    }

    @JvmOverloads
    fun setUpProjectAndCopyForm(
        formFilename: String,
        mediaFilePaths: List<String>? = null
    ): FormEntryActivityTestRule {
        try {
            // Set up demo project
            CollectHelpers.addDemoProject()
            StorageUtils.copyFormToDemoProject(formFilename, mediaFilePaths, true)
        } catch (e: IOException) {
            throw RuntimeException(e)
        }

        return this
    }

    fun <D : Page<D>> fillNewForm(formFilename: String, destination: D): D {
        intent = createNewFormIntent(formFilename)
        scenario = ActivityScenario.launch(intent)
        return destination.assertOnPage()
    }

    fun fillNewForm(formFilename: String, formName: String): FormEntryPage {
        return fillNewForm(formFilename, FormEntryPage(formName))
    }

    fun editForm(formFilename: String, instanceName: String): FormHierarchyPage {
        intent = createEditFormIntent(formFilename)
        scenario = ActivityScenario.launch(intent)
        return FormHierarchyPage(instanceName).assertOnPage()
    }

    fun navigateAwayFromActivity(): FormEntryActivityTestRule {
        scenario.moveToState(Lifecycle.State.STARTED)
        scenario.saveInstanceState()
        return this
    }

    fun destroyActivity(): FormEntryActivityTestRule {
        scenario.moveToState(Lifecycle.State.DESTROYED)
        return this
    }

    fun simulateProcessRestart(): FormEntryActivityTestRule {
        CollectHelpers.simulateProcessRestart()
        return this
    }

    private fun createNewFormIntent(formFilename: String): Intent {
        val application = ApplicationProvider.getApplicationContext<Application>()
        val formPath = DaggerUtils.getComponent(application).storagePathProvider()
            .getOdkDirPath(StorageSubdirectory.FORMS) + "/" + formFilename
        val form = DaggerUtils.getComponent(application).formsRepositoryProvider().get()
            .getOneByPath(formPath)
        val projectId = DaggerUtils.getComponent(application).currentProjectProvider()
            .getCurrentProject().uuid

        return FormFillingIntentFactory.newInstanceIntent(
            application,
            FormsContract.getUri(projectId, form!!.dbId),
            FormFillingActivity::class
        )
    }

    private fun createEditFormIntent(formFilename: String): Intent {
        val application = ApplicationProvider.getApplicationContext<Application>()
        val formPath = DaggerUtils.getComponent(application).storagePathProvider()
            .getOdkDirPath(StorageSubdirectory.FORMS) + "/" + formFilename
        val form = DaggerUtils.getComponent(application).formsRepositoryProvider().get()
            .getOneByPath(formPath)
        val instance = DaggerUtils.getComponent(application).instancesRepositoryProvider().get()
            .getAllByFormId(form!!.formId).first()
        val projectId = DaggerUtils.getComponent(application).currentProjectProvider()
            .getCurrentProject().uuid

        return FormFillingIntentFactory.editInstanceIntent(
            application,
            projectId,
            instance.dbId,
            FormFillingActivity::class
        )
    }
}
