package net.lalibre.collect.otrosmapas.support.pages

import net.lalibre.collect.otrosmapas.R

class FirstLaunchPage : Page<FirstLaunchPage>() {

    override fun assertOnPage(): FirstLaunchPage {
        assertText(net.lalibre.collect.strings.R.string.configure_with_qr_code)
        return this
    }

    fun clickTryCollect(): MainMenuPage {
        scrollToAndClickText(net.lalibre.collect.strings.R.string.try_demo)
        return MainMenuPage().assertOnPage()
    }

    fun clickManuallyEnterProjectDetails(): ManualProjectCreatorDialogPage {
        scrollToAndClickText(net.lalibre.collect.strings.R.string.configure_manually)
        return ManualProjectCreatorDialogPage().assertOnPage()
    }

    fun clickConfigureWithQrCode(): QrCodeProjectCreatorDialogPage {
        scrollToAndClickText(net.lalibre.collect.strings.R.string.configure_with_qr_code)
        return QrCodeProjectCreatorDialogPage().assertOnPage()
    }
}
