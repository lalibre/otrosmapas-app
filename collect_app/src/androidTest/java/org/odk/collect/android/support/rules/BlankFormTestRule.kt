package net.lalibre.collect.otrosmapas.support.rules

import net.lalibre.collect.otrosmapas.support.pages.FormEntryPage

class BlankFormTestRule @JvmOverloads constructor(
    private val formFilename: String,
    private val formName: String,
    private val mediaFilePaths: List<String>? = null
) : FormEntryActivityTestRule() {

    private lateinit var formEntryPage: FormEntryPage

    override fun before() {
        super.before()
        setUpProjectAndCopyForm(formFilename, mediaFilePaths)
        formEntryPage = fillNewForm(formFilename, formName)
    }

    fun startInFormEntry(): FormEntryPage {
        return formEntryPage
    }
}
