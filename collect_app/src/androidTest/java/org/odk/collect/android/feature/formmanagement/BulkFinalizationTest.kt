package net.lalibre.collect.otrosmapas.feature.formmanagement

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith
import net.lalibre.collect.otrosmapas.support.TestDependencies
import net.lalibre.collect.otrosmapas.support.pages.AccessControlPage
import net.lalibre.collect.otrosmapas.support.pages.EditSavedFormPage
import net.lalibre.collect.otrosmapas.support.pages.FormEntryPage.QuestionAndAnswer
import net.lalibre.collect.otrosmapas.support.pages.MainMenuPage
import net.lalibre.collect.otrosmapas.support.pages.ProjectSettingsPage
import net.lalibre.collect.otrosmapas.support.pages.SaveOrDiscardFormDialog
import net.lalibre.collect.otrosmapas.support.rules.CollectTestRule
import net.lalibre.collect.otrosmapas.support.rules.TestRuleChain
import net.lalibre.collect.strings.R.plurals
import net.lalibre.collect.strings.R.string

@RunWith(AndroidJUnit4::class)
class BulkFinalizationTest {

    val testDependencies = TestDependencies()
    val rule = CollectTestRule(useDemoProject = false)

    @get:Rule
    val chain: RuleChain = TestRuleChain.chain(testDependencies).around(rule)

    @Test
    fun canBulkFinalizeDrafts() {
        rule.withProject("http://example.com")
            .copyForm("one-question.xml", "example.com")
            .startBlankForm("One Question")
            .fillOutAndSave(QuestionAndAnswer("what is your age", "97"))
            .startBlankForm("One Question")
            .fillOutAndSave(QuestionAndAnswer("what is your age", "98"))

            .clickDrafts(2)
            .clickFinalizeAll(2)
            .clickFinalize()
            .checkIsSnackbarWithQuantityDisplayed(plurals.bulk_finalize_success, 2)
            .assertTextDoesNotExist("One Question")
            .pressBack(MainMenuPage())

            .assertNumberOfFinalizedForms(2)
    }

    @Test
    fun whenThereAreDraftsWithConstraintViolations_marksFormsAsHavingErrors() {
        rule.withProject("http://example.com")
            .copyForm("two-question-required.xml", "example.com")
            .startBlankForm("Two Question Required")
            .fillOut(QuestionAndAnswer("What is your name?", "Dan"))
            .pressBack(SaveOrDiscardFormDialog(MainMenuPage()))
            .clickSaveChanges()

            .startBlankForm("Two Question Required")
            .fillOutAndSave(
                QuestionAndAnswer("What is your name?", "Tim"),
                QuestionAndAnswer("What is your age?", "45", true)
            )

            .clickDrafts(2)
            .clickFinalizeAll(2)
            .clickFinalize()
            .checkIsSnackbarWithMessageDisplayed(string.bulk_finalize_partial_success, 1, 1)
            .assertText("Two Question Required")
            .pressBack(MainMenuPage())

            .assertNumberOfEditableForms(1)
            .assertNumberOfFinalizedForms(1)
    }

    @Test
    fun whenADraftPreviouslyHadConstraintViolations_marksFormsAsHavingErrors() {
        rule.withProject("http://example.com")
            .copyForm("two-question-required.xml", "example.com")
            .startBlankForm("Two Question Required")
            .fillOut(QuestionAndAnswer("What is your name?", "Dan"))
            .pressBack(SaveOrDiscardFormDialog(MainMenuPage()))
            .clickSaveChanges()

            .clickDrafts(1)
            .clickFinalizeAll(1)
            .clickFinalize()
            .checkIsSnackbarWithQuantityDisplayed(plurals.bulk_finalize_failure, 1)

            .clickOptionsIcon(string.finalize_all_drafts)
            .clickOnString(string.finalize_all_drafts)
            .clickOnButtonInDialog(string.finalize, EditSavedFormPage(false))
            .checkIsSnackbarWithQuantityDisplayed(plurals.bulk_finalize_failure, 1)
    }

    @Test
    fun doesNotFinalizeInstancesWithSavePoints() {
        rule.withProject("http://example.com")
            .copyForm("one-question.xml", "example.com")
            .startBlankForm("One Question")
            .swipeToEndScreen()
            .clickSaveAsDraft()

            .clickDrafts()
            .clickOnForm("One Question")
            .killAndReopenApp(MainMenuPage())

            .clickDrafts(false)
            .clickFinalizeAll(1)
            .clickFinalize()
            .checkIsSnackbarWithMessageDisplayed(string.bulk_finalize_unsupported, 0)
            .assertText("One Question")
            .pressBack(MainMenuPage())

            .assertNumberOfEditableForms(1)
            .assertNumberOfFinalizedForms(0)
    }

    @Test
    fun doesNotFinalizeInstancesFromEncryptedForms() {
        rule.withProject("http://example.com")
            .copyForm("encrypted.xml", "example.com")
            .startBlankForm("encrypted")
            .swipeToEndScreen()
            .clickSaveAsDraft()

            .clickDrafts(1)
            .clickFinalizeAll(1)
            .clickFinalize()
            .checkIsSnackbarWithMessageDisplayed(string.bulk_finalize_unsupported, 0)
            .assertText("encrypted")
            .pressBack(MainMenuPage())

            .assertNumberOfEditableForms(1)
            .assertNumberOfFinalizedForms(0)
    }

    @Test
    fun doesNotFinalizeAlreadyFinalizedInstances() {
        rule.withProject("http://example.com")
            .copyForm("one-question.xml", "example.com")
            .startBlankForm("One Question")
            .fillOutAndSave(QuestionAndAnswer("what is your age", "97"))
            .startBlankForm("One Question")
            .fillOutAndFinalize(QuestionAndAnswer("what is your age", "98"))

            .clickDrafts(1)
            .clickFinalizeAll(1)
            .clickFinalize()
            .checkIsSnackbarWithQuantityDisplayed(plurals.bulk_finalize_success, 1)
            .assertTextDoesNotExist("One Question")
            .pressBack(MainMenuPage())

            .assertNumberOfFinalizedForms(2)
    }

    @Test
    fun whenAutoSendIsEnabled_draftsAreSentAfterFinalizing() {
        val mainMenuPage = rule.withProject(testDependencies.server.url)
            .enableAutoSend()

            .copyForm("one-question.xml", testDependencies.server.hostName)
            .startBlankForm("One Question")
            .fillOutAndSave(QuestionAndAnswer("what is your age", "97"))

            .clickDrafts(1)
            .clickFinalizeAll(1)
            .clickFinalize()
            .pressBack(MainMenuPage())

        testDependencies.scheduler.runDeferredTasks()

        mainMenuPage.clickViewSentForm(1)
            .assertText("One Question")

        assertThat(testDependencies.server.submissions.size, equalTo(1))
    }

    @Test
    fun canCancel() {
        rule.withProject("http://example.com")
            .copyForm("one-question.xml", "example.com")
            .startBlankForm("One Question")
            .fillOutAndSave(QuestionAndAnswer("what is your age", "97"))

            .clickDrafts(1)
            .clickFinalizeAll(1)
            .clickCancel()
            .assertText("One Question")
            .pressBack(MainMenuPage())

            .assertNumberOfEditableForms(1)
    }

    @Test
    fun canBeDisabled() {
        rule.withProject("http://example.com")
            .openProjectSettingsDialog()
            .clickSettings()
            .clickAccessControl()
            .clickFormEntrySettings()
            .clickOnString(string.finalize_all_drafts)
            .pressBack(AccessControlPage())
            .pressBack(ProjectSettingsPage())
            .pressBack(MainMenuPage())

            .copyForm("one-question.xml", "example.com")
            .startBlankForm("One Question")
            .fillOutAndSave(QuestionAndAnswer("what is your age", "1892"))
            .clickDrafts()
            .assertNoOptionsMenu()
    }
}
