package net.lalibre.collect.otrosmapas.support.pages;

public class ErrorDialog extends OkDialog {

    @Override
    public ErrorDialog assertOnPage() {
        super.assertOnPage();
        assertText(net.lalibre.collect.strings.R.string.error_occured);
        return this;
    }
}
