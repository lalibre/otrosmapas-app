package net.lalibre.collect.otrosmapas.support.rules

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import net.lalibre.collect.otrosmapas.database.DatabaseConnection.Companion.closeAll
import net.lalibre.collect.otrosmapas.injection.DaggerUtils
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyComponent
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyModule
import net.lalibre.collect.otrosmapas.support.CollectHelpers
import net.lalibre.collect.otrosmapas.views.DecoratedBarcodeView
import net.lalibre.collect.otrosmapasshared.data.getState
import net.lalibre.collect.otrosmapasshared.ui.ToastUtils
import net.lalibre.collect.otrosmapasshared.ui.multiclicksafe.MultiClickGuard
import net.lalibre.collect.material.BottomSheetBehavior
import net.lalibre.collect.shared.files.DirectoryUtils
import java.io.File
import java.io.IOException

private class ResetStateStatement(
    private val base: Statement,
    private val appDependencyModule: AppDependencyModule? = null
) : Statement() {

    override fun evaluate() {
        val application = ApplicationProvider.getApplicationContext<Application>()
        val oldComponent = DaggerUtils.getComponent(application)

        clearPrefs(oldComponent)
        clearDisk(oldComponent)
        setTestState()
        CollectHelpers.simulateProcessRestart(appDependencyModule)
        base.evaluate()
    }

    private fun clearAppState(application: Application) {
        application.getState().clear()
    }

    private fun setTestState() {
        MultiClickGuard.test = true
        DecoratedBarcodeView.test = true
        ToastUtils.recordToasts = true
        BottomSheetBehavior.DRAGGING_ENABLED = false
    }

    private fun clearDisk(component: AppDependencyComponent) {
        try {
            DirectoryUtils.deleteDirectory(File(component.storagePathProvider().odkRootDirPath))
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
        closeAll()
    }

    private fun clearPrefs(component: AppDependencyComponent) {
        val projectIds = component.projectsRepository().getAll().map { it.uuid }
        component.settingsProvider().clearAll(projectIds)
    }
}

class ResetStateRule @JvmOverloads constructor(
    private val appDependencyModule: AppDependencyModule? = null
) : TestRule {

    override fun apply(base: Statement, description: Description): Statement =
        ResetStateStatement(base, appDependencyModule)
}
