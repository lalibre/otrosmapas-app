package net.lalibre.collect.otrosmapas.support.pages

import net.lalibre.collect.otrosmapas.R

class SaveOrIgnoreDrawingDialog<D : Page<D>>(
    private val drawingName: String,
    private val destination: D
) : Page<SaveOrIgnoreDrawingDialog<D>>() {

    override fun assertOnPage(): SaveOrIgnoreDrawingDialog<D> {
        val title = getTranslatedString(net.lalibre.collect.strings.R.string.exit) + " " + drawingName
        assertText(title)
        return this
    }

    fun clickSaveChanges(): D {
        clickOnString(net.lalibre.collect.strings.R.string.keep_changes)
        return destination.assertOnPage()
    }

    fun clickDiscardChanges(): D {
        clickOnString(net.lalibre.collect.strings.R.string.discard_changes)
        return destination.assertOnPage()
    }
}
