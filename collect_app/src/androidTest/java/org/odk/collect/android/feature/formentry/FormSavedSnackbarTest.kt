package net.lalibre.collect.otrosmapas.feature.formentry

import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import net.lalibre.collect.otrosmapas.R
import net.lalibre.collect.otrosmapas.support.pages.MainMenuPage
import net.lalibre.collect.otrosmapas.support.rules.CollectTestRule
import net.lalibre.collect.otrosmapas.support.rules.TestRuleChain

class FormSavedSnackbarTest {
    private val rule = CollectTestRule()

    @get:Rule
    val copyFormChain: RuleChain = TestRuleChain.chain().around(rule)

    @Test
    fun whenBlankFormSavedAsDraft_displaySnackbarWithEditAction() {
        rule.startAtMainMenu()
            .copyForm("one-question.xml")
            .startBlankForm("One Question")
            .answerQuestion(0, "25")
            .swipeToEndScreen()
            .clickSaveAsDraft()
            .assertText(net.lalibre.collect.strings.R.string.form_saved_as_draft)
            .clickOnString(net.lalibre.collect.strings.R.string.edit_form)
            .assertText("25")
            .assertText(net.lalibre.collect.strings.R.string.jump_to_beginning)
            .assertText(net.lalibre.collect.strings.R.string.jump_to_end)
    }

    @Test
    fun whenDraftFinalized_displaySnackbarWithViewAction() {
        rule.startAtMainMenu()
            .copyForm("one-question.xml")
            .startBlankForm("One Question")
            .answerQuestion(0, "25")
            .swipeToEndScreen()
            .clickSaveAsDraft()
            .clickDrafts()
            .clickOnForm("One Question")
            .clickGoToEnd()
            .clickFinalize()
            .assertText(net.lalibre.collect.strings.R.string.form_saved)
            .clickOnString(net.lalibre.collect.strings.R.string.view_form)
            .assertText("25")
            .assertTextDoesNotExist(net.lalibre.collect.strings.R.string.jump_to_beginning)
            .assertTextDoesNotExist(net.lalibre.collect.strings.R.string.jump_to_end)
            .assertText(net.lalibre.collect.strings.R.string.exit)
    }

    @Test
    fun snackbarCanBeDismissed_andWillNotBeDisplayedAgainAfterRecreatingTheActivity() {
        rule.startAtMainMenu()
            .copyForm("one-question.xml")
            .startBlankForm("One Question")
            .swipeToEndScreen()
            .clickSaveAsDraft()
            .assertText(net.lalibre.collect.strings.R.string.form_saved_as_draft)
            .closeSnackbar()
            .assertTextDoesNotExist(net.lalibre.collect.strings.R.string.form_saved_as_draft)
            .rotateToLandscape(MainMenuPage())
            .assertTextDoesNotExist(net.lalibre.collect.strings.R.string.form_saved_as_draft)
    }
}
