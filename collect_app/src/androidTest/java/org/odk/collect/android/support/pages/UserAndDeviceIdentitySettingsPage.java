package net.lalibre.collect.otrosmapas.support.pages;

public class UserAndDeviceIdentitySettingsPage extends Page<UserAndDeviceIdentitySettingsPage> {

    @Override
    public UserAndDeviceIdentitySettingsPage assertOnPage() {
        assertText(net.lalibre.collect.strings.R.string.user_and_device_identity_title);
        return this;
    }

    public FormMetadataPage clickFormMetadata() {
        clickOnString(net.lalibre.collect.strings.R.string.form_metadata);
        return new FormMetadataPage();
    }
}
