package net.lalibre.collect.otrosmapas.support.pages

import net.lalibre.collect.otrosmapas.R

class ViewFormPage(private val formName: String) : Page<ViewFormPage>() {

    override fun assertOnPage(): ViewFormPage {
        assertToolbarTitle(formName)
        assertText(net.lalibre.collect.strings.R.string.exit)
        return this
    }
}
