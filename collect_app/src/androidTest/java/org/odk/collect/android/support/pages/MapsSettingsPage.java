package net.lalibre.collect.otrosmapas.support.pages;

public class MapsSettingsPage extends Page<MapsSettingsPage> {

    @Override
    public MapsSettingsPage assertOnPage() {
        assertText(net.lalibre.collect.strings.R.string.maps);
        return this;
    }
}
