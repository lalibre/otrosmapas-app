package net.lalibre.collect.otrosmapas.support

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import net.lalibre.collect.otrosmapas.application.Collect
import net.lalibre.collect.otrosmapas.injection.DaggerUtils
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyComponent
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyModule
import net.lalibre.collect.otrosmapas.injection.config.DaggerAppDependencyComponent
import net.lalibre.collect.projects.Project
import net.lalibre.collect.settings.keys.ProjectKeys

object CollectHelpers {
    fun overrideAppDependencyModule(appDependencyModule: AppDependencyModule): AppDependencyComponent {
        val application = ApplicationProvider.getApplicationContext<Collect>()
        val testComponent = DaggerAppDependencyComponent.builder()
            .application(application)
            .appDependencyModule(appDependencyModule)
            .build()
        application.component = testComponent
        return testComponent
    }

    fun simulateProcessRestart(appDependencyModule: AppDependencyModule? = null) {
        ApplicationProvider.getApplicationContext<Collect>().getState().clear()

        val newComponent =
            overrideAppDependencyModule(appDependencyModule ?: AppDependencyModule())

        // Reinitialize any application state with new deps/state
        newComponent.applicationInitializer().initialize()
    }

    @JvmStatic
    fun addDemoProject() {
        val component =
            DaggerUtils.getComponent(ApplicationProvider.getApplicationContext<Application>())
        component.projectsRepository().save(Project.DEMO_PROJECT)
        component.currentProjectProvider().setCurrentProject(Project.DEMO_PROJECT_ID)
    }

    @JvmStatic
    fun addGDProject(gdProject: Project.New, accountName: String, testDependencies: TestDependencies) {
        testDependencies.googleAccountPicker.setDeviceAccount(accountName)
        testDependencies.googleApi.setAccount(accountName)

        val project = DaggerUtils
            .getComponent(ApplicationProvider.getApplicationContext<Application>())
            .projectsRepository()
            .save(gdProject)

        DaggerUtils
            .getComponent(ApplicationProvider.getApplicationContext<Application>())
            .settingsProvider().getUnprotectedSettings(project.uuid)
            .also {
                it.save(ProjectKeys.KEY_PROTOCOL, ProjectKeys.PROTOCOL_GOOGLE_SHEETS)
                it.save(ProjectKeys.KEY_SELECTED_GOOGLE_ACCOUNT, accountName)
            }
    }
}
