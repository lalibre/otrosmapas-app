package net.lalibre.collect.otrosmapas.regression;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import net.lalibre.collect.otrosmapas.support.rules.CollectTestRule;
import net.lalibre.collect.otrosmapas.support.rules.TestRuleChain;
import net.lalibre.collect.otrosmapas.support.pages.AccessControlPage;
import net.lalibre.collect.otrosmapas.support.pages.MainMenuPage;
import net.lalibre.collect.otrosmapas.support.pages.ProjectSettingsPage;
import net.lalibre.collect.otrosmapas.support.pages.ResetApplicationDialog;

//Issue NODK-240
public class ResetApplicationTest {

    public CollectTestRule rule = new CollectTestRule();

    @Rule
    public RuleChain copyFormChain = TestRuleChain.chain()
            .around(rule);

    @Test
    public void when_rotateScreen_should_resetDialogNotDisappear() {
        //TestCase1
        rule.startAtMainMenu()
                .openProjectSettingsDialog()
                .clickSettings()
                .clickProjectManagement()
                .clickOnResetApplication()
                .assertText(net.lalibre.collect.strings.R.string.reset_settings_dialog_title)
                .assertDisabled(net.lalibre.collect.strings.R.string.reset_settings_button_reset)
                .rotateToLandscape(new ResetApplicationDialog())
                .assertText(net.lalibre.collect.strings.R.string.reset_settings_dialog_title)
                .assertDisabled(net.lalibre.collect.strings.R.string.reset_settings_button_reset)
                .rotateToPortrait(new ResetApplicationDialog())
                .assertText(net.lalibre.collect.strings.R.string.reset_settings_dialog_title)
                .assertDisabled(net.lalibre.collect.strings.R.string.reset_settings_button_reset);
    }

    @Test
    public void savedAndBlankForms_shouldBeReset() {
        //TestCase1,4
        rule.startAtMainMenu()
                .copyForm("all-widgets.xml")
                .startBlankForm("All widgets")
                .clickGoToArrow()
                .clickJumpEndButton()
                .clickSaveAsDraft()
                .clickDrafts()
                .assertText("All widgets")
                .pressBack(new MainMenuPage())
                .openProjectSettingsDialog()
                .clickSettings()
                .clickProjectManagement()
                .clickOnResetApplication()
                .assertDisabled(net.lalibre.collect.strings.R.string.reset_settings_button_reset)
                .clickOnString(net.lalibre.collect.strings.R.string.reset_saved_forms)
                .clickOnString(net.lalibre.collect.strings.R.string.reset_blank_forms)
                .clickOnString(net.lalibre.collect.strings.R.string.reset_settings_button_reset)
                .clickOKOnDialog();
        new MainMenuPage()
                .clickFillBlankForm()
                .assertTextDoesNotExist("All widgets")
                .pressBack(new MainMenuPage())
                .clickDrafts(false)
                .assertTextDoesNotExist("All widgets");
    }

    @Test
    public void adminSettings_shouldBeReset() {
        //TestCase2
        rule.startAtMainMenu()
                .openProjectSettingsDialog()
                .clickSettings()
                .clickAccessControl()
                .openUserSettings()
                .uncheckServerOption()
                .pressBack(new AccessControlPage())
                .pressBack(new ProjectSettingsPage())
                .pressBack(new MainMenuPage())
                .openProjectSettingsDialog()
                .clickSettings()
                .checkIfServerOptionIsNotDisplayed()
                .pressBack(new MainMenuPage())
                .openProjectSettingsDialog()
                .clickSettings()
                .clickProjectManagement()
                .clickOnResetApplication()
                .clickOnString(net.lalibre.collect.strings.R.string.reset_settings)
                .clickOnString(net.lalibre.collect.strings.R.string.reset_settings_button_reset)
                .clickOKOnDialog();
        new MainMenuPage()
                .openProjectSettingsDialog()
                .clickSettings()
                .checkIfServerOptionIsDisplayed();
    }

    @Test
    public void userInterfaceSettings_shouldBeReset() {
        //TestCase3
        rule.startAtMainMenu()
                .openProjectSettingsDialog()
                .clickSettings()
                .clickOnUserInterface()
                .assertText(net.lalibre.collect.strings.R.string.theme_system)
                .clickOnTheme()
                .clickOnString(net.lalibre.collect.strings.R.string.theme_dark);

        new MainMenuPage()
                .assertOnPage()
                .openProjectSettingsDialog()
                .clickSettings()
                .clickOnUserInterface()
                .assertText(net.lalibre.collect.strings.R.string.theme_dark)
                .clickOnLanguage()
                .clickOnSelectedLanguage("español")

                .openProjectSettingsDialog()
                .clickSettings()
                .clickOnUserInterface()
                .assertText("español")
                .pressBack(new ProjectSettingsPage())
                .pressBack(new MainMenuPage())
                .openProjectSettingsDialog()
                .clickSettings()
                .clickProjectManagement()
                .clickOnResetApplication()
                .clickOnString(net.lalibre.collect.strings.R.string.reset_settings)
                .clickOnString(net.lalibre.collect.strings.R.string.reset_settings_button_reset)
                .clickOKOnDialog(new MainMenuPage())

                .openProjectSettingsDialog()
                .clickSettings()
                .clickOnUserInterface()
                .assertText(net.lalibre.collect.strings.R.string.theme_system)
                .assertTextDoesNotExist(net.lalibre.collect.strings.R.string.theme_dark)
                .assertText(net.lalibre.collect.strings.R.string.use_device_language)
                .assertTextDoesNotExist("español");
    }
}
