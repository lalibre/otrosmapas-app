package net.lalibre.collect.otrosmapas.support.pages

import net.lalibre.collect.otrosmapas.R

class EntitiesPage : Page<EntitiesPage>() {

    override fun assertOnPage(): EntitiesPage {
        assertToolbarTitle(net.lalibre.collect.strings.R.string.entities_title)
        return this
    }

    fun clickOnDataset(datasetName: String): DatasetPage {
        clickOnText(datasetName)
        return DatasetPage(datasetName)
    }
}
