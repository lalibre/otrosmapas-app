package net.lalibre.collect.otrosmapas.support.pages;

public class ExperimentalPage extends Page<ExperimentalPage> {

    @Override
    public ExperimentalPage assertOnPage() {
        assertToolbarTitle(getTranslatedString(net.lalibre.collect.strings.R.string.experimental));
        return this;
    }
}
