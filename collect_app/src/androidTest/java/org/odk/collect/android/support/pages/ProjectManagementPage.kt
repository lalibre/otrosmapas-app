package net.lalibre.collect.otrosmapas.support.pages

import net.lalibre.collect.otrosmapas.R

class ProjectManagementPage : Page<ProjectManagementPage>() {

    override fun assertOnPage(): ProjectManagementPage {
        assertText(net.lalibre.collect.strings.R.string.project_management_section_title)
        return this
    }

    fun clickOnResetApplication(): ProjectManagementPage {
        clickOnString(net.lalibre.collect.strings.R.string.reset_project_settings_title)
        return this
    }

    fun clickConfigureQR(): QRCodePage {
        clickOnString(net.lalibre.collect.strings.R.string.reconfigure_with_qr_code_settings_title)
        return QRCodePage().assertOnPage()
    }

    fun clickOnDeleteProject(): ProjectManagementPage {
        scrollToRecyclerViewItemAndClickText(net.lalibre.collect.strings.R.string.delete_project)
        return this
    }

    fun deleteProject(): MainMenuPage {
        scrollToRecyclerViewItemAndClickText(net.lalibre.collect.strings.R.string.delete_project)
        clickOnString(net.lalibre.collect.strings.R.string.delete_project_yes)
        return MainMenuPage()
    }

    fun deleteLastProject(): FirstLaunchPage {
        scrollToRecyclerViewItemAndClickText(net.lalibre.collect.strings.R.string.delete_project)
        clickOnString(net.lalibre.collect.strings.R.string.delete_project_yes)
        return FirstLaunchPage()
    }
}
