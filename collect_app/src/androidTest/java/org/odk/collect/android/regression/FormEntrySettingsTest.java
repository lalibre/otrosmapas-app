package net.lalibre.collect.otrosmapas.regression;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import net.lalibre.collect.otrosmapas.R;
import net.lalibre.collect.otrosmapas.support.pages.AccessControlPage;
import net.lalibre.collect.otrosmapas.support.pages.MainMenuPage;
import net.lalibre.collect.otrosmapas.support.pages.ProjectSettingsPage;
import net.lalibre.collect.otrosmapas.support.pages.SaveOrDiscardFormDialog;
import net.lalibre.collect.otrosmapas.support.rules.CollectTestRule;
import net.lalibre.collect.otrosmapas.support.rules.TestRuleChain;

//Issue NODK-243
public class FormEntrySettingsTest {

    public CollectTestRule rule = new CollectTestRule();

    @Rule
    public RuleChain copyFormChain = TestRuleChain.chain()
            .around(rule);

    @SuppressWarnings("PMD.AvoidCallingFinalize")
    @Test
    public void movingBackwards_shouldBeTurnedOn() {
        rule.startAtMainMenu()
                .copyForm("all-widgets.xml")
                .openProjectSettingsDialog()
                .clickSettings()
                .openFormManagement()
                .openConstraintProcessing()
                .clickOnString(net.lalibre.collect.strings.R.string.constraint_behavior_on_finalize)
                .pressBack(new ProjectSettingsPage())
                .pressBack(new MainMenuPage())
                .openProjectSettingsDialog()
                .clickSettings()
                .clickAccessControl()
                .clickFormEntrySettings()
                .clickMovingBackwards()
                .assertText(net.lalibre.collect.strings.R.string.moving_backwards_disabled_title)
                .assertText(net.lalibre.collect.strings.R.string.yes)
                .assertText(net.lalibre.collect.strings.R.string.no)
                .clickOnString(net.lalibre.collect.strings.R.string.yes)
                .assertSaveAsDraftInFormEntryDisabled()
                .pressBack(new AccessControlPage())
                .pressBack(new ProjectSettingsPage())
                .pressBack(new MainMenuPage())
                .openProjectSettingsDialog()
                .clickSettings()
                .openFormManagement()
                .scrollToConstraintProcessing()
                .checkIfConstraintProcessingIsDisabled()
                .assertTextDoesNotExist(net.lalibre.collect.strings.R.string.constraint_behavior_on_finalize)
                .assertText(net.lalibre.collect.strings.R.string.constraint_behavior_on_swipe)
                .pressBack(new ProjectSettingsPage())
                .pressBack(new MainMenuPage())
                .checkIfElementIsGone(R.id.review_data)
                .startBlankForm("All widgets")
                .swipeToNextQuestion("String widget")
                .closeSoftKeyboard()
                .swipeToPreviousQuestion("String widget")
                .pressBack(new SaveOrDiscardFormDialog<>(new MainMenuPage(), false))
                .assertText(net.lalibre.collect.strings.R.string.do_not_save)
                .assertTextDoesNotExist(net.lalibre.collect.strings.R.string.keep_changes)
                .clickOnString(net.lalibre.collect.strings.R.string.do_not_save);
    }
}
