package net.lalibre.collect.otrosmapas.support.pages;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.CursorMatchers.withRowString;

import net.lalibre.collect.otrosmapas.database.forms.DatabaseFormColumns;

public class ViewSentFormPage extends Page<ViewSentFormPage> {

    @Override
    public ViewSentFormPage assertOnPage() {
        assertToolbarTitle(net.lalibre.collect.strings.R.string.view_sent_forms);
        return this;
    }

    public ViewFormPage clickOnForm(String formName) {
        onData(withRowString(DatabaseFormColumns.DISPLAY_NAME, formName)).perform(click());
        return new ViewFormPage(formName).assertOnPage();
    }
}
