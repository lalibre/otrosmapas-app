package net.lalibre.collect.otrosmapas.support

import android.app.Application
import android.provider.BaseColumns
import androidx.test.core.app.ApplicationProvider
import net.lalibre.collect.otrosmapas.database.forms.DatabaseFormColumns
import net.lalibre.collect.otrosmapas.database.instances.DatabaseInstanceColumns
import net.lalibre.collect.otrosmapas.external.FormsContract
import net.lalibre.collect.otrosmapas.external.InstancesContract

object ContentProviderUtils {

    fun getFormDatabaseId(projectId: String, formId: String): Long {
        val contentResolver =
            ApplicationProvider.getApplicationContext<Application>().contentResolver
        val uri = FormsContract.getUri(projectId)
        return contentResolver.query(uri, null, null, null, null, null).use {
            if (it != null) {
                var dbId: Long? = null
                while (it.moveToNext()) {
                    if (it.getString(it.getColumnIndex(DatabaseFormColumns.JR_FORM_ID)) == formId) {
                        dbId = it.getLong(it.getColumnIndex(BaseColumns._ID))
                    }
                }

                dbId ?: throw IllegalArgumentException("Form does not exist!")
            } else {
                throw RuntimeException("Null cursor!")
            }
        }
    }

    fun getInstanceDatabaseId(projectId: String, formId: String): Long {
        val contentResolver =
            ApplicationProvider.getApplicationContext<Application>().contentResolver
        val uri = InstancesContract.getUri(projectId)
        return contentResolver.query(uri, null, null, null, null, null).use {
            if (it != null) {
                var dbId: Long? = null
                while (it.moveToNext()) {
                    if (it.getString(it.getColumnIndex(DatabaseInstanceColumns.JR_FORM_ID)) == formId) {
                        dbId = it.getLong(it.getColumnIndex(BaseColumns._ID))
                    }
                }

                dbId ?: throw IllegalArgumentException("Form does not exist!")
            } else {
                throw RuntimeException("Null cursor!")
            }
        }
    }
}
