package net.lalibre.collect.otrosmapas.fakes

import android.app.Activity
import androidx.test.platform.app.InstrumentationRegistry
import net.lalibre.collect.permissions.ContextCompatPermissionChecker
import net.lalibre.collect.permissions.PermissionListener
import net.lalibre.collect.permissions.PermissionsProvider

/**
 * Mocked implementation of [PermissionsProvider].
 * The runtime permissions can be stubbed for unit testing
 *
 * @author Shobhit Agarwal
 */
class FakePermissionsProvider :
    PermissionsProvider(ContextCompatPermissionChecker(InstrumentationRegistry.getInstrumentation().targetContext)) {
    private var isPermissionGranted = false

    val requestedPermissions = mutableListOf<String>()

    override fun requestPermissions(
        activity: Activity,
        listener: PermissionListener,
        vararg permissions: String
    ) {
        requestedPermissions.addAll(permissions)

        if (isPermissionGranted) {
            listener.granted()
        } else {
            listener.denied()
        }
    }

    fun setPermissionGranted(permissionGranted: Boolean) {
        isPermissionGranted = permissionGranted
    }
}
