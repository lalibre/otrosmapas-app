package net.lalibre.collect.otrosmapas.backgroundwork

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import net.lalibre.collect.otrosmapas.TestSettingsProvider
import net.lalibre.collect.otrosmapas.formmanagement.FormSourceProvider
import net.lalibre.collect.otrosmapas.formmanagement.InstancesDataService
import net.lalibre.collect.otrosmapas.gdrive.GoogleAccountsManager
import net.lalibre.collect.otrosmapas.gdrive.GoogleApiProvider
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyModule
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.AutoSendSettingsProvider
import net.lalibre.collect.otrosmapas.instancemanagement.autosend.InstanceAutoSender
import net.lalibre.collect.otrosmapas.notifications.Notifier
import net.lalibre.collect.otrosmapas.projects.ProjectDependencyProvider
import net.lalibre.collect.otrosmapas.projects.ProjectDependencyProviderFactory
import net.lalibre.collect.otrosmapas.storage.StoragePathProvider
import net.lalibre.collect.otrosmapas.support.CollectHelpers
import net.lalibre.collect.otrosmapas.utilities.ChangeLockProvider
import net.lalibre.collect.otrosmapas.utilities.FormsRepositoryProvider
import net.lalibre.collect.otrosmapas.utilities.InstancesRepositoryProvider
import net.lalibre.collect.metadata.PropertyManager
import net.lalibre.collect.permissions.PermissionsProvider
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.settings.keys.ProjectKeys
import net.lalibre.collect.testshared.RobolectricHelpers

@RunWith(AndroidJUnit4::class)
class AutoSendTaskSpecTest {

    private val instanceAutoSender = mock<InstanceAutoSender>()
    private val projectDependencyProvider = mock<ProjectDependencyProvider>()
    private val projectDependencyProviderFactory = mock<ProjectDependencyProviderFactory>()

    private lateinit var projectId: String

    @Before
    fun setup() {
        CollectHelpers.overrideAppDependencyModule(object : AppDependencyModule() {
            override fun providesInstanceAutoSender(
                autoSendSettingsProvider: AutoSendSettingsProvider?,
                context: Context?,
                notifier: Notifier?,
                googleAccountsManager: GoogleAccountsManager?,
                googleApiProvider: GoogleApiProvider?,
                permissionsProvider: PermissionsProvider?,
                instancesDataService: InstancesDataService?,
                propertyManager: PropertyManager?
            ): InstanceAutoSender {
                return instanceAutoSender
            }

            override fun providesProjectDependencyProviderFactory(
                settingsProvider: SettingsProvider?,
                formsRepositoryProvider: FormsRepositoryProvider?,
                instancesRepositoryProvider: InstancesRepositoryProvider?,
                storagePathProvider: StoragePathProvider?,
                changeLockProvider: ChangeLockProvider?,
                formSourceProvider: FormSourceProvider?
            ): ProjectDependencyProviderFactory {
                return projectDependencyProviderFactory
            }
        })

        RobolectricHelpers.mountExternalStorage()
        projectId = CollectHelpers.setupDemoProject()
        TestSettingsProvider.getUnprotectedSettings(projectId)
            .save(ProjectKeys.KEY_AUTOSEND, "wifi_and_cellular")

        whenever(projectDependencyProviderFactory.create(projectId)).thenReturn(projectDependencyProvider)
    }

    @Test
    fun `passes projectDependencyProvider with proper project id`() {
        val inputData = mapOf(TaskData.DATA_PROJECT_ID to projectId)
        AutoSendTaskSpec().getTask(ApplicationProvider.getApplicationContext(), inputData, true).get()
        verify(instanceAutoSender).autoSendInstances(projectDependencyProvider)
    }

    @Test
    fun `maxRetries should not be limited`() {
        assertThat(AutoSendTaskSpec().maxRetries, `is`(nullValue()))
    }
}
