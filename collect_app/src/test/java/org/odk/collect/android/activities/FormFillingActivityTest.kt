package net.lalibre.collect.otrosmapas.activities

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.DialogFragment
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.work.WorkManager
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import net.lalibre.collect.otrosmapas.external.FormsContract
import net.lalibre.collect.otrosmapas.formmanagement.FormFillingIntentFactory
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyComponent
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyModule
import net.lalibre.collect.otrosmapas.storage.StorageSubdirectory
import net.lalibre.collect.otrosmapas.support.CollectHelpers
import net.lalibre.collect.otrosmapas.support.CollectHelpers.resetProcess
import net.lalibre.collect.otrosmapas.utilities.FileUtils
import net.lalibre.collect.otrosmapasshared.ui.DialogFragmentUtils
import net.lalibre.collect.otrosmapastest.RecordedIntentsRule
import net.lalibre.collect.async.Scheduler
import net.lalibre.collect.externalapp.ExternalAppUtils
import net.lalibre.collect.forms.Form
import net.lalibre.collect.formstest.FormFixtures.form
import net.lalibre.collect.strings.R
import net.lalibre.collect.testshared.ActivityControllerRule
import net.lalibre.collect.testshared.AssertIntentsHelper
import net.lalibre.collect.testshared.EspressoHelpers.assertText
import net.lalibre.collect.testshared.EspressoHelpers.clickOnContentDescription
import net.lalibre.collect.testshared.EspressoHelpers.clickOnText
import net.lalibre.collect.testshared.FakeScheduler
import net.lalibre.collect.testshared.RobolectricHelpers.recreateWithProcessRestore
import org.robolectric.Shadows.shadowOf
import java.io.File

@RunWith(AndroidJUnit4::class)
class FormFillingActivityTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val recordedIntentsRule = RecordedIntentsRule()

    @get:Rule
    val activityControllerRule = ActivityControllerRule()

    private val assertIntentsHelper = AssertIntentsHelper()

    private val scheduler = FakeScheduler()
    private val dependencies = object : AppDependencyModule() {
        override fun providesScheduler(workManager: WorkManager): Scheduler {
            return scheduler
        }
    }

    private val application = ApplicationProvider.getApplicationContext<Application>()
    private lateinit var component: AppDependencyComponent

    @Before
    fun setup() {
        component = CollectHelpers.overrideAppDependencyModule(dependencies)
    }

    @Test
    fun whenProcessIsKilledAndRestored_returnsToHierarchyAtQuestion() {
        val projectId = CollectHelpers.setupDemoProject()

        val form = setupForm("forms/two-question.xml")
        val intent = FormFillingIntentFactory.newInstanceIntent(
            application,
            FormsContract.getUri(projectId, form!!.dbId),
            FormFillingActivity::class
        )

        // Start activity
        val initial = activityControllerRule.build(FormFillingActivity::class.java, intent).setup()
        scheduler.flush()
        assertText("Two Question")
        assertText("What is your name?")

        clickOnText(R.string.form_forward)
        scheduler.flush()
        assertText("What is your age?")

        // Recreate and assert we start FormHierarchyActivity
        val recreated = activityControllerRule.add {
            initial.recreateWithProcessRestore { resetProcess(dependencies) }
        }

        scheduler.flush()
        assertIntentsHelper.assertNewIntent(FormHierarchyActivity::class)

        // Return to FormFillingActivity from FormHierarchyActivity
        val hierarchyIntent = shadowOf(recreated.get()).nextStartedActivityForResult.intent
        shadowOf(recreated.get()).receiveResult(hierarchyIntent, Activity.RESULT_CANCELED, null)
        scheduler.flush()

        assertText("Two Question")
        assertText("What is your age?")
    }

    @Test
    fun whenProcessIsKilledAndRestored_andHierarchyIsOpen_returnsToHierarchyAtQuestion() {
        val projectId = CollectHelpers.setupDemoProject()

        val form = setupForm("forms/two-question.xml")
        val intent = FormFillingIntentFactory.newInstanceIntent(
            application,
            FormsContract.getUri(projectId, form!!.dbId),
            FormFillingActivity::class
        )

        // Start activity
        val initial = activityControllerRule.build(FormFillingActivity::class.java, intent).setup()
        scheduler.flush()
        assertText("Two Question")
        assertText("What is your name?")

        clickOnText(R.string.form_forward)
        scheduler.flush()
        assertText("What is your age?")

        clickOnContentDescription(R.string.view_hierarchy)
        assertIntentsHelper.assertNewIntent(FormHierarchyActivity::class)

        // Recreate and assert we start FormHierarchyActivity
        val recreated = activityControllerRule.add {
            initial.recreateWithProcessRestore { resetProcess(dependencies) }
        }

        scheduler.flush()
        assertIntentsHelper.assertNewIntent(FormHierarchyActivity::class)

        // Return to FormFillingActivity from FormHierarchyActivity
        val hierarchyIntent = shadowOf(recreated.get()).nextStartedActivityForResult.intent
        shadowOf(recreated.get()).receiveResult(hierarchyIntent, Activity.RESULT_CANCELED, null)
        scheduler.flush()

        assertText("Two Question")
        assertText("What is your age?")
    }

    @Test
    fun whenProcessIsKilledAndRestored_andThereADialogFragmentOpen_doesNotRestoreDialogFragment() {
        val projectId = CollectHelpers.setupDemoProject()

        val form = setupForm("forms/two-question.xml")
        val intent = FormFillingIntentFactory.newInstanceIntent(
            application,
            FormsContract.getUri(projectId, form!!.dbId),
            FormFillingActivity::class
        )

        // Start activity
        val initial = activityControllerRule.build(FormFillingActivity::class.java, intent).setup()
        scheduler.flush()
        assertText("Two Question")
        assertText("What is your name?")

        clickOnText(R.string.form_forward)
        scheduler.flush()
        assertText("What is your age?")

        val initialFragmentManager = initial.get().supportFragmentManager
        DialogFragmentUtils.showIfNotShowing(TestDialogFragment::class.java, initialFragmentManager)
        assertThat(
            initialFragmentManager.fragments.any { it::class == TestDialogFragment::class },
            equalTo(true)
        )

        // Recreate and assert we start FormHierarchyActivity
        val recreated = activityControllerRule.add {
            initial.recreateWithProcessRestore { resetProcess(dependencies) }
        }

        scheduler.flush()
        assertIntentsHelper.assertNewIntent(FormHierarchyActivity::class)

        // Return to FormFillingActivity from FormHierarchyActivity
        val hierarchyIntent = shadowOf(recreated.get()).nextStartedActivityForResult.intent
        shadowOf(recreated.get()).receiveResult(hierarchyIntent, Activity.RESULT_CANCELED, null)
        scheduler.flush()

        assertText("Two Question")
        assertText("What is your age?")
    }

    @Test
    fun whenProcessIsKilledAndRestored_andIsWaitingForExternalData_dataCanStillBeReturned() {
        val projectId = CollectHelpers.setupDemoProject()

        val form = setupForm("forms/two-question-external.xml")
        val intent = FormFillingIntentFactory.newInstanceIntent(
            application,
            FormsContract.getUri(projectId, form!!.dbId),
            FormFillingActivity::class
        )

        // Start activity
        val initial = activityControllerRule.build(FormFillingActivity::class.java, intent).setup()
        scheduler.flush()
        assertText("Two Question")
        assertText("What is your name?")

        clickOnText(R.string.form_forward)
        scheduler.flush()
        assertText("What is your age?")

        // Open external app
        clickOnContentDescription(R.string.launch_app)
        assertIntentsHelper.assertNewIntent(hasAction("com.example.EXAMPLE"))

        // Recreate with result
        val returnData = ExternalAppUtils.getReturnIntent("159")
        activityControllerRule.add {
            initial.recreateWithProcessRestore(RESULT_OK, returnData) { resetProcess(dependencies) }
        }

        scheduler.flush()

        assertIntentsHelper.assertNoNewIntent()
        assertText("Two Question")
        assertText("What is your age?")
        assertText("159")
    }

    private fun setupForm(testFormPath: String): Form? {
        val formsDir = component.storagePathProvider().getOdkDirPath(StorageSubdirectory.FORMS)
        val formFile = FileUtils.copyFileFromResources(
            testFormPath,
            File(formsDir, "two-question.xml")
        )

        val formsRepository = component.formsRepositoryProvider().get()
        val form = formsRepository.save(form(formFilePath = formFile.absolutePath))
        return form
    }

    class TestDialogFragment : DialogFragment()
}
