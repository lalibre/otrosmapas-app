package net.lalibre.collect.otrosmapas.entities

import net.lalibre.collect.entities.EntitiesRepository

class InMemEntitiesRepositoryTest : EntitiesRepositoryTest() {

    override fun buildSubject(): EntitiesRepository {
        return InMemEntitiesRepository()
    }
}
