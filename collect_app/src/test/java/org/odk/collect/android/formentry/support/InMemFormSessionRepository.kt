package net.lalibre.collect.otrosmapas.formentry.support

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import net.lalibre.collect.otrosmapas.formentry.FormSession
import net.lalibre.collect.otrosmapas.formentry.FormSessionRepository
import net.lalibre.collect.otrosmapas.javarosawrapper.FormController
import net.lalibre.collect.forms.Form
import net.lalibre.collect.forms.instances.Instance
import net.lalibre.collect.shared.strings.UUIDGenerator

class InMemFormSessionRepository : FormSessionRepository {

    private val map = mutableMapOf<String, MutableLiveData<FormSession>>()

    override fun create(): String {
        return UUIDGenerator().generateUUID()
    }

    override fun get(id: String): LiveData<FormSession> {
        return getLiveData(id)
    }

    override fun set(id: String, formController: FormController, form: Form, instance: Instance?) {
        getLiveData(id).value = FormSession(formController, form, instance)
    }

    override fun clear(id: String) {
        getLiveData(id).value = null
        map.remove(id)
    }

    private fun getLiveData(id: String): MutableLiveData<FormSession> {
        return map.getOrPut(id) { MutableLiveData<FormSession>(null) }
    }
}
