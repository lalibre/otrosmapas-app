package net.lalibre.collect.otrosmapas.configure;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;
import net.lalibre.collect.otrosmapas.TestSettingsProvider;
import net.lalibre.collect.otrosmapas.preferences.utilities.FormUpdateMode;
import net.lalibre.collect.otrosmapas.preferences.utilities.SettingsUtils;
import net.lalibre.collect.settings.keys.ProjectKeys;
import net.lalibre.collect.shared.settings.Settings;

@RunWith(AndroidJUnit4.class)
public class SettingsUtilsTest {

    @Test
    public void getFormUpdateMode_whenProtocolIsGoogleDrive_andModeNotManual_returnsManual() {
        Settings generalSettings = TestSettingsProvider.getUnprotectedSettings();
        Context context = getApplicationContext();

        generalSettings.save(ProjectKeys.KEY_PROTOCOL, ProjectKeys.PROTOCOL_GOOGLE_SHEETS);
        generalSettings.save(ProjectKeys.KEY_FORM_UPDATE_MODE, FormUpdateMode.PREVIOUSLY_DOWNLOADED_ONLY.getValue(context));

        FormUpdateMode formUpdateMode = SettingsUtils.getFormUpdateMode(context, generalSettings);
        assertThat(formUpdateMode, is(FormUpdateMode.MANUAL));
    }
}
