package net.lalibre.collect.otrosmapas.openrosa;

import android.webkit.MimeTypeMap;

import net.lalibre.collect.otrosmapas.openrosa.okhttp.OkHttpConnection;
import net.lalibre.collect.otrosmapas.openrosa.okhttp.OkHttpOpenRosaServerClientProvider;

import okhttp3.OkHttpClient;

public class OkHttpConnectionGetRequestTest extends OpenRosaGetRequestTest {

    @Override
    protected OpenRosaHttpInterface buildSubject() {
        return new OkHttpConnection(
                new OkHttpOpenRosaServerClientProvider(new OkHttpClient()),
                new CollectThenSystemContentTypeMapper(MimeTypeMap.getSingleton()),
                USER_AGENT
        );
    }
}