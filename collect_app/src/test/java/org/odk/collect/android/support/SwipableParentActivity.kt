package net.lalibre.collect.otrosmapas.support

import androidx.fragment.app.FragmentActivity
import net.lalibre.collect.otrosmapas.audio.AudioControllerView.SwipableParent

class SwipableParentActivity : FragmentActivity(), SwipableParent {
    var isSwipingAllowed = false
        private set

    override fun allowSwiping(allowSwiping: Boolean) {
        isSwipingAllowed = allowSwiping
    }
}
