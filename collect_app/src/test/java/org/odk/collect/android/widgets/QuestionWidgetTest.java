package net.lalibre.collect.otrosmapas.widgets;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static net.lalibre.collect.otrosmapas.support.CollectHelpers.setupFakeReferenceManager;
import static java.util.Arrays.asList;

import android.content.Context;

import androidx.core.util.Pair;
import androidx.lifecycle.MutableLiveData;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.javarosa.core.model.data.IAnswerData;
import org.javarosa.core.reference.ReferenceManager;
import org.javarosa.form.api.FormEntryPrompt;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import net.lalibre.collect.otrosmapas.R;
import net.lalibre.collect.otrosmapas.audio.AudioButton;
import net.lalibre.collect.otrosmapas.audio.AudioHelper;
import net.lalibre.collect.otrosmapas.formentry.media.AudioHelperFactory;
import net.lalibre.collect.otrosmapas.formentry.questions.QuestionDetails;
import net.lalibre.collect.otrosmapas.injection.config.AppDependencyModule;
import net.lalibre.collect.otrosmapas.support.CollectHelpers;
import net.lalibre.collect.otrosmapas.support.MockFormEntryPromptBuilder;
import net.lalibre.collect.otrosmapas.support.WidgetTestActivity;
import net.lalibre.collect.async.Scheduler;
import net.lalibre.collect.audioclips.Clip;

@RunWith(AndroidJUnit4.class)
public class QuestionWidgetTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    public AudioHelper audioHelper;

    @Before
    public void setup() throws Exception {
        overrideDependencyModule();
        when(audioHelper.setAudio(any(AudioButton.class), any())).thenReturn(new MutableLiveData<>());
    }

    @Test
    public void whenQuestionHasAudio_audioButtonUsesIndexAsClipID() throws Exception {
        FormEntryPrompt prompt = new MockFormEntryPromptBuilder()
                .withIndex("i am index")
                .withAudioURI("ref")
                .build();

        WidgetTestActivity activity = CollectHelpers.createThemedActivity(WidgetTestActivity.class);
        TestWidget widget = new TestWidget(activity, new QuestionDetails(prompt));

        AudioButton audioButton = widget.getAudioVideoImageTextLabel().findViewById(R.id.audioButton);
        verify(audioHelper).setAudio(audioButton, new Clip("i am index", "blah.mp3"));
    }

    private void overrideDependencyModule() throws Exception {
        ReferenceManager referenceManager = setupFakeReferenceManager(asList(new Pair<>("ref", "blah.mp3")));
        CollectHelpers.overrideAppDependencyModule(new AppDependencyModule() {

            @Override
            public ReferenceManager providesReferenceManager() {
                return referenceManager;
            }

            @Override
            public AudioHelperFactory providesAudioHelperFactory(Scheduler scheduler) {
                return context -> audioHelper;
            }
        });
    }

    private static class TestWidget extends QuestionWidget {

        TestWidget(Context context, QuestionDetails questionDetails) {
            super(context, questionDetails);
        }

        @Override
        public IAnswerData getAnswer() {
            return null;
        }

        @Override
        public void clearAnswer() {

        }

        @Override
        public void setOnLongClickListener(OnLongClickListener l) {

        }
    }
}
