package net.lalibre.collect.otrosmapas.widgets.support

import org.javarosa.core.model.SelectChoice
import org.javarosa.form.api.FormEntryPrompt
import net.lalibre.collect.otrosmapas.widgets.interfaces.SelectChoiceLoader

class FormEntryPromptSelectChoiceLoader : SelectChoiceLoader {

    override fun loadSelectChoices(prompt: FormEntryPrompt): List<SelectChoice> {
        return prompt.selectChoices
    }
}
