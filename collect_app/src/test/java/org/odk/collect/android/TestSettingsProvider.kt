package net.lalibre.collect.otrosmapas

import androidx.test.core.app.ApplicationProvider
import net.lalibre.collect.otrosmapas.application.Collect
import net.lalibre.collect.otrosmapas.injection.DaggerUtils
import net.lalibre.collect.settings.SettingsProvider
import net.lalibre.collect.shared.settings.Settings

// Use just for testing
object TestSettingsProvider {
    @JvmStatic
    fun getSettingsProvider(): SettingsProvider {
        return DaggerUtils.getComponent(ApplicationProvider.getApplicationContext<Collect>()).settingsProvider()
    }

    @JvmStatic
    @JvmOverloads
    fun getUnprotectedSettings(uuid: String? = null): Settings {
        return getSettingsProvider().getUnprotectedSettings(uuid)
    }

    @JvmStatic
    fun getProtectedSettings(): Settings {
        return getSettingsProvider().getProtectedSettings()
    }
}
