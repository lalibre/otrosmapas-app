package net.lalibre.collect.otrosmapas.widgets;

import androidx.annotation.NonNull;

import net.bytebuddy.utility.RandomString;

import org.javarosa.core.model.data.StringData;
import org.mockito.Mock;
import net.lalibre.collect.otrosmapas.formentry.questions.QuestionDetails;
import net.lalibre.collect.otrosmapas.widgets.base.GeneralExStringWidgetTest;
import net.lalibre.collect.otrosmapas.widgets.support.FakeWaitingForDataRegistry;
import net.lalibre.collect.otrosmapas.widgets.utilities.StringRequester;

import static org.mockito.Mockito.when;

/**
 * @author James Knight
 */

public class ExStringWidgetTest extends GeneralExStringWidgetTest<ExStringWidget, StringData> {

    @Mock
    StringRequester stringRequester;

    @NonNull
    @Override
    public ExStringWidget createWidget() {
        return new ExStringWidget(activity, new QuestionDetails(formEntryPrompt), new FakeWaitingForDataRegistry(), stringRequester);
    }

    @NonNull
    @Override
    public StringData getNextAnswer() {
        return new StringData(RandomString.make());
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        when(formEntryPrompt.getAppearanceHint()).thenReturn("");
    }
}
